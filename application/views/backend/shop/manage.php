
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo 'My Orders'; ?></h4>
                        <div class="toolbar">
                           
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                   <th>Track ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Payment Via</th>
                                    <th>Paid Amount</th>
                                    <th>Detail</th>
                                    

                                </tr>
                                </thead>
                                 <tbody>
                                        <?php if($results){
                                            foreach($results as $value){ 
                                                
                                                ?>
                                            <tr id="<?php echo $value['OrderID'];?>">
                                                 <td><?php echo $value['OrderTrackID']; ?></td>
                                                <td><?php echo $value['Name']; ?></td>
                                                <td><?php echo $value['Telephone']; ?></td> 
                                                <td><?php echo $value['PaymentMethod']; ?></td>
                                                <td><?php echo $value['AmountDeposit']; ?></td>
                                                
                                                 
                                                <td>
                                                <a href="<?php echo base_url('cms/shop/detail/'.$value['OrderID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="View Detail">dvr</i><div class="ripple-container"></div></a>
                                                    
                                                
                                                </td>
                                                

                                            </tr>
                                                <?php
                                            
                                        }

                                        }
                                        ?>

                                        </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>