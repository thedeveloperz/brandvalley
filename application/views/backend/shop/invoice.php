<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h4 class="card-title">Order Track ID: <?php echo $order_items[0]['OrderTrackID'];?></h4>
                        <p class="category">Total Amount: $<?php echo $order_items[0]['AmountDeposit'];?></p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <th>Product Name</th>
                                <th>Company Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                                
                            </thead>
                            <tbody>
                                <?php 
                                 $total= 0;
                                foreach ($order_items as $key => $value) {
                                     $total = $total + ($value['Quantity'] * $value['Price']);
                                ?>
                                    <tr>
                                        <td><?php echo $value['Title'];?></td>
                                        <td><?php echo $value['CompanyName'];?></td>
                                        <td><?php echo $value['Quantity'];?></td>
                                        <td>$<?php echo $value['Price'];?></td>
                                        <td>$<?php echo ($value['Quantity'] * $value['Price']);?></td>
                                       
                                    </tr>
                                <?php
                                }
                                ?>
                                <tr>
                                        <td colspan="4">Grand Total</td>
                                        <td>$<?php echo $total;?></td>
                                        
                                        
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="green">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Shipping Details</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>State</th>
                                    <th>City</th>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $order_items[0]['Name'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['Email'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['Telephone'];?>
                                    </td>
                                     <td>
                                        <?php echo $order_items[0]['Address'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['Country'];?>
                                    </td>
                                     <td>
                                        <?php echo $order_items[0]['State'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['City'];?>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Order Detail</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    
                                    <th>Order Date:</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Customer Phone</th>
                                    
                                    
                                </tr>
                                <tr>
                                    
                                    <td>
                                        <?php echo date('d-m-Y', strtotime($order_items[0]['OrderCreateDate']));?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['CustomerFullName'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['CustomerEmail'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['CustomerPhone'];?>
                                    </td>
                                    

                                </tr>
                                
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-tabs" data-background-color="yellow">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                                <span class="nav-tabs-title">Payment Detail</span>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Payment Via</th>
                                    <th>Cnic</th>
                                    <th>TransactionID</th>
                                    
                                    <th>Amount Deposit</th>
                                    <th>Deposit Date</th>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $order_items[0]['PaymentMethod'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['Cnic'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['TransactionID'];?>
                                    </td>
                                    <td>
                                        <?php echo $order_items[0]['AmountDeposit'];?>
                                    </td>
                                    <td>
                                        <?php echo date('d-m-Y', strtotime($order_items[0]['DepositDate']));?>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
