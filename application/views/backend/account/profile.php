<?php
$option = '';
if(!empty($roles)){
    foreach($roles as $role){
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }

$countries_opts = '';
if(!empty($countries)){
    foreach($countries as $country){
        $countries_opts .= '<option value="'.$country->CountryID.'" '.((isset($result[0]->CountryID) && $result[0]->CountryID == $country->CountryID) ? 'selected' : '').'>'.$country->Title.' </option>';
    }
}

$cities_opts = '';
if(!empty($cities_opts)){
    foreach($cities_opts as $city){
        $countries_opts .= '<option value="'.$city->CountryID.'" '.((isset($result[0]->CountryID) && $result[0]->CountryID == $country->CountryID) ? 'selected' : '').'>'.$country->Title.' </option>';
    }
}

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';

        if($key == 0){
            $common_fields = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="RoleID" disabled>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
            $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Email">'.lang('email').'</label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Phone">Phone</label>
                                                                <input type="text" name="Phone" parsley-trigger="change" required  class="form-control" id="Email" value="' . ((isset($result[0]->Phone)) ? $result[0]->Phone : '') . '">
                                                            </div>
                                                        </div>';
            $common_fields3 = '<div class="row">
         <div class="col-md-6">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div></div>';

            if($result[0]->RoleID != 1 && $result[0]->RoleID != 2) {


                $common_fields5 = '<div class="row">
                                        <div class="col-md-6">
                                            <label class="col-sm-2 label-on-left lbl-gender">Gender</label>
                                            <div class="col-sm-10 checkbox-radios">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="Gender" ' . ($result[0]->Gender == "Male" ? "checked" : "") . ' value="Male"><span class="circle"></span><span class="check"></span> Male
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="Gender" value="Female" ' . ($result[0]->Gender == "Female" ? "checked" : "") . '><span class="circle"></span><span class="check"></span> Female
                                                    </label>
                                                </div>
                                                <div class="radio">

                                                </div>
                                            </div>
                                            </div>
                                        
                                        </div>
                                        ';


                $common_fields6 = '<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="'. lang('choose_country') . '" data-size="7" id="CountryIDSignup" required>
                                                        '.$countries_opts.'
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="StateID" class="selectpicker" data-style="select-with-transition" title="'.lang('choose_state').'" data-size="7" id="StateIDSignup"  required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="CityID" class="selectpicker" data-style="select-with-transition"  title="'.lang('choose_city').'" data-size="7" id="CityIDSignup" required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>';





            }
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/acount/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="FullName" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->FullName)) ? $result[$key]->FullName : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="row">
                                                        
                                                        
                                                         '.$common_fields2.'
                                                    </div>
                                                    '.$common_fields4.'
                                                    
                                                   

                                                   '.$common_fields5.'
                                                   
                                                   
                                                   '.$common_fields6.'
                                                    '.$common_fields3.'

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';





    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="#pablo">
<!--                            <img class="img" src="--><?php //echo base_url('assets/images/profile2.jpg')?><!--">-->
                            <?php if(!file_exists($result[0]->Image)) {
                                $image = base_url("assets/backend/img/no_img.png");
                            }else {
                                $image = base_url($result[0]->Image);
                            }
                            ?>
                            <img class="img" src="<?php echo $image ?>" alt="">
                        </a>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $result[0]->FullName ?></h4>
                        <h6 class="category text-gray"><?php echo $result[0]->Type ?></h6>
                        <h6 class="category text-gray"><?php echo $result[0]->Gender ?></h6>

<!--                        <a href="#pablo" class="btn btn-rose btn-round">Follow</a>-->
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">perm_identity</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Update Profile
<!--                            <small class="category">Complete your profile</small>-->
                        </h4>
                        <form action="<?php echo base_url();?>cms/account/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate autocomplete="off">
                            <input type="hidden" name="form_type" value="updateProfile">
                            <input type="hidden" name="UserID" value="<?php echo $result[0]->UserID; ?>">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Full Name</label>
                                        <input type="text" name="FullName" value="<?php echo $result[0]->FullName; ?>" class="form-control">
                                        <span class="material-input"></span></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email address</label>
                                        <input type="email" class="form-control br-readonly" value="<?php echo $result[0]->Email ?>" readonly>
                                        <span class="material-input"></span></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="text" name="Phone" class="form-control" value="<?php echo $result[0]->Phone ?>">
                                        <span class="material-input"></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="UserProfileType" class="selectpicker" data-style="select-with-transition"  title="Select Your Type" data-size="7" id="UserProfileType" required>
                                            <option value=""></option>
                                            <?php foreach ($ProfileTypes as $type) { ?>
                                               <option value="<?php echo $type->UserProfileTypeID ?>" <?php echo ($type->UserProfileTypeID == $result[0]->UserProfileType ? 'selected' : ''); ?> ><?php echo $type->ProfileType ?></option>
                                            <?php } ?>
<!--                                            <option value="WholeSaler" --><?php //echo($result[0]->UserProfileType == 'WholeSaler'? 'selected' : '');?><!-->Whole Saler</option>-->
<!--                                            <option value="Retailer" --><?php //echo($result[0]->UserProfileType == 'Retailer'? 'selected' : '');?><!-->Retailer</option>-->
<!--                                            <option value="Manufacturer" --><?php //echo($result[0]->UserProfileType == 'Manufacturer'? 'selected' : '');?><!-->Manufacturer</option>-->
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="Country" data-size="7" id="CountryIDSignup" required>
                                            <?php echo $countries_opts; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="StateID" class="selectpicker" data-style="select-with-transition" title="State" data-size="7" id="StateIDSignup"  required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CityID" class="selectpicker" data-style="select-with-transition"  title="City" data-size="7" id="CityIDSignup" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if(!file_exists($result[0]->Image)) {
                                            $image = base_url("assets/backend/img/no_img.png");
                                        }else {
                                            $image = base_url($result[0]->Image);
                                        }
                                        ?>
                                    <img src="<?php echo $image;?>" style="width:200px;height:200px;">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label>Update Photo</label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="AboutUs">About Us</label>
                                        <textarea class="form-control textarea" name="AboutUs" id="AboutUs" style="height: 100px;"><?php echo $result[0]->AboutUs ?></textarea>

                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-12">
                                    <?php if(!file_exists($result[0]->CoverImage)) {
                                            $image = base_url("assets/backend/img/no_img.png");
                                        }else {
                                            $image = base_url($result[0]->CoverImage);
                                        }
                                        ?>
                                    <img src="<?php echo $image;?>" style="width:200px;height:200px;">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label>Update Cover Photo(1560*170)</label><br>
                                        <input type="file" name="CoverImage[]" id="filer_input3" required>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info pull-right">Update Profile</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        var CountryID =  <?php echo $result[0]->CountryID ?>;
        var SelectedStateID = <?php echo $result[0]->StateID ?>;
        var SelectedCityID = <?php echo $result[0]->CityID ?>;

        function getStates(CountryID) {

            if(CountryID == '')
            {
                $('#StateIDSignup').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getCountryStates',
                    data: {
                        'CountryID': CountryID,
                        'SelectedStateID': SelectedStateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
        };

        // on change
        $('#CountryIDSignup').on('change',function(){
            var CountryID = $(this).val();
            getStates(CountryID);
        });

        // run on load to show selected state
        getStates(CountryID);


        function getCities(StateID) {

            if(StateID == '')
            {
                $('#CityIDSignup').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID,
                        'SelectedCityID': SelectedCityID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
        }

        // runs on change
        $('#StateIDSignup').on('change',function(){
            var StateID = $(this).val();
            getCities(StateID);

        });

        // runds on load
        getCities(SelectedStateID);

        tinymce.init({
            forced_root_block : "",
            selector: 'textarea',
            plugins: "code,textcolor",
            theme : "modern",
            mode: "exact",
            toolbar: "forecolor,backcolor, undo,redo,cleanup,bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
            theme_advanced_toolbar_location : "top",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
            + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
            + "bullist,numlist,outdent,indent",
            theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
            +"undo,redo,cleanup,code,separator,sub,sup,charmap",
            theme_advanced_buttons3 : "",
            height:"250px",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            }
        });

    });
</script>