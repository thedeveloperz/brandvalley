<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Create Business Page Product</h4>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SystemLanguageID" value="1">
                            <input type="hidden" name="BusinessproductID" value="<?php echo $product->BusinessproductID ?>">
                            <input type="hidden" name="IsDefault" value="1">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title" value="<?php echo $product->Title ?>" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="<?php echo lang('choose_country');?>" data-size="7" id="CountryIDSignup" required>
                                            <?php foreach ($countries as $key => $value) { ?>
                                                <option value="<?php echo $value->CountryID; ?>"    <?php echo ((isset($product->CountryID) && $product->CountryID == $value->CountryID) ? 'selected' : '') ?> > <?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="StateID" class="selectpicker" data-style="select-with-transition" title="<?php echo lang('choose_state');?>" data-size="7" id="StateIDSignup"  required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CityID" class="selectpicker" data-style="select-with-transition"  title="<?php echo lang('choose_city');?>" data-size="7" id="CityIDSignup" required>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description"><?php echo lang('short_description'); ?></label>
                                        <textarea class="form-control" name="Description" id="Description"><?php echo $product->Description ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label>Image</label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function(){
        var CountryID =  <?php echo $product->CountryID ?>;
        var SelectedStateID = <?php echo $product->StateID ?>;
        var SelectedCityID = <?php echo $product->CityID ?>;

        function getStates(CountryID) {

            if(CountryID == '')
            {
                $('#StateIDSignup').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getCountryStates',
                    data: {
                        'CountryID': CountryID,
                        'SelectedStateID': SelectedStateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
        };

        // on change
        $('#CountryIDSignup').on('change',function(){
            var CountryID = $(this).val();
            getStates(CountryID);
        });

        // run on load to show selected state
        getStates(CountryID);


        function getCities(StateID) {

            if(StateID == '')
            {
                $('#CityIDSignup').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID,
                        'SelectedCityID': SelectedCityID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
        }

        // runs on change
        $('#StateIDSignup').on('change',function(){
            var StateID = $(this).val();
            getCities(StateID);

        });

        // runds on load
        getCities(SelectedStateID);

    });


</script>