<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Business Page Products</h4>
                        <?php if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){ ?>
                        <?php if(getBusinessPage($this->session->userdata['admin']['UserID'])) { ?>
                            <div class="toolbar">
                                <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Create Business Product</button>
                                </a>
                            </div>
                            <?php } else {  ?>
                                <div class="toolbar">
                                    <div class="alert alert-danger">Click <a style="text-decoration: underline;" href="<?php echo base_url('cms/businessproduct/businesspage');?>"> here </a> to create business page first. </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>ID#</th>
                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('Description');?></th>

                                    <th><?php echo lang('is_active');?></th>


                                    <?php //if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions');?></th>
                                     <?php //} ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->BusinessproductID;?>">

                                            <td><?php echo $value->BusinessproductID; ?></td>

                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->Description; ?></td>


                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                             <?php //if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <td>
                                                <?php //if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->BusinessproductID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                <?php //} ?>
                                               
                                                <?php //if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->BusinessproductID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php //} ?>
                                            </td>
                                            <?php //} ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>