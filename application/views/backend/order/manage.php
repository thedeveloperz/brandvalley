<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-tabs">
                    <ul class="nav nav-pills nav-pills-warning">
                        <?php if( $this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2) { ?>
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Pending Pending</a>
                            </li>
                            
                        <?php } ?>
                        <li class="<?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'active' : '');?>">
                                <a href="#pill2" data-toggle="tab" aria-expanded="<?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'true' : 'false');?>">received</a>
                        </li>
                        <li class="">
                            <a href="#pill3" data-toggle="tab" aria-expanded="false">Dispatched</a>
                        </li>
                        <li class="">
                            <a href="#pill4" data-toggle="tab" aria-expanded="false">Delivered</a>
                        </li>
                    </ul>
                </div>
                <div class="card tab-card">
<!--                    <div class="card-header card-header-icon" data-background-color="purple">-->
<!--                        <i class="material-icons">assignment</i>-->
<!--                    </div>-->
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="tab-content">
                            <?php if( $this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2) { ?>
                            <div class="material-datatables tab-pane active" id="pill1">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Track ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Payment Via</th>
                                            <th>Paid Amount</th>
                                            <th>Status</th>
                                            <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <th>Action</th>
                                        <?php } ?>
                                            

                                            

                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($results){
                                            foreach($results as $value){ 
                                                if($value['Status'] == 'Pending'){
                                                ?>
                                            <tr id="<?php echo $value['OrderID'];?>">
                                                 <td><?php echo $value['OrderTrackID']; ?></td>
                                                <td><?php echo $value['Name']; ?></td>
                                                <td><?php echo $value['Telephone']; ?></td> 
                                                <td><?php echo $value['PaymentMethod']; ?></td>
                                                <td><?php echo $value['AmountDeposit']; ?></td>
                                                <td> 
                                                 <?php 
                                                if($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2)
                                                {
                                                ?>
                                                    <select class="order_status" data-order-id = "<?php echo $value['OrderID'];?>">
                                                        <option value="Pending" <?php echo ($value['Status'] == 'Pending' ? 'selected' : ''); ?>>Pending</option>
                                                        <option value="Received" <?php echo ($value['Status'] == 'Received' ? 'selected' : ''); ?>>Received</option>
                                                    </select>
                                                <?php 
                                                }
                                                else{
                                                    echo $value['Status'];
                                                }
                                                ?>
                                                </td> 
                                                 <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                <td>

                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/detail/'.$value['OrderID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                
                                                </td>
                                                <?php } ?>

                                            </tr>
                                                <?php
                                            }
                                        }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            
                            <?php } ?>
                            <div class="material-datatables tab-pane <?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'active' : '');?>" id="pill2">
                                <div class="material-datatables">
                                    <table class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Track ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Payment Via</th>
                                            <th>Paid Amount</th>
                                             <th>Status</th>
                                             <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <th>Action</th>
                                        <?php } ?>
                                            

                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($results){
                                            foreach($results as $value){ 
                                                $order_status = $value['Status'];
                                                $user_order_status = getOrderStatus($this->session->userdata['admin']['UserID'],$value['OrderID']);
                                                if($user_order_status){
                                                    $order_status = $user_order_status->Status;
                                                }
                                                if($order_status == 'Received'){
                                                ?>
                                            <tr id="<?php echo $value['OrderID'];?>">
                                                 <td><?php echo $value['OrderTrackID']; ?></td>
                                                <td><?php echo $value['Name']; ?></td>
                                                <td><?php echo $value['Telephone']; ?></td> 
                                                <td><?php echo $value['PaymentMethod']; ?></td>
                                                <td><?php echo $value['AmountDeposit']; ?></td> <td> 
                                                 
                                                    <select class="order_status" data-order-id = "<?php echo $value['OrderID'];?>">
                                                        <option value="Received" <?php echo ($value['Status'] == 'Received' ? 'selected' : ''); ?>>Received</option>
                                                        <option value="Dispatched" <?php echo ($value['Status'] == 'Dispatched' ? 'selected' : ''); ?>>Dispatched</option>
                                                    </select>
                                                
                                                </td>  
                                                <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                <td>

                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/detail/'.$value['OrderID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                
                                                </td>
                                                <?php } ?>   
                                            </tr>
                                                <?php
                                            }
                                        }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="material-datatables tab-pane" id="pill3">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                       <thead>
                                        <tr>

                                            <th>Track ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Payment Via</th>
                                            <th>Paid Amount</th>
                                            <th>Status</th>
                                            <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <th>Action</th>
                                        <?php } ?>

                                            

                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($results){
                                            foreach($results as $value){ 
                                                $order_status = $value['Status'];
                                                $user_order_status = getOrderStatus($this->session->userdata['admin']['UserID'],$value['OrderID']);
                                                if($user_order_status){
                                                    $order_status = $user_order_status->Status;
                                                }
                                                if($order_status == 'Dispatched'){
                                                ?>
                                            <tr id="<?php echo $value['OrderID'];?>">
                                                 <td><?php echo $value['OrderTrackID']; ?></td>
                                                <td><?php echo $value['Name']; ?></td>
                                                <td><?php echo $value['Telephone']; ?></td> 
                                                <td><?php echo $value['PaymentMethod']; ?></td>
                                                <td><?php echo $value['AmountDeposit']; ?></td>
                                                <td>
                                                <select class="order_status" data-order-id = "<?php echo $value['OrderID'];?>">
                                                        
                                                        <option value="Dispatched" <?php echo ($value['Status'] == 'Dispatched' ? 'selected' : ''); ?>>Dispatched</option>
                                                        <option value="Delivered" <?php echo ($value['Status'] == 'Delivered' ? 'selected' : ''); ?>>Delivered</option>
                                                </select>
                                                </td>
                                                <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                <td>

                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/detail/'.$value['OrderID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                
                                                </td>
                                                <?php } ?>       
                                            </tr>
                                                <?php
                                            }
                                        }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="material-datatables tab-pane" id="pill4">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Track ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Payment Via</th>
                                            <th>Paid Amount</th>
                                            <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <th>Action</th>
                                        <?php } ?>

                                            

                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($results){
                                            foreach($results as $value){ 
                                                $order_status = $value['Status'];
                                                $user_order_status = getOrderStatus($this->session->userdata['admin']['UserID'],$value['OrderID']);
                                                if($user_order_status){
                                                    $order_status = $user_order_status->Status;
                                                }
                                                if($order_status == 'Delivered'){ ?>
                                            <tr id="<?php echo $value['OrderID'];?>">
                                                 <td><?php echo $value['OrderTrackID']; ?></td>
                                                <td><?php echo $value['Name']; ?></td>
                                                <td><?php echo $value['Telephone']; ?></td> 
                                                <td><?php echo $value['PaymentMethod']; ?></td>
                                                <td><?php echo $value['AmountDeposit']; ?></td> <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                <td>

                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/detail/'.$value['OrderID']);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                
                                                </td>
                                                <?php } ?>    
                                            </tr>
                                                <?php
                                            }
                                        }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    
$(document).ready(function() {
        $('.datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('.datatables').DataTable();

        

        $('.card .material-datatables label').addClass('form-group');



        
    });

</script>

<script>
    $( document ).ready(function() {
            $('.order_status').on('change',function(){
                    if (confirm("Are you sure you want to change status ?")) {
                        $.blockUI({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });

                        $.ajax({
                            type: "POST",
                            url: base_url + 'cms/order/action',
                            data: {
                                'id': $(this).attr('data-order-id'),
                                'form_type': 'change_status',
                                'value' : $(this).val()
                            },
                            dataType: "json",
                            cache: false,
                            //async:false,
                            success: function (result) {

                                if (result.error != false) {
                                    showError(result.error);
                                } else {
                                    showSuccess(result.success);
                                    
                                }


                                

                            },
                            complete: function () {
                                $.unblockUI();
                            }
                        });
                        return true;
                    } else {
                        return false;
                    }
            });
    });
</script>
