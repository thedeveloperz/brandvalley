<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">money</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Registration Fee</h4>
                        <div class="toolbar">
                            
                        </div>
                        <div class="row alert alert-success">
                            <div class="col-md-12">
                                <p>Please pay registration fee before publish product.If you already paid then please wait for approval.</p>
                            </div>
                        </div>
                       
                         <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate autocomplete="off">
                                <input type="hidden" name="form_type" value="save">
                                <div class="row">
                                    <div class="col-sm-10 checkbox-radios">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Type" checked="true" value="customer"><span class="circle"></span><span class="check"></span> Signup as Customer ($50)
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Type" value="employee"><span class="circle"></span><span class="check"></span> Signup as Employee ($10)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Pay via</label>
                                            <select name="PayVia" class="pay_via selectpicker" data-style="select-with-transition" title="" data-size="7" id="payvia" required>
                                               <option value="jazzcash">Jazz Cash</option>
                                                <option value="easypesa">Easy Pesa</option>
                                                <option value="upesa">U Pesa</option>
                                                <option value="ublomni">UBL Omni</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="nic_number">NIC Number</label>
                                            <input type="text" name="Cnic" required  class="form-control" id="nic_number" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="amount_deposit">Amount Deposit</label>
                                                    <input type="text" name="Amount"  class="form-control" id="amount_deposit" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="transaction_id">Transaction ID</label>
                                                    <input type="text" name="TransactionID"  class="form-control" id="transaction_id" autocomplete="off">
                                                </div>
                                            </div>
                                </div>
                                <div class="row">
                                            <!-- <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="amt_deposit">Amount Deposit</label>
                                                    <input type="text" name="amount_deposit"  class="form-control" id="amt_deposit" autocomplete="off">
                                                </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group label-floating is-focused">
                                                    <label class="control-label" for="deposit_date">Deposit Date</label>
                                                    <input type="date" name="DepositDate" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group text-left m-b-0">
                                            <button class="btn btn-info" type="submit">
                                                <?php echo lang('submit');?>
                                            </button>
                                </div>
                        </form>


                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
