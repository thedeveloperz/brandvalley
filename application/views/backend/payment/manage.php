<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-tabs">
                    <ul class="nav nav-pills nav-pills-warning">
                        <?php if( $this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2) { ?>
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Pending Registation Payments</a>
                            </li>
                            <li class="">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false">Approved Registration Payments</a>
                            </li>
                        <?php } ?>
                        <li class="<?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'active' : '');?>">
                            <a href="#pill3" data-toggle="tab" aria-expanded="<?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'true' : 'false');?>">Pending Publish Product Payments</a>
                        </li>
                        <li class="">
                            <a href="#pill4" data-toggle="tab" aria-expanded="false">Approved Publish Product Payments</a>
                        </li>
                    </ul>
                </div>
                <div class="card tab-card">
<!--                    <div class="card-header card-header-icon" data-background-color="purple">-->
<!--                        <i class="material-icons">assignment</i>-->
<!--                    </div>-->
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4> <?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'Total Earning = '.$total_earning.'' : '');?>
                        <form method="post" action="" class="dates-form">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="label-control">Start Date</label>
                                        <input type="text" name="start_date" class="form-control datepicker"  value="<?php echo (isset($post_data['start_date']) ? date('m/d/Y',strtotime($post_data['start_date'])) : '');?>">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="label-control">End Date</label>
                                        <input type="text" name="end_date" class="form-control datepicker"  value="<?php echo (isset($post_data['end_date']) ? date('m/d/Y',strtotime($post_data['end_date'])) : '');?>">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="tab-content">
                            <?php if( $this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2) { ?>
                            <div class="material-datatables tab-pane active" id="pill1">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Transaction ID</th>

                                            <th>Employee</th>
                                            

<!--                                            <th>Payment Type</th>-->
                                            <th>Payment Via</th>

                                            <th>Cnic</th>
                                            <th>Amount To Pay</th>
                                            <th>Paid Amount</th>

                                            <th>DepositDate</th>
<!--                                            <th>Status</th>-->
                                            <?php if(checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                  <th><?php echo lang('actions');?></th>
                                             <?php } ?>

                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($pending_registrations){
                                            foreach($pending_registrations as $value){ ?>
                                                <tr id="<?php echo $value->PaymentID;?>">
                                                    <td><?php echo $value->TransactionID; ?></td>
                                                    <td><?php echo $value->FullName; ?></td>
<!--                                                    <td>--><?php //echo $value->PaymentType; ?><!--</td>-->
                                                    <td><?php echo $value->PaymentVia; ?></td>
                                                    <td><?php echo $value->Cnic; ?></td>
                                                    <td><?php echo $value->AmountToPaid; ?></td>
                                                    <td><?php echo $value->Amount; ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($value->DepositDate));?>
                                                        </td>
<!--                                                    <td>--><?php //echo $value->Status; ?><!--</td>    -->
                                                     <?php if(checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanView')){?> 
                                                    <td><a href="javascript:void(0);" class="approved_p" data-id="<?php echo $value->PaymentID;?>"> Click to approved</a></td>
                                                <?php } ?>
                                                     
                                                </tr>
                                                <?php
                                            }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="material-datatables tab-pane" id="pill2">
                                <div class="material-datatables">
                                    <table class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Transaction ID</th>

                                            <th>Employee</th>
                                            

<!--                                            <th>Payment Type</th>-->
                                            <th>Payment Via</th>

                                            <th>Cnic</th>
                                           
                                            <th>Amount To Pay</th>
                                            <th>Paid Amount</th>

                                            <th>DepositDate</th>
<!--                                            <th>Status</th>-->


                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($approved_registrations){
                                            foreach($approved_registrations as $value){ ?>
                                                <tr id="<?php echo $value->PaymentID;?>">
                                                    <td><?php echo $value->TransactionID; ?></td>
                                                    <td><?php echo $value->FullName; ?></td>
<!--                                                    <td>--><?php //echo $value->PaymentType; ?><!--</td>-->
                                                    <td><?php echo $value->PaymentVia; ?></td>
                                                    <td><?php echo $value->Cnic; ?></td>
                                                    
                                                    <td><?php echo $value->AmountToPaid; ?></td>
                                                    <td><?php echo $value->Amount; ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($value->DepositDate));?>
                                                        </td>
<!--                                                    <td>--><?php //echo $value->Status; ?><!--</td>    -->


                                                     
                                                </tr>
                                                <?php
                                            }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="material-datatables tab-pane <?php echo (($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) ? 'active' : '');?>" id="pill3">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Transaction ID</th>

                                            <th>Employee</th>
                                            

<!--                                            <th>Payment Type</th>-->
                                            <th>Payment Via</th>

                                            <th>Cnic</th>
                                            <th>Package Title</th>
                                            <th>Amount To Pay</th>
                                            <th>Paid Amount</th>

                                            <th>DepositDate</th>
<!--                                            <th>Status</th>-->

                                            <?php if(checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                  <th><?php echo lang('actions');?></th>
                                             <?php } ?>
                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($pending_products){
                                            foreach($pending_products as $value){ ?>
                                                <tr id="<?php echo $value->PaymentID;?>">
                                                    <td><a href="<?php echo base_url('cms/product/edit/'.$value->CommonID);?>"><?php echo $value->TransactionID; ?></a></td>
                                                    <td><?php echo $value->FullName; ?></td>
<!--                                                    <td>--><?php //echo $value->PaymentType; ?><!--</td>-->
                                                    <td><?php echo $value->PaymentVia; ?></td>
                                                    <td><?php echo $value->Cnic; ?></td>
                                                    <td><?php echo $value->PackageTitle; ?></td>
                                                    <td><?php echo $value->AmountToPaid; ?></td>
                                                    <td><?php echo $value->Amount; ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($value->DepositDate));?>
                                                        </td>
                                                   <?php if(checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(47,$this->session->userdata['admin']['UserID'],'CanView')){?>     
<!--                                                    <td>--><?php //echo $value->Status; ?><!--</td>    -->
                                                    <td><a href="javascript:void(0);" class="approved_p" data-id="<?php echo $value->PaymentID;?>"> Click to approved</a></td>
                                                <?php } ?>

                                                     
                                                </tr>
                                                <?php
                                            }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="material-datatables tab-pane" id="pill4">
                                <div class="material-datatables">
                                    <table  class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>

                                            <th>Transaction ID</th>

                                            <th>Employee</th>
                                            

<!--                                            <th>Payment Type</th>-->
                                            <th>Payment Via</th>

                                            <th>Cnic</th>
                                            <th>Package Title</th>
                                            <th>Amount To Pay</th>
                                            <th>Paid Amount</th>
                                        <?php if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){?>
                                            <th>Earning</th>
<?php } ?>
                                            <th>DepositDate</th>
<!--                                            <th>Status</th>-->


                                            

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if($approved_products){
                                            foreach($approved_products as $value){ ?>
                                                <tr id="<?php echo $value->PaymentID;?>">
                                                    <td><a href="<?php echo base_url('cms/product/edit/'.$value->CommonID);?>"><?php echo $value->TransactionID; ?></a></td>
                                                    <td><?php echo $value->FullName; ?></td>
<!--                                                    <td>--><?php //echo $value->PaymentType; ?><!--</td>-->
                                                    <td><?php echo $value->PaymentVia; ?></td>
                                                    <td><?php echo $value->Cnic; ?></td>
                                                    <td><?php echo $value->PackageTitle; ?></td>
                                                    <td><?php echo $value->AmountToPaid; ?></td>
                                                    <td><?php echo $value->Amount; ?></td>
                                                <?php if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){?>
                                                        <td><?php echo (($value->DollerSystemID == 2 || $value->DollerSystemID == 3) ?  $value->Amount/2 : 0); ?></td>
                                                    <?php  } ?>
                                                    <td><?php echo date('d-m-Y',strtotime($value->DepositDate));?>
                                                        </td>
<!--                                                    <td>--><?php //echo $value->Status; ?><!--</td>    -->


                                                     
                                                </tr>
                                                <?php
                                            }

                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    
$(document).ready(function() {
        $('.datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('.datatables').DataTable();

        

        $('.card .material-datatables label').addClass('form-group');



        $('.approved_p').on('click',function(){
            if (confirm("Are you sure you want to approve this payment?")) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/payment/action',
                    data: {
                        'id': $(this).attr('data-id'),
                        'form_type': 'approved'
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function (result) {

                        if (result.error != false) {
                            showError(result.error);
                        } else {
                            showSuccess(result.success);
                            $('#'+$(this).attr('data-id')).remove();
                        }


                        

                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
                return true;
            } else {
                return false;
            }

        });
    });

</script>