<!doctype html>
<html lang="en">

<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />    
<!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/backend/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url();?>assets/backend/images/favicon.png" type="image/x-icon">
    <!-- App title -->
    <title>
        <?php echo $site_setting->SiteName; ?>
    </title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet" />
    
    <?php //if (strpos(base_url(uri_string()), '/cms/') == true) { ?>
        <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet" />
    <?php //} ?>



    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/barrating/fontawesome-stars.min.css">


    <!--   Core JS Files   -->
    <script src="<?php echo base_url();?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/plugins/response.min.js" type="text/javascript"></script>
    

    <script src="<?php echo base_url();?>assets/plugins/barrating/jquery.barrating.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/app.js" type="text/javascript"></script>

    <?php //if (strpos(base_url(uri_string()), '/cms/') == true) { ?>
        <script src="<?php echo base_url();?>assets/backend/js/material.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/backend/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <?php //} ?>



    <!-- Customzed css by Asif -->
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" />
    <!-- jquery autocomplete-->

      <script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/typeahead.js"></script>


</head>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('
    are_you_sure ');?>';
</script>
<style>
    .validate_error {
        border: 1px solid red;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }
</style>

<body>
    <header id="header" class="type_6">
        <div class="bottom_part">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-5 col-sm-5">
                        <a href="<?php echo base_url();?>" class="logo">
                            <img src="https://brandsvalley.net/assets/web/images/logo.png" alt="Brands Valley">
                        </a>
                    </div>
                    <div class="col-lg-7 col-md-8 col-sm-8 location-selector-inputs">

                        <form method="post" action="<?php echo base_url('index');?>">

                            <ul class="formsection pull-right">
                                <li class="col-lg-4 col-md-4 col-sm-12 padding-5px">
                                    <select class="open_categories  active" id="country" name="CountryID">
                                        <option value="">Select Country</option>
                                       <?php foreach ($countries as $key => $value) { ?>
                                                <option value="<?php echo $value->CountryID; ?>" class="animated_item"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                            <?php }?>
                                    </select>
                                </li>
                                <li class="col-lg-4 col-md-4 col-sm-12 padding-5px">
                                    <select class="open_categories " id="states" name="StateID">

                                        <option value="">Select State</option>

                                    </select>
                                </li>
                                <li class="col-lg-3 col-md-3 col-sm-12 padding-5px">
                                    <select class="open_categories" id="city" name="CityID">
                                        <option value="">Select City</option>
                                    </select>
                                </li>
                                <li class="col-lg-1 col-md-1 col-sm-12 padding-5px">
                                    <button type="submit" class="btn btn-info go-btn" name="goForCookies" id="goForCookies" style="padding: 10px 18px;">
                                        <i class="fa fa-location-arrow"></i>
                                    </button>
                                </li>

                            </ul>

                        </form>

                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 social-icons-wrapper">

                        <ul class="social_btns pull-right">
                            <li>
                                <a target="_blank" href="https://www.facebook.com/BusinessUp1/" class="icon_btn middle_btn social_facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/111942499114878334046" target="_blank" class="icon_btn middle_btn social_googleplus"><i class="fa fa-google"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/brands_valley" target="_blank" class="icon_btn middle_btn  social_twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/brandsvalleyinternational/" target="_blank" class="icon_btn middle_btn social_instagram"><i class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- row -->
            </div>
            <!-- container-fluid -->

        </div>
        <!-- Bottom_part -->


        <div class="container responsive-search">
            <div class="row">
                <div class="col-sm-12">
                    <form class="navbar-form navbar-left" role="search" action="<?php echo base_url('product/details');?>" method="post">
                        <div class="form-group">
                            <div class="search-box">
                                <select name="SearchCategoryID" class="search-cat trans-select" id="SearchCategoryID">
                                    <option value="">Categories</option>
                                    <?php if($search_categories){
                                        foreach ($search_categories as $key => $c_inner) {
                                            echo '<option value="'.$c_inner->CategoryID.'" '.((isset($post_data['SearchCategoryID']) && $post_data['SearchCategoryID'] == $c_inner->CategoryID ) ? 'selected' : '' ).'>'.$c_inner->Title.'</option>';
                                        }
                                    } ?>
                                </select>
                                <input type="text" class="input-search txtCategory" placeholder="Search" id="txtCategory" name="txtCategory" value="<?php echo (isset($post_data['txtCategory']) ? $post_data['txtCategory']: '' );?>" autocomplete="off" >
                                <button type="submit" class="btn-search btn-default">
                                    <i class="fa fa-search color-white" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </header>



<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default" <?php if (strpos(base_url(uri_string()), '/cms/') == true) { echo "id='cms-navbar'"; }?> role="navigation">
  
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- <a class="navbar-brand" href="#">Brand</a> -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li>
                    <div class="smllogo">
                        <div class="clearfix">
                            <div class="pull-left"><i class="fa fa-map-marker font-vvbig" aria-hidden="true"></i></div>
                            <div class="pull-left">
                                <div>Deliver to</div>
                                <div class="font-medium">Pakistan</div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="shopby-dropwon dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span>shop by</span>
                    <div class="font-medium">Departments <b class="caret"></b></div>
                  </a>
                  <?php if($categories){ ?>
                  <ul class="dropdown-menu has-subnav">
                    <i class="arrow up"></i>
                    <?php foreach ($categories as $key => $value) { ?>
                    <li class="subnav-dropdown">
                        <a href="#"><?php echo $value->Title; ?></a>
                        <?php $getSubCategories = getSubCategories($value->CategoryID,'EN');
                        if($getSubCategories){
                            ?>
                                
                            
                        <div class="subnav-box">
                            <ul class="subnav-ul clearfix">
                                <?php foreach ($getSubCategories as $key => $sub_category) { ?>
                                <li><a href="#"><?php echo $sub_category->Title; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    </li>
                <?php } ?>
                    
                  </ul>
                </li>
              </ul>
          <?php } ?>
              <form class="navbar-form navbar-left" role="search" action="<?php echo base_url('product/details');?>" method="post">
                <div class="form-group">
                  <div class="search-box">
                    <select name="SearchCategoryID" class="search-cat trans-select" id="SearchCategoryID">
                        <option value="">Select Categories</option>
                        <?php if($search_categories){
                            foreach ($search_categories as $key => $c_inner) {
                             echo '<option value="'.$c_inner->CategoryID.'" '.((isset($post_data['SearchCategoryID']) && $post_data['SearchCategoryID'] == $c_inner->CategoryID ) ? 'selected' : '' ).'>'.$c_inner->Title.'</option>';
                            }
                        } ?>
                    </select>
                    <input type="text" class="input-search txtCategory" placeholder="Search" id="txtCategory" name="txtCategory" value="<?php echo (isset($post_data['txtCategory']) ? $post_data['txtCategory']: '' );?>" autocomplete="off" >
                    <button type="submit" class="btn-search btn-default">
                        <i class="fa fa-search color-white" aria-hidden="true"></i>
                    </button>
                  </div>  
                </div>
              </form>
              <ul class="acc-nav nav navbar-nav navbar-right">
              <?php if($this->session->userdata('admin')){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
<!--                            <i class="fa fa-bars" aria-hidden="true"></i>-->
                            Account <b class="caret"></b>
                        </a>
                      <ul class="dropdown-menu">
                        <li><a href="#">My Profile</a></li>
                        <li><a href="<?php echo base_url('cms/product')?>">Publish Ad</a></li>
                        <li><a href="#">Payments</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url('account/logout'); ?>">Logout</a></li>
                      </ul>
                    </li>
                      <li>
                          <a href="#" class="clearfix">
                              <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                              <span class="cart-count">10</span>
                          </a>
                      </li>
                 <?php } else { ?>
                  <li class="top-nav-link login_link">
                      <a href="<?php echo base_url('/account/login'); ?>">
                          Login
                      </a>
                  </li>
                  <li class="top-nav-link signup_link">
                      <a href="<?php echo base_url('/account/signup'); ?>">
                          Signup
                      </a>
                  </li>
                  <?php } ?>
              </ul>
            </div><!-- /.navbar-collapse -->
    </nav>
    </div>
</div>









    <div class="wrapper">
    <div class="overlay"></div>    
<script>
    $(document).ready(function () {

    // show overlay in desktop views
    var viewPortWidth = Response.viewportW();
    if( viewPortWidth => 768 ){
        $(".shopby-dropwon > .dropdown-toggle, .has-subnav").hover(function(){
                $('.overlay').show();
            }, function() { $('.overlay').hide() });  

       // hoverd menu
           $(function(){
                $(".dropdown").hover(            
                function() {
                    $('.dropdown-menu', this).stop( true, true ).show();
                    $(this).toggleClass('open');          
                },
                function() {
                    $('.dropdown-menu', this).stop( true, true ).hide();
                    $(this).toggleClass('open');               
            });
        });       
    };

    // mobile side
    // if( viewPortWidth <= 767 ){
    //     $('li.subnav-dropdown > a').on('click', function(){
    //         $('.subnav-box').removeClass('show');
    //         $(this).find('.subnav-box').show();
    //     });
    // }




    // $(".dropdown-toggle").hover(function(){
    //     $(this).trigger('hover');
    //     console.log("hoverd")
    // });

    // sub menu clickable in mobile views
    // if(Response.viewportW() <= 767){
    //     $(".subnav-dropdown > a").on('click', function(){
    //         console.log(Response.viewportW())
    //         $(this).find(".subnav-box").toggle();
    //     });
    // };


    $('#country').on('change',function(){

            var CountryID = $(this).val();

            if(CountryID == '')
            {
                $('#StateID').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#states').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });
       $('#states').on('change',function(){

            var StateID = $(this).val();

            if(StateID == '')
            {
                $('#CityID').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#city').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });



       });
</script>
<script>
    $(document).ready(function () {
       
        $('.txtCategory').typeahead({
             
            source: function (query, result) {
                $.ajax({
                    url: "<?php echo base_url('index/getCateroyProduct');?>/"+$('#SearchCategoryID').val(),
                    data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        result($.map(data, function (item) {
                            return item.label;
                        }));
                    }
                });
            }
        });


    });
</script>