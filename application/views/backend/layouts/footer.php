

</div>
</div>




<div class="main-footer">
		<div class="container-fluid">
			<div class="space-left-right">
				<div class="dis-table">
					<div class="dis-cell footer-box">
						<h3>About Us</h3>
						<p>Brands Valley is an advertising platform targetting clients either local or international who are looking for all kinds of branding. Brands Valley looks forward to companies or local businesses which are very quality conscious and we are aiming to provide extremely powerful concepts and techniques of advertisements for our clients.</p>
						<div class="footer-logo">
							<img src="<?php echo base_url(); ?>assets/images/logo_bv.png">
						</div>
					</div>
					<div class="dis-cell footer-box">
						<h3>Legal</h3>
						<ul class="footer-nav">
							<li><a href="<?php echo base_url(); ?>index/aboutbv">About BV</a></li>
							<li><a href="<?php echo  base_url(); ?>index/terms">Terms of Services</a></li>
							<li><a href="<?php echo  base_url(); ?>index/policy">Privacy Policy</a></li>
						</ul>
					</div>
<!--					<div class="dis-cell footer-box">-->
<!--						<h3>Make Money with Us</h3>-->
<!--						<ul class="footer-nav">-->
<!--							<li><a href="">Sell on BV</a></li>-->
<!--							<li><a href="">Sell Your Services</a></li>-->
<!--							<li><a href="">Investor Relations</a></li>-->
<!--						</ul>-->
<!--					</div>-->
<!--					<div class="dis-cell footer-box">-->
<!--						<h3>BV Payment Products</h3>-->
<!--						<ul class="footer-nav">-->
<!--							<li><a href="">Sell on BV</a></li>-->
<!--							<li><a href="">Sell Your Services</a></li>-->
<!--							<li><a href="">Investor Relations</a></li>-->
<!--							<li><a href="">BV Devices</a></li>-->
<!--						</ul>-->
<!--					</div>-->
					<div class="dis-cell footer-box">
						<h3>Contact</h3>
						<ul class="footer-nav">
							<li class="location-icon">
								<h6>Address:</h6>
								<p>House no.34 street no.5 Main Bazar Garhi Shahu Lahore</p>
							</li>
							<li class="receiver-icon">
								<h6>Call Free</h6>
								<p>0332 4001729 / 0304 4712382</p>
							</li>
							<li class="fax-icon">
								<h6>Email:</h6>
								<p>info@brandsvalley.net</p>
							</li>
						
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom text-center">
			<p>&copy;</p>
			<ul class="bottombar">
				<li><a href="<?php echo base_url(); ?>aboutbv">Brands Valley</a></li>
				<li>Powered by <a href="https://herryb.com"><img width="125" src="https://herryb.com/wp-content/uploads/2018/10/Logo-resize.jpeg"></a> </li>
                <li><p>All Rights Reserved.</p></li>
 			</ul>
		</div>
	</div>


</body>
<script type="text/javascript">
    $(document).ready(function () {
                   
       $('#CountryID').on('change',function(){
           
            var CountryID = $(this).val();

            if(CountryID == '')
            {
                $('#StateID').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/state/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateID').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });


    

       $('#StateID').on('change',function(){
           
            var StateID = $(this).val();

            if(StateID == '')
            {
                $('#CityID').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityID').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       $('#CategoryID').on('change',function(){
           
            var CategoryID = $(this).val();

            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#SubCategoryID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });
       $('#DollersystemID').on('change',function(){
           
            var DollersystemID = $(this).val();

            if(DollersystemID != '')
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            
            
                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/package/getAllPackages',
                    data: {
                        'DollersystemID': DollersystemID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#DurationPackage').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
           
       });

       // activate datepicker fields
        $('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                inline: true
            }
         });

       <?php if(!get_cookie('location')){ ?>
        //setTimeout(function(){ getLocation(); }, 1000);// i am not calling this function because google api is paid
                

       <?php 
       }
       ?>

       
    });

    function getLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }
    function showPosition(position) {

        $.ajax({
            type: "POST",
            url: base_url + 'index/setLocation',
            data: {
                'Latitude': position.coords.latitude,
                'Longitude' : position.coords.longitude
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function(result) {

               

            },
            complete: function() {
                
            }
        });

         
    }
</script>
<!-- Forms Validations Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url();?>assets/backend/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url();?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url();?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.tagsinput.js"></script>

<?php //if (strpos(base_url(uri_string()), '/cms/') == true) { ?>
    <script src="<?php echo base_url();?>assets/backend/js/material-dashboard.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/demo.js"></script>
<?php //} ?>
<script src="<?php echo base_url();?>assets/backend/js/script.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.blockUI.js"></script>


<script src="<?php echo base_url();?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.fileuploads.init.js"></script>

<?php if($this->session->flashdata('message')){ ?>
    <script>

        $( document ).ready(function() {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:32:16 GMT -->
</html>
