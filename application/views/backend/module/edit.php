<?php
$option = '';
if(!empty($modules)){
    foreach($modules as $module){
        $option .= '<option value="'.$module->ModuleID.'" '.((isset($result[0]->ParentID) && $result[0]->ParentID == $module->ModuleID) ? 'selected' : '').'>'.$module->Title.' </option>';
    } }
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        if($key == 0){
            $common_fields = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ParentID'.$key.'">'.lang('choose_parent_module').'</label>
                                                                <select id="ParentID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="ParentID">
                                                                    <option value="0">'.lang('parent_module').'</option>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
            $common_fields2 = '<div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Slug">'.lang('slug').'</label>
                                                                <input type="text" name="Slug" parsley-trigger="change" required  class="form-control" id="Slug" value="'.((isset($result[$key]->Slug)) ? $result[$key]->Slug : '').'">
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="IconClass">'.lang('icon_class').'</label>
                                                                <input type="text" name="IconClass" parsley-trigger="change"  class="form-control" id="IconClass" value="'.((isset($result[$key]->IconClass)) ? $result[$key]->IconClass : '').'">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                    <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                    
                                                    
                                                    ';
        }

                    $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                   
                                                    <div class="row">
                                                        '.$common_fields.'
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    '.$common_fields2.'
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';





    }
}


?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->Title;?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-10">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>