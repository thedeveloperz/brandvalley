<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }

$countries_opts = '';
if(!empty($countries)){
    foreach($countries as $country){
        $countries_opts .= '<option value="'.$country->CountryID.'" '.((isset($result[0]->CountryID) && $result[0]->CountryID == $country->CountryID) ? 'selected' : '').'>'.$country->Title.' </option>';
    }
}

$cities_opts = '';
if(!empty($cities_opts)){
    foreach($cities_opts as $city){
        $countries_opts .= '<option value="'.$city->CountryID.'" '.((isset($result[0]->CountryID) && $result[0]->CountryID == $country->CountryID) ? 'selected' : '').'>'.$country->Title.' </option>';
    }
}

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';
        
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="RoleID" disabled>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Email">'.lang('email').'</label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div> 
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Phone">Phone</label>
                                                                <input type="text" name="Phone" parsley-trigger="change" required  class="form-control" id="Email" value="' . ((isset($result[0]->Phone)) ? $result[0]->Phone : '') . '">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row">
         <div class="col-md-6">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div></div>';

 if($result[0]->RoleID != 1 && $result[0]->RoleID != 2) {
     

     $common_fields5 = '<div class="row">
                                        <div class="col-md-6">
                                            <label class="col-sm-2 label-on-left lbl-gender">Gender</label>
                                            <div class="col-sm-10 checkbox-radios">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="Gender" ' . ($result[0]->Gender == "Male" ? "checked" : "") . ' value="Male"><span class="circle"></span><span class="check"></span> Male
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="Gender" value="Female" ' . ($result[0]->Gender == "Female" ? "checked" : "") . '><span class="circle"></span><span class="check"></span> Female
                                                    </label>
                                                </div>
                                                <div class="radio">

                                                </div>
                                            </div>
                                            </div>
                                        
                                        </div>
                                        ';


     $common_fields6 = '<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="'. lang('choose_country') . '" data-size="7" id="CountryIDSignup" required>
                                                        '.$countries_opts.'
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="StateID" class="selectpicker" data-style="select-with-transition" title="'.lang('choose_state').'" data-size="7" id="StateIDSignup"  required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">&nbsp;</label>
                                                    <select name="CityID" class="selectpicker" data-style="select-with-transition"  title="'.lang('choose_city').'" data-size="7" id="CityIDSignup" required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>';





            }
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="FullName" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->FullName)) ? $result[$key]->FullName : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="row">
                                                        
                                                        
                                                         '.$common_fields2.'
                                                    </div>
                                                    '.$common_fields4.'
                                                    
                                                   

                                                   '.$common_fields5.'
                                                   
                                                   
                                                   '.$common_fields6.'
                                                    '.$common_fields3.'

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title">Edit <?php echo $result[0]->FullName;?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-10">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <script>
        $(document).ready(function () {
            var CountryID =  <?php echo $result[0]->CountryID ?>;
            var SelectedStateID = <?php echo $result[0]->StateID ?>;
            var SelectedCityID = <?php echo $result[0]->CityID ?>;

            function getStates(CountryID) {

                if(CountryID == '')
                {
                    $('#StateIDSignup').html('<option value=""><?php echo lang("choose_state");?></option>');
                }
                else
                {
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });


                    $.ajax({
                        type: "POST",
                        url: base_url + 'index/getCountryStates',
                        data: {
                            'CountryID': CountryID,
                            'SelectedStateID': SelectedStateID
                        },
                        dataType: "json",
                        cache: false,
                        //async:false,
                        success: function(result) {

                            $('#StateIDSignup').html(result.html);
                            $(".selectpicker").selectpicker('refresh');
                        },
                        complete: function() {
                            $.unblockUI();
                        }
                    });
                }
            };

        // on change
            $('#CountryIDSignup').on('change',function(){
                var CountryID = $(this).val();
                getStates(CountryID);
            });

        // run on load to show selected state
            getStates(CountryID);


        function getCities(StateID) {

            if(StateID == '')
            {
                $('#CityIDSignup').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID,
                        'SelectedCityID': SelectedCityID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }
        }

        // runs on change
            $('#StateIDSignup').on('change',function(){
                var StateID = $(this).val();
                getCities(StateID);

            });

        // runds on load
            getCities(SelectedStateID);

        });
    </script>