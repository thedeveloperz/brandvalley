<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
$images = '';
$videos = '';
if (!empty($site_images)) {

    $images .= '<div class="form-group clearfix">
       <div class="col-sm-12 padding-left-0 padding-right-0">
          <div class="jFiler jFiler-theme-dragdropbox">
             <div class="jFiler-items jFiler-row">
                <ul class="jFiler-items-list jFiler-items-grid">';
                    foreach($site_images as $img){
                   $images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                      <div class="jFiler-item-container">
                         <div class="jFiler-item-inner">
                            <div class="jFiler-item-thumb">
                               <div class="jFiler-item-status"></div>
                               <div class="jFiler-item-info"></div>
                               <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false"></div>
                            </div>
                            <div class="jFiler-item-assets jFiler-row">
                               <ul class="list-inline pull-left">
                                  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                               </ul>
                               <ul class="list-inline pull-right">
                                  <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                               </ul>
                            </div>
                         </div>
                      </div>
                   </li>';
                     } 
                   
$images .= '</ul>
             </div>
          </div>
       </div>
    </div>';

    
}
if (!empty($site_videos)) {
    foreach($site_videos as $vkey=>$vid){
        $videos .= '<div class="row fieldGroup">
                    <div class="input-group col-md-12">
                        <div class="col-md-6">';
                        if($vkey == 0)
                        {
                            $videos .= '<label>'.lang('video_url').'</label>';
                        }
                                                   
                        $videos .= '<input type="text" name="VideoURL[]" class="form-control" value="'.$vid['VideoURL'].'" placeholder="'.lang('youtube_video').'" '.($vkey == 0 ? 'required' : '').'/>
                        </div>';
                        if($vkey > 0)
                        {
                            $videos .= '<div class="col-md-2">
                                            <div class="input-group-addon" style="padding:0px !important; width: auto !important;"> 
                                                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                            </div>
                                        </div>';
                        }
                    $videos .= '</div>
                </div>';

    }
}
if(!empty($languages)){
    foreach($languages as $key => $language){

        $common_fields_af_comp_name = '';
        $common_fields_bff_title = '';
        $common_fields = '';
        $common_fields2 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $country_dropdown = '';
        $state_dropdown = '';
        $city_dropdown = '';
        $common_no_fields = '';
        $common_check_fields = '';
        if($key == 0){
            
            foreach($categories as $category)
            {
                $category_dropdown .= '<option '.($category->CategoryID == $result[$key]->CategoryID ? 'selected="selected"' : '').' value="'.$category->CategoryID.'">'.$category->Title.'</option>';
            }
            foreach($subcategories as $SubCategory)
            {
                $sub_category_dropdown .= '<option '.($SubCategory->CategoryID == $result[$key]->SubCategoryID ? 'selected="selected"' : '').' value="'.$SubCategory->CategoryID.'">'.$SubCategory->Title.'</option>';
            }
            foreach($countries as $country)
            {
                $country_dropdown .= '<option '.(in_array($country->CountryID, $selectedCountries) ? 'selected="selected"' : '').' value="'.$country->CountryID.'">'.$country->Title.'</option>';
            }
            /*foreach($states as $state)
            {
                $state_dropdown .= '<option '.($state->StateID == $result[$key]->State ? 'selected="selected"' : '').' value="'.$state->StateID.'">'.$state->Title.'</option>';
            }
            foreach($cities as $city)
            {
                $city_dropdown .= '<option '.($city->CityID == $result[$key]->City ? 'selected="selected"' : '').' value="'.$city->CityID.'">'.$city->Title.'</option>';
            }*/
        $common_fields2 = '<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price"> '.  lang('price') . ' <span class="red-alert">(Required)</span></label>
                                        <input type="text" name="Price" required  class="form-control" id="Price" value=" ' . $result[$key]->Price . '">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CategoryID" class="form-control" id="CategoryID" required>
                                            <option value="">'.lang('choose_category').'</option>
                                            '.$category_dropdown.'
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="SubCategoryID" class="form-control" id="SubCategoryID" required>
                                            <option value="">'.lang('choose_sub_category').'</option>
                                            '.$sub_category_dropdown.'
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="Country[]" class="selectpicker" data-style="select-with-transition" multiple title="'.lang('choose_country').'" data-size="7" id="CountryID" required>
                                            <option value="">'.lang('choose_country').'</option>
                                            '.$country_dropdown.'
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="State[]" class="selectpicker" data-style="select-with-transition" multiple title="'.lang('choose_state').'" data-size="7" id="StateID" required>
                                            <option>'.lang('choose_state').'</option>
                                            '.$states.'
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="City[]" class="selectpicker" data-style="select-with-transition" multiple title="'.lang('choose_city').'" data-size="7" id="CityID" required>
                                            <option value="">'.lang('choose_city').'</option>
                                            '.$cities.'
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>';
        $common_fields_bff_title = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email">'.lang('email').'</label>
                                        <input type="text" name="Email" required  class="form-control" id="Email" value="'.((isset($result[$key]->Email)) ? $result[$key]->Email : '').'">
                                    </div>
                                </div>';
//
//        $common_no_fields = '<div class="row">
//                                <div class="col-md-6">
//                                    <div class="form-group label-floating">
//                                        <label class="control-label" for="Contact">'.lang('contact').'</label>
//                                        <input type="text" name="Contact" required  class="form-control" id="Contact" value="'.((isset($result[$key]->Contact)) ? $result[$key]->Contact : '').'">
//                                    </div>
//                                </div>
//                                <div class="col-md-6">
//                                    <div class="form-group label-floating">
//                                        <label class="control-label" for="WhatsApp">'.lang('whats_app').'</label>
//                                        <input type="text" name="WhatsApp" required  class="form-control" id="WhatsApp" value="'.((isset($result[$key]->WhatsApp)) ? $result[$key]->WhatsApp : '').'">
//                                    </div>
//                                </div>
//                            </div>
//                            <div class="row">
//                                <div class="col-md-6">
//                                    <div class="form-group label-floating">
//                                        <label class="control-label" for="PositionNumber">'.lang('position_number').'</label>
//                                        <input type="text" name="PositionNumber" required  class="form-control" id="PositionNumber" value="'.((isset($result[$key]->PositionNumber)) ? $result[$key]->PositionNumber : '').'">
//                                    </div>
//                                </div>
//                            </div>';

        $common_check_fields = '';//'<div class="row">
//                                <div class="col-sm-3 checkbox-radios">
//                                    <div class="form-group label-floating">
//                                        <div class="checkbox">
//                                            <label for="SliderImage">
//                                                <input name="SliderImage" value="1" type="checkbox" id="SliderImage" '.((isset($result[$key]->SliderImage) && $result[$key]->SliderImage == 1) ? 'checked' : '').'/> '.lang('slider_image').'?
//                                            </label>
//                                        </div>
//                                    </div>
//                                </div>
//                                <div class="col-md-3">
//                                   <img style="height:200px;" src="' . (file_exists($result[0]->SliderImageFile) ? base_url() . $result[0]->SliderImageFile : base_url('uploads/no_image.png')) . '" >
//                                   <div class="form-group m-b-20">
//                                        <label></label><br>
//                                        <input type="file" name="SliderImageFile[]">
//                                    </div>
//                                </div>
//                                <div class="col-sm-3 checkbox-radios">
//                                    <div class="form-group label-floating">
//                                        <div class="checkbox">
//                                            <label for="Gif">
//                                                <input name="Gif" value="1" type="checkbox" id="Gif" '.((isset($result[$key]->Gif) && $result[$key]->Gif == 1) ? 'checked' : '').'/> Gif?
//                                            </label>
//                                        </div>
//                                    </div>
//                                </div>
//                                 <div class="col-md-3">
//                                   <img style="height:200px;" src="' . (file_exists($result[0]->GifImageFile) ? base_url() . $result[0]->GifImageFile : base_url('uploads/no_image.png')) . '" >
//                                    <div class="form-group m-b-20">
//                                        <label></label><br>
//                                        <input type="file" name="GifImageFile[]">
//                                    </div>
//                                </div>
//                            </div>
//                            <div class="row">
//                                <div class="col-sm-6 checkbox-radios">
//                                        <div class="form-group label-floating">
//                                            <div class="checkbox">
//                                                <label for="Globe">
//                                                    <input name="Globe" value="1" type="checkbox" id="Globe" '.((isset($result[$key]->Globe) && $result[$key]->Globe == 1) ? 'checked' : '').'/> '.lang('global').'?
//                                                </label>
//                                            </div>
//                                        </div>
//                                    </div>
//                            </div>
//                            ';
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">
                                                   
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').' <span class="red-alert">(Required)</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="CompanyUrl">'.lang('company_url').' <span class="green-alert">(Optional)</span></label>
                                                                <input type="text" name="CompanyUrl" required  class="form-control" id="CompanyUrl" value="'.((isset($result[$key]->CompanyUrl)) ? $result[$key]->CompanyUrl : '').'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Contact">Contact <span class="green-alert">(Optional)</span></label>
                                                                <input type="text" name="Contact" value="'.((isset($result[$key]->Contact)) ? $result[$key]->Contact : '').'"  class="form-control" id="Contact">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                         '.$common_fields2.'

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ShortDescription">'.lang('short_description').'</label>
                                                                <textarea class="form-control" name="ShortDescription" id="ShortDescription">'.$result[$key]->ShortDescription.'</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">'.lang('description').'</label>
                                                                <textarea class="form-control" name="Description" id="Description">'.$result[$key]->Description.'</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group m-b-20">
                                                                <label>'.lang('Image').'</label><br>
                                                                <input type="file" name="Image[]" id="filer_input1" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        '.$images.'
                                                    </div>
                                                    '.$videos.'
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <label>&nbsp;</label>
                                                            <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                                        </div>
                                                    </div>
                                                   '.$common_no_fields.'
                                                    
                                                    '.$common_check_fields.'

                                                    '.(($this->session->userdata['admin']['RoleID'] == 1 OR $this->session->userdata['admin']['RoleID'] == 2) ? '<div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>' : '' ).'
                                                    

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="modal fade" id="DeleteArticleImage" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo lang('delete'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> <?php echo lang('AreYouSureDeleteFile'); ?></div>
            </div>
            <div class="modal-footer ">
                <a type="button" class="btn btn-success delete_url" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo lang('Yes'); ?></a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> <?php echo lang('No'); ?></button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-12">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row fieldGroupCopy" style="display: none;">
    <div class="input-group col-md-12">
        <div class="col-md-6">
            <input type="text" name="VideoURL[]" class="form-control" placeholder="<?php echo lang('youtube_video');?>"/>
        </div>
        <div class="col-md-2">
            <div class="input-group-addon" style="padding:0px !important; width: auto !important;"> 
                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </div>
        </div>
    </div>
</div>

    <style type="text/css">
        .red-alert {
            color: #fe3d2e;
        }
        .green-alert {
            color: #00a200;
        }
    </style>
<script type="text/javascript">
    $(document).on('click', '.remove_image', function () {
        var image_id       = $(this).attr('data-image-id');
        var image_path       = $(this).attr('data-image-path');
        $(".delete_url").attr('data-modal-image-id', image_id);
        $(".delete_url").attr('data-modal-image-path', image_path);
        $('#DeleteArticleImage').modal('show');
    });

    $(document).on('click', '.delete_url', function () {
        var image_id       = $(this).attr('data-modal-image-id');
        var image_path       = $(this).attr('data-modal-image-path');
        var $this = $(this);
        $.ajax({
                type: "POST",
                url: '<?php echo base_url() . 'cms/' . $ControllerName . '/DeleteImage'; ?>',
                data: {
                    image_path: image_path,
                    image_id: image_id
                },
                success: function (result) {
                    $.unblockUI;
                    if (result.error != false) {
                        $("#img-"+image_id).remove();
                    }



                },
                complete: function () {
                    $('#DeleteArticleImage').modal('hide');
                    $.unblockUI();
                }
            });
    });
    $(document).ready(function() {
        //group add limit
        var maxGroup = 100;
        //add more fields group
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="row fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });
        
        //remove fields group
        $("body").on("click",".remove",function(){ 
            $(this).parents(".fieldGroup").remove();
        });
    });
</script>