<style type="text/css">
    .cal_val{
        font-size: 16px;
        font-weight: 900;
        float: right;
        color: #333;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">

                            <div class="row">
<!--                                <div class="col-md-6">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <label class="control-label" for="CompanyName">--><?php //echo lang('company_name'); ?><!--</label>-->
<!--                                        <input type="text" name="CompanyName" required  class="form-control" id="CompanyName">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?> <span class="red-alert">(Required)</span></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CompanyUrl"><?php echo lang('company_url'); ?> <span class="green-alert">(Optional)</span></label>
                                        <input type="text" name="CompanyUrl" required  class="form-control" id="CompanyUrl">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Contact"><?php echo lang('contact'); ?> <span class="green-alert">(Optional)</span></label>
                                        <input type="text" name="Contact" required  class="form-control" id="Contact">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
<!--                                <div class="col-md-6">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <label class="control-label" for="Email">--><?php //echo lang('email'); ?><!--</label>-->
<!--                                        <input type="text" name="Email" required  class="form-control" id="Email">-->
<!--                                    </div>-->
<!--                                </div>-->

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Price"><?php echo lang('price'); ?> <span class="red-alert">(Required)</span></label>
                                        <input type="text" name="Price" required  class="form-control" id="Price">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CategoryID" class="form-control" id="CategoryID" required>
                                            <option value=""><?php echo lang('choose_category');?></option>
                                            <?php foreach ($categories as $key => $value) { ?>
                                                <option value="<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="SubCategoryID" class="form-control" id="SubCategoryID" required>
                                            <option value=""><?php echo lang('choose_sub_category');?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="Country[]" class="selectpicker" data-style="select-with-transition" multiple title="<?php echo lang('choose_country');?>" data-size="7" id="CountryID" required>
                                            <?php foreach ($countries as $key => $value) { ?>
                                                <option value="<?php echo $value->CountryID; ?>"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="State[]" class="selectpicker" data-style="select-with-transition" multiple title="<?php echo lang('choose_state');?>" data-size="7" id="StateID" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="City[]" class="selectpicker" data-style="select-with-transition" multiple title="<?php echo lang('choose_city');?>" data-size="7" id="CityID" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <?php if($this->session->userdata['admin']['RoleID'] == 1 && $this->session->userdata['admin']['RoleID'] == 2)
                                { ?>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                               <?php } ?>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label red-alert" for="ShortDescription"><?php echo lang('short_description'); ?> <span class="red-alert">(Required)</span></label>
                                        <textarea class="form-control" name="ShortDescription" id="ShortDescription"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description"><?php echo lang('description'); ?> <span class="green-alert">(Optional)</span></label>
                                        <textarea class="form-control" name="Description" id="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group m-b-20">
                                        <label><?php echo lang('Image'); ?></label><br>
                                        <input type="file" name="Image[]" id="filer_input1" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row fieldGroup">
                                <div class="input-group col-md-12">
                                    <div class="col-md-6">
                                        <label><?php echo lang('video_url');?></label>
                                        <input type="text" name="VideoURL[]" class="form-control" value="" placeholder="<?php echo lang('youtube_video');?>" required/>
                                    </div>
                                </div>
                            </div>
<!--                            <div class="row">-->
<!--                                <div class="col-md-2">-->
<!--                                    <label>&nbsp;</label>-->
<!--                                    <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="row">
<!--                                <div class="col-md-6">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <label class="control-label" for="WhatsApp">--><?php //echo lang('whats_app'); ?><!--</label>-->
<!--                                        <input type="text" name="WhatsApp" required  class="form-control" id="WhatsApp">-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                            <div class="row">
<!--                                <div class="col-md-6">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <label class="control-label" for="PositionNumber">--><?php //echo lang('position_number'); ?><!--</label>-->
<!--                                        <input type="text" name="PositionNumber" required  class="form-control" id="PositionNumber">-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
<!--                            <div class="row">-->
<!--                                -->
<!--                            </div>-->

<!--                            <div class="row">-->
<!--                                <div class="col-sm-3 checkbox-radios">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="checkbox">-->
<!--                                            <label for="SliderImage">-->
<!--                                                <input name="SliderImage" value="1" type="checkbox" id="SliderImage" class="show_div"/> --><?php //echo lang('slider_image'); ?><!--? (20$ extra) (Image Size 1386 * 369)-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="col-md-3 slider_image_div">-->
<!--                                    <div class="form-group m-b-20">-->
<!--                                        <label>--><?php //echo lang('Image'); ?><!--</label><br>-->
<!--                                        <input type="file" name="SliderImageFile[]">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="col-sm-3 checkbox-radios">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="checkbox">-->
<!--                                            <label for="Gif">-->
<!--                                                <input name="Gif" value="1" type="checkbox" id="Gif" class="show_div"/> Is Gif? (10$ extra) (Image Size 1386 * 169)-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="col-md-3 gif_image_div">-->
<!--                                    <div class="form-group m-b-20">-->
<!--                                        <label>--><?php //echo lang('Image'); ?><!--</label><br>-->
<!--                                        <input type="file" name="GifImageFile[]">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                -->
<!--                            </div>-->
                            
<!--                            <div class="row">-->
<!--                                <div class="col-sm-6 checkbox-radios">-->
<!--                                    <div class="form-group label-floating">-->
<!--                                        <div class="checkbox">-->
<!--                                            <label for="Globe">-->
<!--                                                <input name="Globe" value="1" type="checkbox" id="Globe"/> --><?php //echo lang('global'); ?><!--?-->
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                -->
<!--                            </div>-->

                            <?php 
                                if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2)
                                {
                                    $option = '';
                                    if(!empty($doller_systems)){ 
                                        foreach($doller_systems as $doller_system){ 
                                            $option .= '<option value="'.$doller_system->DollerSystemID.'">'.$doller_system->Title.' </option>';
                                        } 
                                    }
                            ?>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="DollersystemID"><?php echo lang('i_want_to_publish'); ?></label>
                                                <select name="DollersystemID" class="form-control" id="DollersystemID">
                                                        <option selected="" value=""></option>
                                                        <?php echo $option; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="DurationPackage"><?php echo lang('duration_package'); ?></label>
                                                <select name="DurationPackage" class="form-control" id="DurationPackage">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label col-md-6" for="AmountCalculations"><?php echo lang('amount_calculations'); ?>: <span class="cal_val" id="AmountCalculations">0</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label col-md-6" for="GrandTotal"><?php echo lang('grand_total'); ?>: <span class="cal_val" id="GrandTotal">0</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label col-md-6" for="Discount"><?php echo lang('discount'); ?>: <span class="cal_val" id="Discount">0</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label col-md-6" for="NetPayables"><?php echo lang('net_payables'); ?>: <span class="cal_val" id="NetPayables">0</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="PaymentVia"><?php echo lang('pay_via'); ?></label>
                                                <select class="form-control" id="PaymentVia" name="PaymentVia" required="required">
                                                    <option selected="" value=""></option>
<!--                                                    <option value="NA">Pay Later</option>-->
                                                    <option value="jazzcash">Jazz Cash</option>
                                                    <option value="easypesa">Easy Pesa</option>
                                                    <option value="upesa">U Pesa</option>
                                                    <option value="ublomni">UBL Omni</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label" for="Cnic"><?php echo lang('id_card_number'); ?></label>
                                                <input type="text" name="Cnic" required  class="form-control" id="Cnic">
                                            </div>
                                        </div>
                                    </div>

                                                                    <div class="row hide payvia_fields">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group label-floating">
                                                                                <label class="control-label" for="Amount">Amount Deposit</label>
                                                                                <input type="text" name="Amount"  class="form-control" id="Amount" autocomplete="off">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group label-floating">
                                                                                <label class="control-label" for="TransactionID">Transaction ID</label>
                                                                                <input type="text" name="TransactionID"  class="form-control" id="TransactionID" autocomplete="off">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row hide payvia_fields">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group label-floating is-focused">
                                                                                    <label class="control-label" for="DepositDate ">Deposit Date</label>
                                                                                    <input type="text" name="DepositDate" class="form-control datepicker" value="<?php echo date("m/d/Y") ?>">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                            <?php
                                }
                            ?>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="row fieldGroupCopy" style="display: none;">
    <div class="input-group col-md-12">
        <div class="col-md-6">
            <input type="text" name="VideoURL[]" class="form-control" placeholder="<?php echo lang('youtube_video');?>"/>
        </div>
        <div class="col-md-2">
            <div class="input-group-addon" style="padding:0px !important; width: auto !important;"> 
                <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .red-alert {
        color: #fe3d2e;
    }
    .green-alert {
        color: #00a200;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        //group add limit
        var maxGroup = 100;
        //add more fields group
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length < maxGroup){
                var fieldHTML = '<div class="row fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                $('body').find('.fieldGroup:last').after(fieldHTML);
            }else{
                alert('Maximum '+maxGroup+' groups are allowed.');
            }
        });
        
        //remove fields group
        $("body").on("click",".remove",function(){ 
            $(this).parents(".fieldGroup").remove();
        });


               // pay-via select fields hide-show
       $('#PaymentVia').on('change', function(){
          var sl_val = $(this).val();
          if(sl_val != 'pay_later'){
            $('.payvia_fields').removeClass('hide').addClass('show');
          }else {
            $('.payvia_fields').removeClass('show').addClass('hide');
          }
       });
       let charges = 0;
       let discount = 0;
       let grandTotal = 0;

    // Package selection
        $("#DurationPackage").on('change', function () {
            charges = parseInt($(this).find(':selected').data('charges'));
            discount = parseInt($(this).find(':selected').data('discount'));
            let curr_symbol = $(this).find(':selected').data('sign');

            grandTotal = charges - discount;

            console.log('Symbol: ', curr_symbol);

            $("#AmountCalculations").html(curr_symbol + charges);
            $("#Discount").html(curr_symbol + discount);

            if($('input#SliderImage:checked').length>0) 
            {
                grandTotal = grandTotal + 20;
            }

            if($('input#Gif:checked').length>0) 
            {
                grandTotal = grandTotal + 10;

            }


            $("#GrandTotal").html(curr_symbol + grandTotal);
            $("#NetPayables").html(curr_symbol + grandTotal);

            
        });

        $('#SliderImage').on('click',function(){
            

            if($('input#SliderImage:checked').length>0) 
            {
                $('.slider_image_div').show();
                grandTotal = grandTotal + 20;
                $("#GrandTotal").html('$' + grandTotal);
                $("#NetPayables").html('$' + grandTotal);
            }else{
                 $('.slider_image_div').hide();
                 grandTotal = grandTotal - 20;
                 $("#GrandTotal").html('$' + grandTotal);
                $("#NetPayables").html('$' + grandTotal);
            }

        });

        $('#Gif').on('click',function(){
            
            if($('input#Gif:checked').length>0) 
            {
                $('.gif_image_div').show();
                 grandTotal = grandTotal + 10;
                $("#GrandTotal").html('$' + grandTotal);
                $("#NetPayables").html('$' + grandTotal);
            }else{
                 $('.gif_image_div').hide();
                  grandTotal = grandTotal - 10;
                $("#GrandTotal").html('$' + grandTotal);
                $("#NetPayables").html('$' + grandTotal);
            }

        });

    });
</script>