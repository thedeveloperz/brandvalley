<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>sr#</th>
<!--                                    <th>--><?php //echo lang('company_name');?><!--</th>-->
<!--                                    <th>--><?php //echo lang('email');?><!--</th>-->
                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('company_url');?></th>
                                    <th><?php echo lang('category');?></th>
                                    <th><?php echo lang('sub_category');?></th>
                                    <th><?php echo lang('slider_image');?></th>


                                    <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    $index=1;
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->ProductID;?>">
                                            <td><?php echo $index;?></td>
<!--                                            <td>--><?php //echo $value->CompanyName; ?><!--</td>-->
<!--                                            <td>--><?php //echo $value->Email; ?><!--</td>-->
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->CompanyUrl; ?></td>
                                            <td><?php echo $value->Category; ?></td>
                                            <td><?php echo $value->SubCategory; ?></td>


                                            <td><?php echo ($value->SliderImage ? lang('yes') : lang('no')); ?></td>

                                             <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->ProductID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons" title="Edit">dvr</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->ProductID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                        $index++;
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>