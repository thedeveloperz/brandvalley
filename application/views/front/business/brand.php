<div class="content">
    <section id="location-section">
        <div class="container-fluid">
            <ul class="location-list clearfix">
                <li>You are here</li>
                <li><?php echo (($CountryData && isset($CountryData[0]->Title) ? $CountryData[0]->Title : ''));?></li>
                <li><?php echo (($StateData && isset($StateData[0]->Title) ? $StateData[0]->Title : ''));?></li>
                <li><?php echo (($CityData && isset($CityData[0]->Title) ? $CityData[0]->Title : ''));?></li>
            </ul>
        </div>
    </section>
<div class="container">
    <div class="row">
<!--        <div class="col-md-2">-->
<!--            <div class="brand-logo">-->
<!--                --><?php //if(!file_exists($BusinessPage->BusinessLogo)) {
//                    $image = base_url("assets/backend/img/no_img.png");
//                }else {
//                    $image = base_url($BusinessPage->BusinessLogo);
//                }
//                ?>
<!--                <img class="img-responsive" src="--><?php //echo $image ?><!--" alt="">-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-md-12">
            <h4 class="card-title"><?php echo $BusinessPage->BusinessName?></h4>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <div class="container-fluid listings">
            <?php if($products){ ?>
            <div class="row">
                <?php foreach($products as $key => $product){ ?>
                    <?php if($key % 3 == 0 && $key != 0){ ?>
                    </div><!-- row -->
                    <div class="row">
                    <?php } ?>
                    <?php if(!file_exists($product->Image)) {
                        $image = base_url("assets/backend/img/no_img.png");
                    }else {
                        $image = base_url($product->Image);
                    }
                    ?>
                <div class="col-sm-4">
                    <a
                            href="#"
                            class="list-box"
                            data-business_name="<?php echo ($BusinessPage) ? $BusinessPage->BusinessName : '' ?>"
                            data-image="<?php echo $image ?>"
                            data-title="<?php echo $product->Title ?>"
                            data-desc="<?php echo $product->Description ?>"
                    >
                        <div class="list-img">
                            <img class="img-responsive" src="<?php echo $image ?>" />
                        </div>
                        <div class="list-content">
                            <h4><?php echo $product->Title ?></h4>
<!--                            <p class="list-type">--><?php //echo $profile['FullName'] ?><!--</p>-->
<!--                            <p class="list-desc">--><?php //echo $profile['Email'] ?><!--</p>-->
<!--                            <p class="list-phone"><i class="fa fa-phone-square" aria-hidden="true"></i>  --><?php //echo $profile['Phone'] ?><!--</p>-->
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div><!-- row -->
            <?php } else { ?>
                <div class="alert alert-danger">No Products found in your selected location.</div>
            <?php } ?>
        </div>
    </div>

    </div><!-- col-md-8 -->
</div><!-- row -->


</div><!-- container -->


</div><!-- main-panel -->


<!-- Modal -->
<div class="modal fade" id="quick-product-modal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content quick-modal">
            <div class="loading-animation">
                <img src="<?= base_url() . "assets/img/" ?>loader.svg" width="50" alt="">
            </div>
            <div class="modl">
                <div class="modal-body" style="padding-bottom: 60px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="modal-img__large">
                                <img src="" class="modal-img">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="quick-modal__category"></div>
                            <h3 class="product-box__title modal-title"></h3>
                            <div class="product-box__short-desc modal-desc"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        // trigger quick product view modal
        $(document).on('click', '.list-box', function(e){
            e.preventDefault();
            let image = $(this).data('image');
            let title = $(this).data('title');
            let desc = $(this).data('desc');
            let business_name = $(this).data('business_name');
            $('.modl').hide();
            $('.loading-animation').show();
            $('#quick-product-modal').modal('show');

            $('.modal-img').attr('src', image);
            $('.modal-title').html(title);
            $('.modal-desc').html(desc);
            $('.quick-modal__category').html(business_name);

            setTimeout(function(){
                $('.loading-animation').hide();
                $('.modl').show();
            }, 1000);
        });
    });
</script>

