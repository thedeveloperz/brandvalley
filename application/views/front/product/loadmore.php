<?php if($top_categories){
    foreach ($top_categories as $key => $cat_products) {
        if(!empty($cat_products['products'])){
            ?>

            <div class="products mostsold-products category" data-category_id="<?php echo $cat_products['CategoryID'] ?>">
                <div class="container-fluid">
                    <div class="carousel-container">
                        <div class="products-carousel__title">
                            <h3><?php echo $cat_products['Title'];?></h3>
                        </div>
                        <div class="products-carousel owl-carousel owl-theme" style="display: block">

                            <?php foreach ($cat_products['products'] as $key => $product) { ?>


                                <div class="item">
                                    <div class="product-box">
                                        <div class="product-box__img">
                                            <?php if(!file_exists($product['ImageName'])) {
                                                $image = base_url("assets/backend/img/no_img.png");
                                            }else {
                                                $image = base_url($product['ImageName']);
                                            }
                                            ?>
                                            <img src="<?php echo $image ?>" alt="">
                                            <div class="product-box__quick-view">
                                                <a href="javascript:void(0);" class="product-box__quick-view-btn" data-id = "<?php echo  $product['ProductID'];?>"><i class="fa fa-search" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <h3 class="product-box__title"><?php echo $product['Title']; ?></h3>
                                        <div class="product_price">Rs. <?php echo $product['Price']; ?></div>
                                        <div class="views">Views(<?php echo get_product_view_count($product['ProductID']) ?>)</div>

                                        <div class="product-box__rating">
                                            <?php $avg_rating = getProductAvgRating($product['ProductID']) ?>
                                            <select class="rating-box">
                                                <?php for($i=1; $i<=5; $i++) { ?>
                                                    <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="product-box__btn-box">
<!--                                            <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                                <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                                <div class="ripple-container"></div>-->
<!--                                            </button>-->
                                            <a href="<?php echo base_url('product/details/'.$product['ProductID']);?>" class="product-box__dtl-btn">View Full Details</a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div><!-- products.mostsold-products -->



            <?php

        }
    }

}
?>


<script>
    $(document).ready(function () {

// rating plugin init
        $(function() {
            $('.rating-box').barrating({
                theme: 'fontawesome-stars',
                readonly: true
            });
        });

        $(".products-carousel").owlCarousel({
            items : 5, //10 items above 1000px browser width
            itemsDesktop : [1000,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0
            loop:false,
            nav: true,
            dots: false,
            responsive : {
                320 : {
                    items: 1
                },
                480 : {
                    items : 2,
                },
                768 : {
                    items : 2,
                },
                1200 : {
                    items : 5,
                }
            }
        });


    });
</script>
