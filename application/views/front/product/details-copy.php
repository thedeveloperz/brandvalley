<!-- css - Asif ---->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/fontawesome-stars.min.css">

<section id="product-details-section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="breadcrumbs">
          <ul class="breadcrumbs__list clearfix">
            <li class="breadcrumbs__list-item"><a href="#">Home</a></li>
            <li class="breadcrumbs__list-item"><a href="#"><?php echo $product[0]['Category']; ?></a></li>
            <li class="breadcrumbs__list-item"><?php echo $product[0]['SubCategory']; ?></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="product-gallery">
          <span class='zoom' id='ex1'>
        		<img class="zoomer-large-img" src='<?php echo base_url($product[0]['ImageName']) ?>' width='555' height='320' alt='Daisy on the Ohoopee'/>
        	</span>
        </div>
        <div class="zoomer-thumbs owl-carousel owl-theme">
          <?php if($product_images){
            foreach ($product_images as $key => $value) { ?>
                    <div class="item"><a href="#" class="zoomer-thumb"><img src="<?php echo base_url($value->ImageName);?>"></a></div>
              <?php
             
            }
          }
          ?>
          
        
      </div>

      </div>
      <div class="col-sm-6">
        <h3 class="product-box__title"><?php echo $product[0]['Title']; ?></h3>
        <div class="product-box__rating">
            <?php if($reviews) { ?>
            <?php $avg_rating = getProductAvgRating($product[0]['ProductID']) ?>
            <select class="rating-box">
                <?php for($i=1; $i<=5; $i++) { ?>
                    <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                <?php } ?>
            </select>
            <?php } ?>
            <span class="rating-numbers"><?php echo ($reviews) ? count($reviews) : 0 ?> Review(s)</span>
        </div>
        <p class="product-desc">
          <?php echo $product[0]['Description']; ?>
           
        </p>
        <div class="quantity-box">
            <span class="qty-title">Qty: </span>
            <div class="qty-spinner">
                <input type='button' value='-' class='qtyminus' field='quantity' />
                <input type='text' name='quantity' value='1' class='qty' />
                <input type='button' value='+' class='qtyplus' field='quantity' />
            </div>
        </div>
        <div class="product-box__btn-details">
          <a class="btn btn-info" title="Add To Cart">
            <span class="btn-label"><i class="material-icons">add_shopping_cart</i></span>
            Add To Cart<div class="ripple-container"></div>
          </a>
        </div>
      </div>
    </div>
    <?php if($ProductVideo) { ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="video-box">
                    <iframe width="560" height="315" src="<?php echo $ProductVideo->VideoURL ?>"
                            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    <?php } ?>
  </div>
</section>

<section id="product-reviews">
    <div class="container">
        <?php if($reviews) { ?>
        <div class="row">
         <?php foreach($reviews as $review) { ?>
             <div class="col-sm-12">
                 <div class="user-review">
                     <div class="user-avatar clearfix">
                         <div class="user-img">
                             <?php if($this->session->userdata['admin']['Image'] == ''){
                                 $image = base_url('assets/backend/img/no_img.png');
                             }else{
                                 $image = base_url($this->session->userdata['admin']['Image']);
                             }
                             ?>
                             <img src="<?php echo $image ?>">
                         </div>
                         <div class="user-avatar-right">
                             <div class="user-name"><?php echo $review['FullName'] ?></div>
                             <div class="product-box__rating">
                                 <select name="rating" class="rating-box">
                                     <?php for($i = 1; $i <= 5; $i++) { ?>
                                     <option value="<?php echo $i; ?>" <?php echo ($i == $review['Rating']) ? "selected": "" ?> > <?php echo $i; ?> </option>
                                     <?php } ?>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="review-timestamp"><?php echo date('F d, Y', strtotime($review['CreatedAt'])); ?></div>
                     <div class="review-text"><?php echo $review['Review'] ?></div>
                 </div>
             </div>
         <?php } ?>
        </div>
      <?php } ?>








        <div class="row">
            <div class="col-sm-12">
                <?php if($this->session->userdata('admin')){ ?>
                <?php if( canWriteReview($this->session->userdata('admin')['UserID'], $product[0]['ProductID'])){ ?>
                <form method="post" class="form_data" action="<?php echo base_url("/product/addreview") ?>">
                    <input type="hidden" name="ProductID" value="<?php echo $product[0]['ProductID']?>">
                    <div class="form-group">
                        <label class="control-label review-label">Write Review</label>
                        <textarea name="review" class="review-textarea form-control" rows="8"></textarea>
                    </div>
                    <div class="product-box__rating">
                        <select name="rating" class="rating-box-active">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-fill btn-info">Submit</button>
                </form>
                    <?php } ?>
                <?php }else { ?>
                    <p><a class="btn btn-info" href="<?php echo base_url("account/login") ?>">Login</a> To write a review</p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<?php if(!empty($sub_categories)){ ?>
          <section id="products-details-similars">
    <div class="products feature-products">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3>Related Categories</h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">
                <?php foreach ($sub_categories as $key => $related_category) {  ?>
                        <div class="item">
                            <div class="product-box">
                                <div class="product-box__img">
                                    <img src="<?php echo base_url($related_category['Image']) ?>" alt="">
                                    
                                </div>
                               
                                <h3 class="product-box__title"><?php echo $related_category['Title']; ?></h3>
                                <div class="product-box__btn-box">
                                
                                
                                </div>
                            </div>
                  </div>

<?php
                }
                ?>
                  
                  
              </div>
            </div>
        </div>
    </div><!-- products.feature-products -->

</section>

      <?php     
         
        

}
?>
<?php if(!empty($related_products)){ ?>
          <section id="products-details-similars">
    <div class="products feature-products">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3>Related Products</h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">
                <?php foreach ($related_products as $key => $related_product) {  ?>
                        <div class="item">
                            <div class="product-box">
                                <div class="product-box__img">
                                    <img src="<?php echo base_url($related_product['ImageName']) ?>" alt="">
                                    <div class="product-box__quick-view">
                                        <a href="javascript:void(0);" class="product-box__quick-view-btn" data-id="<?php echo $related_product['ProductID'];?>"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="product-box__rating">
                                    <select class="rating-box">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3" selected>3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                                <h3 class="product-box__title"><?php echo $related_product['Title']; ?></h3>
                                <div class="product-box__btn-box">
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
                                <a href="<?php echo base_url('product/details/'.$related_product['ProductID']);?>" class="product-box__dtl-btn">View Full Details</a>
                                </div>
                            </div>
                  </div>

<?php
                }
                ?>
                  
                  
              </div>
            </div>
        </div>
    </div><!-- products.feature-products -->

</section>

      <?php     
         
        

}
?>



<!-- JS Plugins - Asif ---->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script src="<?php echo base_url('assets/plugins/zoomer/jquery.zoom.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<div class="modal fade" id="quick-product-modal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content quick-modal">
      <div class="loading-animation">
        <img src="<?= base_url() . "assets/img/" ?>loader.svg" width="50" alt="">
      </div>
      <div class="modl">
        
      </div>

    </div>

  </div>
</div>


<script type="text/javascript">

// rating plugin init
   $(function() {
      $('.rating-box').barrating({
           theme: 'fontawesome-stars',
           readonly: true
       });
       $('.rating-box-active').barrating({
           theme: 'fontawesome-stars',
       });
   });

    $(document).ready(function(){
        // image zoomer
    			$('#ex1').zoom();

// thumbs carousel
          $('.owl-carousel').owlCarousel({
              loop:false,
              margin:10,
              nav:true,
              items: 3,
              dots: false,
              responsive:{
                  0:{
                      items:1
                  },
                  600:{
                      items:3
                  },
                  1000:{
                      items:5
                  }
              }
          });

          // quantity spinner
          // This button will increment the value
            $('.qtyplus').click(function(e){
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name='+fieldName+']').val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('input[name='+fieldName+']').val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name='+fieldName+']').val(0);
                }
            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function(e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name='+fieldName+']').val());
                // If it isn't undefined or its greater than 0
                if (!isNaN(currentVal) && currentVal > 1) {
                    // Decrement one
                    $('input[name='+fieldName+']').val(currentVal - 1);
                }
            });

            $(".products-carousel").owlCarousel({
                 items : 4, //10 items above 1000px browser width
                 itemsDesktop : [1000,5], //5 items between 1000px and 901px
                 itemsDesktopSmall : [900,3], // betweem 900px and 601px
                 itemsTablet: [600,2], //2 items between 600 and 0
                 loop:true,
                 nav: true,
                 dots: false
            });

        // quick thumb load in main images
        $('.zoomer-thumb').click(function(e){
            e.preventDefault();
            var thumbSrc = $(this).find('img').attr('src');
            // load thumb into main image.
            $(".zoomer-large-img").attr('src', thumbSrc);
            $(".zoomImg").attr('src', thumbSrc);
        });



         // trigger quick product view modal
        $(document).on('click', '.product-box__quick-view-btn', function(e){
            e.preventDefault();
            $('.loading-animation').show();
            $('#quick-product-modal').modal('show');
            $.ajax({
            type: "POST",
            url: base_url+'product/details/'+$(this).attr('data-id')+'/true',
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                $('.modl').hide();
                $('.modl').html(result.html);

                setTimeout(function(){
                  $('.modl').show();
                  $('.loading-animation').hide();
                }, 2000);
            },
            complete: function () {
                
            }
        }); 



           

            // hide modal main content first
            //$('.modl').hide();

            // show loading animation during ajax request
            //$('.loading-animation').show();
            

        });


        // quick thumb load in main images
        $('.product-thumbs__thumbnail').on('click',function(e){
            e.preventDefault();
            var thumbSrc = $(this).find('img').attr('src');
            // load thumb into main image.
            $(".modal-img__large > img").attr('src', thumbSrc).fadeIn();
        });

    });


</script>
