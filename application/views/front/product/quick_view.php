<div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="row">
            <div class="col-sm-6">
              <div class="modal-img__large">
                  <?php if(!file_exists($product[0]['ImageName'])) {
                      $image = base_url("assets/backend/img/no_img.png");
                  }else {
                      $image = base_url($product[0]['ImageName']);
                  }
                  ?>
                  <img src="<?php echo $image;?>">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="quick-modal__category">
                Appliances
              </div>
              <h3 class="product-box__title"><?php echo $product[0]['Title'];?></h3>
                <div class="product_price">Rs. <?php echo $product[0]['Price']; ?></div>
                <div class="product-box__rating">
                    <?php $avg_rating = getProductAvgRating($product[0]['ProductID']) ?>
                    <select class="rating-box">
                        <?php for($i=1; $i<=5; $i++) { ?>
                            <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                        <?php } ?>
                    </select>
                </div>
              <div class="product-box__short-desc">
                <?php echo $product[0]['ShortDescription']; ?>
              </div>
              <div class="product-box__btn-details">
                <a href="<?php echo base_url('product/details/'.$product[0]['ProductID']);?>" class="btn">see details</a>
<!--                <a class="btn btn-info" title="Add To Cart">-->
<!--                  <span class="btn-label"><i class="material-icons">add_shopping_cart</i></span>-->
<!--                  Add To Cart<div class="ripple-container"></div>-->
<!--                </a>-->
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <i _ngcontent-c17="" class="baseline-add_shopping_cart icon-image-preview"></i>
          <div class="row">
            <div class="col-sm-12">
              <div class="product-thumbs clearfix">
                <?php if($product_images){
                   foreach ($product_images as $key => $value) { ?>
                          <a href="javacript:void(0);" class="product-thumbs__thumbnail"><img src="<?php echo base_url($value->ImageName);?>"></a>

                    <?php
                    
                   }
                 }
                 ?>
                
              </div>
            </div>
          </div>
        </div>
 <script>
   $(document).ready(function(){
   $('.rating-box').barrating({
        theme: 'fontawesome-stars',
       readonly: true
      });

    $('.product-thumbs__thumbnail').on('click',function(e){
            e.preventDefault();
            var thumbSrc = $(this).find('img').attr('src');
            // load thumb into main image.
            $(".modal-img__large > img").attr('src', thumbSrc).fadeIn();
        });
 });

 </script>       