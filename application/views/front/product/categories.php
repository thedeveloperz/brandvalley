<div class="content">
    <section id="location-section">
        <div class="container-fluid">
            <ul class="location-list clearfix">
                <li>You are here</li>
                <li><?php echo (($CountryData && isset($CountryData[0]->Title) ? $CountryData[0]->Title : ''));?></li>
                <li><?php echo (($StateData && isset($StateData[0]->Title) ? $StateData[0]->Title : ''));?></li>
                <li><?php echo (($CityData && isset($CityData[0]->Title) ? $CityData[0]->Title : ''));?></li>
            </ul>
        </div>
    </section>
<div class="container">
<div class="row">
    <div class="col-md-2">
        <?php if($sub_cats){ ?>
        <div class="sub-cats-container">
            <h6>Subcategories</h6>
            <ul class="sub-cats">
                <?php foreach($sub_cats as $cat){ ?>
                    <li><a class="<?php echo ($cat->CategoryID == $sub_category_id) ? 'active' : '' ?>" href="<?php echo base_url("/product/categories/$category_id/$cat->CategoryID") ?>"><?php echo $cat->Title ?></a></li>
                <?php } ?>
            </ul>
        </div><!-- sub-cats-container -->
        <?php } ?>
    </div><!-- col-md-4 -->
    <div class="col-md-10">
        <section id="location-section">
            <ul class="location-list clearfix">
                <li><b><?php echo $main_category[0]['Title']; ?></b></li>
                <?php if($sub_category){ ?>
                <li><?php echo $sub_category[0]['Title'] ?></li>
                <?php } ?>
            </ul>
        </section>


        <div class="container">
            <div class="row">
                <?php if($category_products) { ?>
                <?php foreach($category_products as $key => $value){ ?>
                <?php if($key % 3 == 0 && $key != 0) { ?>
                </div><!-- row -->
                <div class="row">
                <?php } ?>
                <div class="col-md-3">
                    <div class="item">
                        <div class="product-box category-product-box">
                            <div class="product-box__img">
                                <?php if(!file_exists($value['ImageName'])) {
                                    $image = base_url("assets/backend/img/no_img.png");
                                }else {
                                    $image = base_url($value['ImageName']);
                                }
                                ?>
                                <img src="<?php echo $image ?>" alt="">

                                <div class="product-box__quick-view">
                                    <a href="#" class="product-box__quick-view-btn" data-id="<?php echo  $value['ProductID'];?>" ><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="product-box__rating">
                                <?php $avg_rating = getProductAvgRating($value['ProductID']) ?>
                                <select class="rating-box">
                                    <?php for($i=1; $i<=5; $i++) { ?>
                                        <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <h3 class="product-box__title"><?php echo $value['Title'] ?></h3>
                            <div class="product_price">Rs. <?php echo $value['Price']; ?></div>
                            <div class="product-box__btn-box">
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
                                <a href="<?php echo base_url('product/details/') ?>/<?php echo $value['ProductID']; ?>" class="product-box__dtl-btn">View Full Details</a>
                            </div>
                        </div>
                    </div><!-- item -->
                </div>
                <?php } ?>
                <?php }else { ?>
                    <p class="col-sm-6 alert alert-danger">No products found.</p>
                <?php } ?>

            </div>
        </div>
    </div>

    </div><!-- col-md-8 -->
</div><!-- row -->






<!--    <div class="row">-->
<!--        <div class="col-md-offset-2 col-md-10">-->
<!--            <div class="carousel-container">-->
<!--                <div class="products-carousel__title">-->
<!--                    <h3>Iphone</h3>-->
<!--                </div>-->
<!--                <div class="products-carousel owl-carousel owl-theme">-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <div class="product-box">-->
<!--                            <div class="product-box__img">-->
<!--                                <img src="--><?php //echo base_url('uploads/product_image/17034301920181011011850analyse_green.png') ?><!--" alt="">-->
<!--                                <div class="product-box__quick-view">-->
<!--                                    <a href="#" class="product-box__quick-view-btn" data-id="1"><i class="fa fa-search" aria-hidden="true"></i></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="product-box__rating">-->
<!--                                <select class="rating-box">-->
<!--                                    <option value="1">1</option>-->
<!--                                    <option value="2">2</option>-->
<!--                                    <option value="3" selected>3</option>-->
<!--                                    <option value="4">4</option>-->
<!--                                    <option value="5">5</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <h3 class="product-box__title">Samsung S9</h3>-->
<!--                            <div class="product-box__btn-box">-->
<!--                                <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                    <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                    <div class="ripple-container"></div>-->
<!--                                </button>-->
<!--                                <a href="--><?php //echo base_url('product/details/10') ?><!--" class="product-box__dtl-btn">View Full Details</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->





</div><!-- container -->


</div><!-- main-panel -->

<!-- Modal -->
<div class="modal fade" id="quick-product-modal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content quick-modal">
            <div class="loading-animation">
                <img src="<?= base_url() . "assets/img/" ?>loader.svg" width="50" alt="">
            </div>
            <div class="modl">

            </div>

        </div>

    </div>
</div>






<script type="text/javascript">

    // rating plugin init
    $(function() {
        $('.rating-box').barrating({
            theme: 'fontawesome-stars',
            readonly: true
        });
    });

    $(document).ready(function(){
        // image slider
        $("#owl-image-slider").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            items: 1,
            autoplay: true,
            smartSpeed: 1000
        });

        $(".products-carousel").owlCarousel({
            items : 5, //10 items above 1000px browser width
            itemsDesktop : [1000,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0
            loop:false,
            nav: true,
            dots: false,
            responsive : {
                320 : {
                    items: 1
                },
                480 : {
                    items : 2,
                },
                768 : {
                    items : 2,
                },
                1200 : {
                    items : 4,
                }
            }
        });

        // brands slider
        $('#brands-slider').owlCarousel({
            loop: true,
            autoplay: true,
            items: 6,
            dots: false,
            margin: 10,
            autoplayTimeout: 3000,
            smartSpeed: 1000,
            responsive : {
                320 : {
                    items: 2
                },
                480 : {
                    items : 2,
                },
                768 : {
                    items : 3,
                },
                1200 : {
                    items : 6,
                }
            }
        });

        // trigger quick product view modal
        $(document).on('click', '.product-box__quick-view-btn', function(e){

            e.preventDefault();
            $('.loading-animation').show();
            $('#quick-product-modal').modal('show');
            $.ajax({
                type: "POST",
                url: base_url+'product/details/'+$(this).attr('data-id')+'/true',
                data: new FormData(this),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                //async:false,
                success: function (result) {
                    $('.modl').hide();
                    $('.modl').html(result.html);

                    setTimeout(function(){
                        $('.modl').show();
                        $('.loading-animation').hide();
                    }, 2000);
                },
                complete: function () {

                }
            });





            // hide modal main content first
            //$('.modl').hide();

            // show loading animation during ajax request
            //$('.loading-animation').show();


        });


        // quick thumb load in main images


    });


</script>
