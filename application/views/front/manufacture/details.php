<div class="content">


<section id="banner" style="margin-bottom: 10px;">
  <div class="container-fluid align-center">
    <?php if(file_exists($result[0]['CoverImage'])){ ?>

     <img class="gif-banner" src="<?php echo base_url($result[0]['CoverImage']); ?>" alt="" height="200;">
   <?php }else{ ?>
         <img class="gif-banner" src="<?php echo base_url('assets/img/gif-banner.gif') ?>" alt="">
<?php
    }
    ?>
  </div>

</section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="breadcrumbs">
                <ul class="breadcrumbs__list clearfix">
                    <li class="breadcrumbs__list-item"><a href="<?php echo base_url();?>">Home</a></li>
                    <li class="breadcrumbs__list-item"><a href="javascript:void(0);"><?php echo 'Manufacture'; ?></a></li>
                    <li class="breadcrumbs__list-item"><?php echo $result[0]['FullName']; ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-4 red profile-info-front" style="backgroud-color:yellow">
            <?php echo $result[0]['Email']; ?>
        </div>
        <div class="col-md-4 profile-info-front yellow">
            <?php echo $result[0]['Phone']; ?>
        </div>
        <div class="col-md-4 profile-info-front green">
            <?php echo $result[0]['UserProfileType']; ?>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-12">
            <?php echo $result[0]['AboutUs']; ?>
        </div>
    </div>
</div>
 


</div><!-- main-panel -->


