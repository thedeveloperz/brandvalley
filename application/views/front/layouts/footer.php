

</div>
</div>




<div class="main-footer">
		<div class="container-fluid">
			<div class="space-left-right">
				<div class="dis-table">
					<div class="dis-cell footer-box">
						<h3>About Us</h3>
						<p>Brands Valley is an advertising platform targetting clients either local or international who are looking for all kinds of branding. Brands Valley looks forward to companies or local businesses which are very quality conscious and we are aiming to provide extremely powerful concepts and techniques of advertisements for our clients.</p>
						<div class="footer-logo">
							<img src="https://brandsvalley.net/assets/web/images/footer-logo.png">
						</div>
					</div>
					<div class="dis-cell footer-box">
						<h3>Get to Know Us</h3>
						<ul class="footer-nav">
							<li><a href="">Careers</a></li>
							<li><a href="">About Be a Brands</a></li>
							<li><a href="">Investor Relations</a></li>
<!--							<li><a href="">BV Devices</a></li>-->
						</ul>
					</div>
<!--					<div class="dis-cell footer-box">-->
<!--						<h3>Make Money with Us</h3>-->
<!--						<ul class="footer-nav">-->
<!--							<li><a href="">Sell on BV</a></li>-->
<!--							<li><a href="">Sell Your Services</a></li>-->
<!--							<li><a href="">Investor Relations</a></li>-->
<!--							<li><a href="">BV Devices</a></li>-->
<!--						</ul>-->
<!--					</div>-->
<!--					<div class="dis-cell footer-box">-->
<!--						<h3>BV Payment Products</h3>-->
<!--						<ul class="footer-nav">-->
<!--							<li><a href="">Sell on BV</a></li>-->
<!--							<li><a href="">Sell Your Services</a></li>-->
<!--							<li><a href="">Investor Relations</a></li>-->
<!--							<li><a href="">BV Devices</a></li>-->
<!--						</ul>-->
<!--					</div>-->
					<div class="dis-cell footer-box">
						<h3>Contact Human Resources</h3>
						<ul class="footer-nav">
							<li class="location-icon">
								<h6>Address:</h6>
								<p>65 Main Road, PO BOX 28, PAK
								Kaiapoi 7442, Chirstal NZ</p>
							</li>
							<li class="receiver-icon">
								<h6>Call Free</h6>
								<p>+ 92 301 427 8992</p>
							</li>
							<li class="fax-icon">
								<h6>Email:</h6>
								<p>info@brandsvalley.net</p>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom text-center">
			<p>Copy &nbsp;</p>
			<ul class="bottombar">
				<li><a href="https://brandsvalley.net/mudasar/home">Brands Valley</a></li>
				<li><a href="https://brandsvalley.net/mudasar/home/TermsofService">Terms of services</a></li>
				<li><a href="https://brandsvalley.net/mudasar/home/TermsofService">Privacy Policy</a></li>
 			</ul>
 			<p>All Rights Reserved.</p>
		</div>
	</div>

</body>

<style type="text/css">
    /* footer styles */
    .dis-cell {
        display: table-cell;
        position: relative;
        vertical-align: middle;
    }

    .dis-table {
        display: table;
        width: 100%;
        height: 100%;
    }
    .main-footer .dis-table {
        padding-bottom: 30px;
        position: relative;
    }

    .main-footer {
        background-color: #233c51;
        padding: 40px 0 0 0;
    }
    .footer-box h3 {
        color: #fff;
        font-size: 18px;
        margin: 0 0 26px;
        position: relative;
        padding: 0 0 15px 0;
    }
    .footer-box h3:before, .footer-box h3:after, .brand-valley-bounty h3:before, .brand-valley-text h3:after {
        content:"";
        position:absolute;
        left:0;
        bottom:0;
        width:60px;
        height:3px;
        background-color:#fff;
    }
    ul.footer-nav h6 {
        color: #fff;
        font-size: 14px;
        margin: 0;
        font-weight: 600;
        font-family: 'proxima_nova', sans-serif;
    }
    .footer-box {
        width: 20%;
        vertical-align: top;
        padding: 0 10px 0 0;
    }
    .footer-bottom ul {
        display: inline-block;
    }
    .footer-box h3:after {
        width: 8px;
        left: 65px;
    }
    .footer-box p, .footer-box a {
        color: #c8ced4;
        font-size: 13px;
        line-height: 20px;
        font-family: 'proxima_nova', sans-serif;
        font-weight: 300;
    }
    ul.footer-nav li {
        display: block;
        margin: 0 0 5px;
    }
    .footer-box p {
        padding: 0 20px 0 0;
    }
    .footer-bottom {
        background-color: #1c2d3d;
        padding: 12px 0;
    }
    .footer-bottom p {
        color: #fff;
        font-size: 13px;
        font-family: 'proxima_nova', sans-serif;
        display: inline-block;
        margin: 3px 0 0px 0 !important;
        vertical-align: top;
    }

    .footer-box img {
        max-width: 100%;
    }
    .footer-logo {
        margin: 22px 0 0;
    }
    .footer-box:first-child {
        width: 25%;
    }
    .footer-box:nth-child(2), .footer-box:nth-child(3), .footer-box:nth-child(4) {
        width: 15%;
    }
    .footer-box:last-child {
        padding: 0 0 0px 20px;
    }
    .topbar > li, .bottombar > li {
        display: inline-block;
        text-align: left;
    }
    .topbar li {
        position: relative;
    }
    .topbar > li:not(:last-child)::after, .bottombar > li:not(:last-child)::after {
        color: #c8c8c8;
        content: "|";
        display: inline-block;
        margin: 0 3px 0px 7px;
    }
    ul.bottombar li a {
        line-height: 28px;
        color: #fff;
    }

</style>
<script type="text/javascript">
    $(document).ready(function () {

       $('#CountryID').on('change',function(){

            var CountryID = $(this).val();

            if(CountryID == '')
            {
                $('#StateID').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/state/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateID').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });
       $('#StateID').on('change',function(){

            var StateID = $(this).val();

            if(StateID == '')
            {
                $('#CityID').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/city/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityID').html(result.html);
                        if(result.array)
                        {
                            $(".selectpicker").selectpicker('refresh');
                        }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });
       $('#CategoryID').on('change',function(){

            var CategoryID = $(this).val();

            if(CategoryID == '')
            {
                $('#SubCategoryID').html('<option value=""><?php echo lang("choose_sub_category");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/category/getSubCategory',
                    data: {
                        'CategoryID': CategoryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#SubCategoryID').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });
       $('#DollersystemID').on('change',function(){

            var DollersystemID = $(this).val();

            if(DollersystemID != '')
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/package/getAllPackages',
                    data: {
                        'DollersystemID': DollersystemID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#DurationPackage').html(result.html);

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });



    




    });
</script>
<!-- Forms Validations Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url();?>assets/backend/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url();?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url();?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url();?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url();?>assets/backend/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url();?>assets/backend/js/demo.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/script.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.blockUI.js"></script>





<script src="<?php echo base_url();?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.fileuploads.init.js"></script>

<?php if($this->session->flashdata('message')){ ?>
    <script>

        $( document ).ready(function() {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:32:16 GMT -->
</html>
