<!doctype html>
<html lang="en">

<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->

<head>
    <meta charset="utf-8" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/backend/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url();?>assets/backend/images/favicon.png" type="image/x-icon">
    <!-- App title -->
    <title>
        <?php echo $site_setting->SiteName; ?>
    </title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />
    <!--   Core JS Files   -->
    <script src="<?php echo base_url();?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>


    <link href="<?php echo base_url(); ?>assets/megamenu/webslidemenu.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/megamenu/cst.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>assets/megamenu/webslidemenu.js" type="text/javascript"></script>





</head>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('are_you_sure');?>';
</script>
<style>
    .validate_error {
        border: 1px solid red;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }
</style>

<body>

    <header id="header" class="type_6">
        <div class="bottom_part">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-5 col-sm-5">
                        <a href="https://brandsvalley.net/" class="logo">
                            <img src="https://brandsvalley.net/assets/web/images/logo.png" alt="Brands Valley">
                        </a>
                    </div>
                    <div class="col-lg-7 col-md-8 col-sm-8">

                        <form method="post" action="https://brandsvalley.net//home/setRegion">

                            <ul class="formsection pull-right">
                                <li class="col-lg-4 col-md-4 col-sm-12 padding-5px">
                                    <select class="open_categories  active" id="country" name="country" onchange="getStatesWithCountry(2);">
                                        <option value="">Select Country</option>
                                        <option value="22" class="animated_item">Belize</option>
                                        <option value="23" class="animated_item">Benin</option>
                                        <option value="24" class="animated_item">Bermuda</option>
                                        <option value="25" class="animated_item">Bhutan</option>
                                        <option value="26" class="animated_item">Bolivia</option>
                                        <option value="27" class="animated_item">Bosnia and Herzegovina</option>
                                        <option value="28" class="animated_item">Botswana</option>
                                        <option value="29" class="animated_item">Bouvet Island</option>
                                        <option value="30" class="animated_item">Brazil</option>
                                        <option value="31" class="animated_item">British Indian Ocean Territory</option>
                                        <option value="32" class="animated_item">Brunei</option>
                                        <option value="33" class="animated_item">Bulgaria</option>
                                        <option value="34" class="animated_item">Burkina Faso</option>
                                        <option value="35" class="animated_item">Burundi</option>
                                        <option value="36" class="animated_item">Cambodia</option>
                                        <option value="37" class="animated_item">Cameroon</option>
                                        <option value="38" class="animated_item">Canada</option>
                                        <option value="39" class="animated_item">Cape Verde</option>
                                        <option value="40" class="animated_item">Cayman Islands</option>
                                        <option value="41" class="animated_item">Central African Republic</option>
                                        <option value="42" class="animated_item">Chad</option>
                                        <option value="43" class="animated_item">Chile</option>
                                        <option value="44" class="animated_item">China</option>
                                        <option value="45" class="animated_item">Christmas Island</option>
                                        <option value="46" class="animated_item">Cocos (Keeling) Islands</option>
                                        <option value="47" class="animated_item">Colombia</option>
                                        <option value="48" class="animated_item">Comoros</option>
                                        <option value="49" class="animated_item">Republic Of The Congo</option>
                                        <option value="50" class="animated_item">Democratic Republic Of The Congo</option>
                                        <option value="51" class="animated_item">Cook Islands</option>
                                        <option value="52" class="animated_item">Costa Rica</option>
                                        <option value="53" class="animated_item">Cote D'Ivoire (Ivory Coast)</option>
                                        <option value="54" class="animated_item">Croatia (Hrvatska)</option>
                                        <option value="55" class="animated_item">Cuba</option>
                                        <option value="56" class="animated_item">Cyprus</option>
                                        <option value="57" class="animated_item">Czech Republic</option>
                                        <option value="58" class="animated_item">Denmark</option>
                                        <option value="59" class="animated_item">Djibouti</option>
                                        <option value="60" class="animated_item">Dominica</option>
                                        <option value="61" class="animated_item">Dominican Republic</option>
                                        <option value="62" class="animated_item">East Timor</option>
                                        <option value="63" class="animated_item">Ecuador</option>
                                        <option value="64" class="animated_item">Egypt</option>
                                        <option value="65" class="animated_item">El Salvador</option>
                                        <option value="66" class="animated_item">Equatorial Guinea</option>
                                        <option value="67" class="animated_item">Eritrea</option>
                                        <option value="68" class="animated_item">Estonia</option>
                                        <option value="69" class="animated_item">Ethiopia</option>
                                        <option value="70" class="animated_item">External Territories of Australia</option>
                                        <option value="71" class="animated_item">Falkland Islands</option>
                                        <option value="72" class="animated_item">Faroe Islands</option>
                                        <option value="73" class="animated_item">Fiji Islands</option>
                                        <option value="74" class="animated_item">Finland</option>
                                        <option value="75" class="animated_item">France</option>
                                        <option value="76" class="animated_item">French Guiana</option>
                                        <option value="77" class="animated_item">French Polynesia</option>
                                        <option value="78" class="animated_item">French Southern Territories</option>
                                        <option value="79" class="animated_item">Gabon</option>
                                        <option value="80" class="animated_item">Gambia The</option>
                                        <option value="81" class="animated_item">Georgia</option>
                                        <option value="82" class="animated_item">Germany</option>
                                        <option value="83" class="animated_item">Ghana</option>
                                        <option value="84" class="animated_item">Gibraltar</option>
                                        <option value="85" class="animated_item">Greece</option>
                                        <option value="86" class="animated_item">Greenland</option>
                                        <option value="87" class="animated_item">Grenada</option>
                                        <option value="88" class="animated_item">Guadeloupe</option>
                                        <option value="89" class="animated_item">Guam</option>
                                        <option value="90" class="animated_item">Guatemala</option>
                                        <option value="91" class="animated_item">Guernsey and Alderney</option>
                                        <option value="92" class="animated_item">Guinea</option>
                                        <option value="93" class="animated_item">Guinea-Bissau</option>
                                        <option value="94" class="animated_item">Guyana</option>
                                        <option value="95" class="animated_item">Haiti</option>
                                        <option value="96" class="animated_item">Heard and McDonald Islands</option>
                                        <option value="97" class="animated_item">Honduras</option>
                                        <option value="98" class="animated_item">Hong Kong S.A.R.</option>
                                        <option value="99" class="animated_item">Hungary</option>
                                        <option value="100" class="animated_item">Iceland</option>
                                        <option value="101" class="animated_item">India</option>
                                        <option value="102" class="animated_item">Indonesia</option>
                                        <option value="103" class="animated_item">Iran</option>
                                        <option value="104" class="animated_item">Iraq</option>
                                        <option value="105" class="animated_item">Ireland</option>
                                        <option value="106" class="animated_item">Israel</option>
                                        <option value="107" class="animated_item">Italy</option>
                                        <option value="108" class="animated_item">Jamaica</option>
                                        <option value="109" class="animated_item">Japan</option>
                                        <option value="110" class="animated_item">Jersey</option>
                                        <option value="111" class="animated_item">Jordan</option>
                                        <option value="112" class="animated_item">Kazakhstan</option>
                                        <option value="113" class="animated_item">Kenya</option>
                                        <option value="114" class="animated_item">Kiribati</option>
                                        <option value="115" class="animated_item">Korea North</option>
                                        <option value="116" class="animated_item">Korea South</option>
                                        <option value="117" class="animated_item">Kuwait</option>
                                        <option value="118" class="animated_item">Kyrgyzstan</option>
                                        <option value="119" class="animated_item">Laos</option>
                                        <option value="120" class="animated_item">Latvia</option>
                                        <option value="121" class="animated_item">Lebanon</option>
                                        <option value="122" class="animated_item">Lesotho</option>
                                        <option value="123" class="animated_item">Liberia</option>
                                        <option value="124" class="animated_item">Libya</option>
                                        <option value="125" class="animated_item">Liechtenstein</option>
                                        <option value="126" class="animated_item">Lithuania</option>
                                        <option value="127" class="animated_item">Luxembourg</option>
                                        <option value="128" class="animated_item">Macau S.A.R.</option>
                                        <option value="129" class="animated_item">Macedonia</option>
                                        <option value="130" class="animated_item">Madagascar</option>
                                        <option value="131" class="animated_item">Malawi</option>
                                        <option value="132" class="animated_item">Malaysia</option>
                                        <option value="133" class="animated_item">Maldives</option>
                                        <option value="134" class="animated_item">Mali</option>
                                        <option value="135" class="animated_item">Malta</option>
                                        <option value="136" class="animated_item">Man (Isle of)</option>
                                        <option value="137" class="animated_item">Marshall Islands</option>
                                        <option value="138" class="animated_item">Martinique</option>
                                        <option value="139" class="animated_item">Mauritania</option>
                                        <option value="140" class="animated_item">Mauritius</option>
                                        <option value="141" class="animated_item">Mayotte</option>
                                        <option value="142" class="animated_item">Mexico</option>
                                        <option value="143" class="animated_item">Micronesia</option>
                                        <option value="144" class="animated_item">Moldova</option>
                                        <option value="145" class="animated_item">Monaco</option>
                                        <option value="146" class="animated_item">Mongolia</option>
                                        <option value="147" class="animated_item">Montserrat</option>
                                        <option value="148" class="animated_item">Morocco</option>
                                        <option value="149" class="animated_item">Mozambique</option>
                                        <option value="150" class="animated_item">Myanmar</option>
                                        <option value="151" class="animated_item">Namibia</option>
                                        <option value="152" class="animated_item">Nauru</option>
                                        <option value="153" class="animated_item">Nepal</option>
                                        <option value="154" class="animated_item">Netherlands Antilles</option>
                                        <option value="155" class="animated_item">Netherlands The</option>
                                        <option value="156" class="animated_item">New Caledonia</option>
                                        <option value="157" class="animated_item">New Zealand</option>
                                        <option value="158" class="animated_item">Nicaragua</option>
                                        <option value="159" class="animated_item">Niger</option>
                                        <option value="160" class="animated_item">Nigeria</option>
                                        <option value="161" class="animated_item">Niue</option>
                                        <option value="162" class="animated_item">Norfolk Island</option>
                                        <option value="163" class="animated_item">Northern Mariana Islands</option>
                                        <option value="164" class="animated_item">Norway</option>
                                        <option value="165" class="animated_item">Oman</option>
                                        <option value="166" class="animated_item">Pakistan</option>
                                        <option value="167" class="animated_item">Palau</option>
                                        <option value="168" class="animated_item">Palestinian Territory Occupied</option>
                                        <option value="169" class="animated_item">Panama</option>
                                        <option value="170" class="animated_item">Papua new Guinea</option>
                                        <option value="171" class="animated_item">Paraguay</option>
                                        <option value="172" class="animated_item">Peru</option>
                                        <option value="173" class="animated_item">Philippines</option>
                                        <option value="174" class="animated_item">Pitcairn Island</option>
                                        <option value="175" class="animated_item">Poland</option>
                                        <option value="176" class="animated_item">Portugal</option>
                                        <option value="177" class="animated_item">Puerto Rico</option>
                                        <option value="178" class="animated_item">Qatar</option>
                                        <option value="179" class="animated_item">Reunion</option>
                                        <option value="180" class="animated_item">Romania</option>
                                        <option value="181" class="animated_item">Russia</option>
                                        <option value="182" class="animated_item">Rwanda</option>
                                        <option value="183" class="animated_item">Saint Helena</option>
                                        <option value="184" class="animated_item">Saint Kitts And Nevis</option>
                                        <option value="185" class="animated_item">Saint Lucia</option>
                                        <option value="186" class="animated_item">Saint Pierre and Miquelon</option>
                                        <option value="187" class="animated_item">Saint Vincent And The Grenadines</option>
                                        <option value="188" class="animated_item">Samoa</option>
                                        <option value="189" class="animated_item">San Marino</option>
                                        <option value="190" class="animated_item">Sao Tome and Principe</option>
                                        <option value="191" class="animated_item">Saudi Arabia</option>
                                        <option value="192" class="animated_item">Senegal</option>
                                        <option value="193" class="animated_item">Serbia</option>
                                        <option value="194" class="animated_item">Seychelles</option>
                                        <option value="195" class="animated_item">Sierra Leone</option>
                                        <option value="196" class="animated_item">Singapore</option>
                                        <option value="197" class="animated_item">Slovakia</option>
                                        <option value="198" class="animated_item">Slovenia</option>
                                        <option value="199" class="animated_item">Smaller Territories of the UK</option>
                                        <option value="200" class="animated_item">Solomon Islands</option>
                                        <option value="201" class="animated_item">Somalia</option>
                                        <option value="202" class="animated_item">South Africa</option>
                                        <option value="203" class="animated_item">South Georgia</option>
                                        <option value="204" class="animated_item">South Sudan</option>
                                        <option value="205" class="animated_item">Spain</option>
                                        <option value="206" class="animated_item">Sri Lanka</option>
                                        <option value="207" class="animated_item">Sudan</option>
                                        <option value="208" class="animated_item">Suriname</option>
                                        <option value="209" class="animated_item">Svalbard And Jan Mayen Islands</option>
                                        <option value="210" class="animated_item">Swaziland</option>
                                        <option value="211" class="animated_item">Sweden</option>
                                        <option value="212" class="animated_item">Switzerland</option>
                                        <option value="213" class="animated_item">Syria</option>
                                        <option value="214" class="animated_item">Taiwan</option>
                                        <option value="215" class="animated_item">Tajikistan</option>
                                        <option value="216" class="animated_item">Tanzania</option>
                                        <option value="217" class="animated_item">Thailand</option>
                                        <option value="218" class="animated_item">Togo</option>
                                        <option value="219" class="animated_item">Tokelau</option>
                                        <option value="220" class="animated_item">Tonga</option>
                                        <option value="221" class="animated_item">Trinidad And Tobago</option>
                                        <option value="222" class="animated_item">Tunisia</option>
                                        <option value="223" class="animated_item">Turkey</option>
                                        <option value="224" class="animated_item">Turkmenistan</option>
                                        <option value="225" class="animated_item">Turks And Caicos Islands</option>
                                        <option value="226" class="animated_item">Tuvalu</option>
                                        <option value="227" class="animated_item">Uganda</option>
                                        <option value="228" class="animated_item">Ukraine</option>
                                        <option value="229" class="animated_item">United Arab Emirates</option>
                                        <option value="230" class="animated_item">United Kingdom</option>
                                        <option value="231" class="animated_item">United States</option>
                                        <option value="232" class="animated_item">United States Minor Outlying Islands</option>
                                        <option value="233" class="animated_item">Uruguay</option>
                                        <option value="234" class="animated_item">Uzbekistan</option>
                                        <option value="235" class="animated_item">Vanuatu</option>
                                        <option value="236" class="animated_item">Vatican City State (Holy See)</option>
                                        <option value="237" class="animated_item">Venezuela</option>
                                        <option value="238" class="animated_item">Vietnam</option>
                                        <option value="239" class="animated_item">Virgin Islands (British)</option>
                                        <option value="240" class="animated_item">Virgin Islands (US)</option>
                                        <option value="241" class="animated_item">Wallis And Futuna Islands</option>
                                        <option value="242" class="animated_item">Western Sahara</option>
                                        <option value="243" class="animated_item">Yemen</option>
                                        <option value="244" class="animated_item">Yugoslavia</option>
                                        <option value="245" class="animated_item">Zambia</option>
                                        <option value="246" class="animated_item">Zimbabwe</option>
                                        <option value="247" class="animated_item">holland</option>
                                        <option value="248" class="animated_item">Afghanistan</option>
                                    </select>
                                </li>
                                <li class="col-lg-4 col-md-4 col-sm-12 padding-5px">
                                    <select class="open_categories " id="states" name="states" onchange="getCitiesWithStates(2);">

                                        <option value="">Select State</option>

                                    </select>
                                </li>
                                <li class="col-lg-3 col-md-3 col-sm-12 padding-5px">
                                    <select class="open_categories" id="city" name="city">
                                        <option value="">Select City</option>
                                    </select>
                                </li>
                                <li class="col-lg-1 col-md-1 col-sm-12 padding-5px">

                                    <button type="submit" class="button_blue" name="goForCookies" id="goForCookies" style="padding: 10px 18px;">Go</button>

                                </li>

                            </ul>

                        </form>

                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2">

                        <ul class="social_btns pull-right">
                            <li>
                                <a target="_blank" href="https://www.facebook.com/BusinessUp1/" class="icon_btn middle_btn social_facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/111942499114878334046" target="_blank" class="icon_btn middle_btn social_googleplus"><i class="fa fa-google"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/brands_valley" target="_blank" class="icon_btn middle_btn  social_twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/brandsvalleyinternational/" target="_blank" class="icon_btn middle_btn social_instagram"><i class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- row -->
            </div>
            <!-- container-fluid -->

        </div>
        <!-- Bottom_part -->

    </header>

    <div class="wsmenucontainer clearfix">
        <div class="overlapblackbg"></div>

        <div class="wsmobileheader clearfix">
            <a id="wsnavtoggle" class="animated-arrow"><span></span></a>
            <a class="smallogo"><img src="images/sml-logo.png" alt="" /></a>
            <a class="callusicon" href="tel:123456789"><span class="fa fa-phone"></span></a>
        </div>

        <div class="headerfull">
            <!--Main Menu HTML Code-->
            <div class="wsmain">

                <div class="smllogo">
                    <a href="#">
                        <div class="smllogo-text">
                            <i class="fa fa-map-marker" aria-hidden="true"></i> Delever to
                            <div class="smllogo-text__country">Pakistan</div>
                        </div>
                    </a>
                </div>

                <nav class="wsmenu clearfix">
                    <ul class="mobile-sub wsmenu-list">

                        <li><a href="#" class="navtext"><span>Shop By</span> <span>Department</span></a>

                            <div class="wsshoptabing wstfullmenubg01 clearfix" id="allcategories">
                                <i class="up-arrow"></i>
                                <div class="wsshoptabingwp clearfix">
                                    <ul class="wstabitem clearfix">
                                        <li class="wsshoplink-active"><a href="#wstabcontent01"> Women's Clothing &amp; Accessories</a></li>
                                        <li><a href="#wstabcontent02"> Men's Clothing &amp; Accessories</a></li>
                                        <li><a href="#wstabcontent03"> Movies, Music &amp; Games</a></li>
                                        <li><a href="#wstabcontent04">Home &amp; Kitchen</a></li>
                                        <li><a href="#wstabcontent05">Electronics Appliances</a></li>
                                        <li><a href="#wstabcontent06">Computers &amp; Accessories</a></li>
                                        <li><a href="#wstabcontent07">Auto accessories</a></li>
                                        <li><a href="#wstabcontent08">Health Care Products</a></li>
                                    </ul>

                                    <div class="wstabcontent clearfix">

                                        <div id="wstabcontent01" class="wsshoptab-active clearfix">

                                            <div class="wstmegamenucoll clearfix">
                                                <div class="wstheading">Women's Clothing</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Sleepwear &amp; Robes</a></li>
                                                    <li><a href="#">Shapewear </a></li>
                                                    <li><a href="#">Tops &amp; shirts</a></li>
                                                    <li><a href="#">Sweatshirts</a></li>
                                                    <li><a href="#">Fashion Hoodies <span class="wstmenutag greentag">New</span></a></li>
                                                    <li><a href="#">Jeans &amp; Trousers</a></li>
                                                    <li><a href="#">Capris and Shorts <span class="wstmenutag bluetag">Trending</span></a></li>
                                                    <li><a href="#">Leggings</a></li>
                                                    <li><a href="#">Swimsuits &amp; Cover Ups</a></li>
                                                    <li><a href="#">Lingerie, Sleep &amp; Lounge</a></li>
                                                    <li><a href="#">Inner &amp; Nightwear</a> <span class="wstmenutag redtag">Sale</span></li>

                                                    <li><a href="#">Jumpsuits, Rompers &amp; Overalls</a></li>
                                                    <li><a href="#">Coats, Jackets &amp; Vests</a></li>
                                                    <li><a href="#">Suiting &amp; Blazers </a></li>
                                                    <li><a href="#">Socks &amp; Hosiery</a></li>
                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Handbags & Wallets</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Clutches</a> </li>
                                                    <li><a href="#">Cross-Body Bags</a> </li>
                                                    <li><a href="#">Evening Bags</a> </li>
                                                    <li><a href="#">Shoulder Bags</a> <span class="wstmenutag orangetag">Hot</span></li>
                                                    <li><a href="#">Top-Handle Bags</a> </li>
                                                    <li><a href="#">Wristlets</a> </li>
                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Accessories</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Handbag Accessories</a> </li>
                                                    <li><a href="#">Sunglasses Accessories</a> </li>
                                                    <li><a href="#">Eyewear Accessories</a> </li>
                                                    <li><a href="#">Scarves & Wraps</a> </li>
                                                    <li><a href="#">Gloves & Mittens</a> </li>
                                                    <li><a href="#">Hats & Caps</a> </li>
                                                    <li><a href="#">Handbag Accessories</a> </li>
                                                </ul>

                                            </div>

                                        </div>

                                        <div id="wstabcontent02" class="clearfix">

                                            <div class="wstmegamenucoll clearfix">
                                                <div class="wstheading">Men's Clothing</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Shirts <span class="wstmenutag greentag">New</span></a></li>
                                                    <li><a href="#">Fashion Hoodies & Sweatshirts </a></li>
                                                    <li><a href="#">Sweaters</a></li>
                                                    <li><a href="#">Jackets & Coats</a></li>
                                                    <li><a href="#">Jeans </a></li>
                                                    <li><a href="#">Pants &amp; Trousers</a></li>
                                                    <li><a href="#">Capris and Shorts </a></li>
                                                    <li><a href="#">Swim</a></li>
                                                    <li><a href="#">Suits & Sport Coats</a></li>
                                                    <li><a href="#">Underwear</a></li>
                                                    <li><a href="#">Socks</a> </li>

                                                    <li><a href="#">Sleep & Lounge</a></li>
                                                    <li><a href="#">T-Shirts & Tanks <span class="wstmenutag redtag">20% off Sale</span></a></li>
                                                    <li><a href="#">Active</a></li>
                                                    <li><a href="#">Sport Coats <span class="wstmenutag bluetag">Trending</span></a></li>
                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Shoes & Wallets</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Athletic</a> </li>
                                                    <li><a href="#">Boots</a> <span class="wstmenutag orangetag">Exclusive</span></li>
                                                    <li><a href="#">Fashion Sneakers</a> </li>
                                                    <li><a href="#">Loafers & Slip-Ons</a> </li>
                                                    <li><a href="#">Mules & Clogs</a> </li>
                                                    <li><a href="#">Outdoor</a> </li>
                                                    <li><a href="#">Oxfords</a> </li>
                                                    <li><a href="#">Sandals</a> </li>
                                                    <li><a href="#">Slippers</a> </li>
                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Accessories</div>
                                                <ul class="wstliststy01">
                                                    <li><a href="#">Belts</a> </li>
                                                    <li><a href="#">Suspenders</a> </li>
                                                    <li><a href="#">Eyewear Accessories</a> </li>
                                                    <li><a href="#">Neckties</a> </li>
                                                    <li><a href="#">Bow Ties & Cummerbunds</a> </li>
                                                    <li><a href="#">Collar Stays</a> </li>
                                                    <li><a href="#">Gloves & Mittens</a> </li>
                                                </ul>

                                            </div>
                                            <div class="wstmegamenucolr clearfix">
                                                <a href="#"><img src="images/man-ad-img.jpg" alt=""></a>
                                            </div>

                                        </div>

                                        <div id="wstabcontent03" class="clearfix">

                                            <ul class="wstliststy02">
                                                <li class="wstheading">Latest Movies</li>
                                                <li><a href="#">Action & Adventure <span class="wstmenutag greentag">New</span></a></li>
                                                <li><a href="#">Bollywood </a></li>
                                                <li><a href="#">Comedy</a></li>
                                                <li><a href="#">Documentary</a></li>
                                                <li><a href="#">Educational</a></li>
                                                <li><a href="#">Exercise & Fitness </a></li>
                                                <li><a href="#">Faith & Spirituality</a></li>
                                                <li><a href="#">Fantasy</a></li>
                                                <li><a href="#">Romance</a></li>
                                                <li><a href="#">Science Fiction</a></li>
                                            </ul>
                                            <ul class="wstliststy02">
                                                <li class="wstheading">Newest Games</li>
                                                <li><a href="#">PlayStation 4 </a> </li>
                                                <li><a href="#">Xbox One </a> <span class="wstmenutag orangetag">Most Viewed</span></li>
                                                <li><a href="#">Xbox 360 </a> </li>
                                                <li><a href="#">Nintendo DS</a> </li>
                                                <li><a href="#">PlayStation Vita </a> </li>
                                                <li><a href="#">Retro Gaming</a> </li>
                                                <li><a href="#">Digital Games</a> </li>
                                                <li><a href="#">Microconsoles</a> </li>
                                                <li><a href="#">Kids & Family </a> </li>
                                                <li><a href="#">Accessories </a> </li>
                                            </ul>
                                            <ul class="wstliststy02">
                                                <li class="wstheading">Popular Music Genre</li>
                                                <li><a href="#">Alternative & Indie Rock</a> </li>
                                                <li><a href="#">Broadway & Vocalists</a> </li>
                                                <li><a href="#">Children's Music</a> </li>
                                                <li><a href="#">Christian <span class="wstmenutag bluetag">50% off</span></a> </li>
                                                <li><a href="#">Classical</a> </li>
                                                <li><a href="#">Classic Rock</a> </li>
                                                <li><a href="#">Comedy & Miscellaneous  </a> </li>
                                                <li><a href="#">Country</a> </li>
                                                <li><a href="#">Dance & Electronic</a> </li>
                                                <li><a href="#">Latin Music</a> </li>
                                            </ul>
                                            <ul class="wstliststy02">
                                                <li class="wstheading">Popular Music Genre</li>
                                                <li><a href="#">Alternative & Indie Rock</a> </li>
                                                <li><a href="#">Broadway & Vocalists</a> </li>
                                                <li><a href="#">Children's Music <span class="wstmenutag redtag">Discounted</span></a></a>
                                                </li>
                                                <li><a href="#">Christian</a> </li>
                                                <li><a href="#">Classical</a> </li>
                                                <li><a href="#">Classic Rock</a> </li>
                                                <li><a href="#">Comedy & Miscellaneous</a> </li>
                                                <li><a href="#">Country</a> </li>
                                                <li><a href="#">Dance & Electronic</a> </li>
                                                <li><a href="#">Latin Music</a> </li>
                                            </ul>

                                            <div class="wstadsize01 clearfix">
                                                <a href="#"><img src="images/ad-size01.jpg" alt=""></a>
                                            </div>
                                            <div class="wstadsize02 clearfix">
                                                <a href="#"><img src="images/ad-size02.jpg" alt=""></a>
                                            </div>

                                        </div>

                                        <div id="wstabcontent04" class="clearfix">

                                            <ul class="wstliststy02">
                                                <li class="wstheading">Home Appliances</li>
                                                <li><a href="#">Air Conditioners <span class="wstmenutag greentag">New</span></a></li>
                                                <li><a href="#">Air Coolers </a></li>
                                                <li><a href="#">Fans</a></li>
                                                <li><a href="#">Microwaves</a></li>
                                                <li><a href="#">Refrigerators</a></li>
                                                <li><a href="#">Washing Machines </a></li>
                                                <li><a href="#">Bedsheets </a></li>
                                                <li><a href="#">Jars & Containers </a></li>
                                                <li><a href="#">LED & CFL bulbs </a></li>
                                                <li><a href="#">Drying Racks </a></li>
                                                <li><a href="#">Laundry Baskets</a> <span class="wstmenutag orangetag">New</span></li>
                                                <li><a href="#">Vases</a></li>
                                                <li><a href="#">Clocks</a></li>
                                                <li><a href="#">Washing Machines </a></li>
                                                <li><a href="#">Bedsheets </a></li>
                                            </ul>
                                            <ul class="wstliststy02">
                                                <li class="wstheading">Kitchen Appliances</li>
                                                <li><a href="#">Air Fryers </a></li>
                                                <li><a href="#">Choppers </a></li>
                                                <li><a href="#">Espresso Machines</a></li>
                                                <li><a href="#">Food Processors</a> <span class="wstmenutag bluetag">Popular</span></li>
                                                <li><a href="#">Hand Blenders</a></li>
                                                <li><a href="#">Induction Cooktops</a></li>
                                                <li><a href="#">Juicers</a></li>
                                                <li><a href="#">Microwave Ovens</a></li>
                                                <li><a href="#">Mixers & Grinders</a></li>
                                                <li><a href="#">Ovens</a></li>
                                                <li><a href="#">Rice Cookers</a></li>
                                                <li><a href="#">Stand Mixers</a></li>
                                                <li><a href="#">Sandwich Makers</a></li>
                                                <li><a href="#">Tandoor & Grills</a></li>
                                                <li><a href="#">Toasters</a></li>
                                            </ul>

                                        </div>

                                        <div id="wstabcontent05" class="clearfix">

                                            <ul class="wstliststy02">
                                                <li><img src="images/ele-menu-img01.jpg" alt=" "></li>
                                                <li class="wstheading">TV & Audio</li>
                                                <li><a href="#">4K Ultra HD TVs </a></li>
                                                <li><a href="#">Curved TVs </a></li>
                                                <li><a href="#">LED & LCD TVs</a></li>
                                                <li><a href="#">OLED TVs</a> <span class="wstmenutag bluetag">Popular</span></li>
                                                <li><a href="#">Plasma TVs</a></li>
                                                <li><a href="#">Smart TVs</a></li>
                                                <li><a href="#">Home Theater</a></li>
                                                <li><a href="#">Wireless & streaming</a></li>
                                                <li><a href="#">Stereo System</a></li>
                                                <li><a href="#">Turntables & Accessories</a></li>
                                                <li><a href="#">Compact Radios</a></li>
                                                <li><a href="#">Headphones</a></li>
                                            </ul>

                                            <ul class="wstliststy02">
                                                <li><img src="images/ele-menu-img02.jpg" alt=" "></li>
                                                <li class="wstheading">Camera, Photo & Video</li>
                                                <li><a href="#">Accessories <span class="wstcount">(1145)</span></a></li>
                                                <li><a href="#">Bags & Cases <span class="wstcount">(445)</span></a></li>
                                                <li><a href="#">Binoculars & Scopes <span class="wstcount">(45)</span></a></li>
                                                <li><a href="#">Digital Cameras <span class="wstcount">(845)</span></a> </li>
                                                <li><a href="#">Film Photography <span class="wstcount">(245)</span></a> <span class="wstmenutag bluetag">Popular</span></li>
                                                <li><a href="#">Flashes <span class="wstcount">(105)</span></a></li>
                                                <li><a href="#">Lenses <span class="wstcount">(445)</span></a></li>
                                                <li><a href="#">Lighting & Studio <span class="wstcount">(225)</span></a></li>
                                                <li><a href="#">Video <span class="wstcount">(145)</span></a></li>
                                                <li><a href="#">Printers & Scanners <span class="wstcount">(305)</span></a></li>
                                                <li><a href="#">Projectors <span class="wstcount">(745)</span></a></li>
                                                <li><a href="#">Tripods & Monopods  <span class="wstcount">(805)</span></a></li>
                                            </ul>

                                            <ul class="wstliststy02">
                                                <li><img src="images/ele-menu-img03.jpg" alt=" "></li>
                                                <li class="wstheading">Cell Phones & Accessories</li>
                                                <li><a href="#">Unlocked Cell Phones </a></li>
                                                <li><a href="#">Smartwatches </a></li>
                                                <li><a href="#">Carrier Phones</a></li>
                                                <li><a href="#">Cell Phone Cases</a> <span class="wstmenutag orangetag">Hot</span></li>
                                                <li><a href="#">Apple Cell Phones</a></li>
                                                <li><a href="#">Bluetooth Headsets</a></li>
                                                <li><a href="#">Cell Phone Accessories</a></li>
                                                <li><a href="#">Fashion Tech</a></li>
                                                <li><a href="#">Headphone</a></li>
                                                <li><a href="#">Smartwatches</a></li>
                                                <li><a href="#">Cell Phones</a></li>
                                                <li><a href="#">Low Cell Phones</a></li>
                                            </ul>

                                            <ul class="wstliststy02">
                                                <li><img src="images/ele-menu-img04.jpg" alt=" "></li>
                                                <li class="wstheading">Wearable Device</li>
                                                <li><a href="#">Activity Trackers </a></li>
                                                <li><a href="#">Sports & GPS Watches</a></li>
                                                <li><a href="#">Smart Watches</a> <span class="wstmenutag greentag">New</span></li>
                                                <li><a href="#">Virtual Reality Headsets</a></li>
                                                <li><a href="#">Smart Tracking</a></li>
                                                <li><a href="#">Wearable Cameras</a></li>
                                                <li><a href="#">Smart Glasses</a></li>
                                                <li><a href="#">Kids & Pets</a></li>
                                                <li><a href="#">Smart Sport Accessories</a></li>
                                                <li><a href="#">Healthcare Devices</a></li>
                                                <li><a href="#">Fashion Tech</a></li>
                                                <li><a href="#">Daily workout </a></li>
                                            </ul>

                                        </div>

                                        <div id="wstabcontent06" class="clearfix">

                                            <div class="wstmegamenucoll01 clearfix">
                                                <div class="wstheading">Monitors <a href="#" class="wstmorebtn">View All</a></div>
                                                <ul class="wstliststy03">
                                                    <li><a href="#">50 Inches & Above <span class="wstmenutag greentag">New</span></a></li>
                                                    <li><a href="#">40 to 49.9 Inches  </a></li>
                                                    <li><a href="#">30 to 39.9 Inches</a></li>
                                                    <li><a href="#">26 to 29.9 Inches</a></li>
                                                    <li><a href="#">18 to 19.9 Inches</a></li>
                                                    <li><a href="#">16 to 17.9 Inches</a></li>
                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Printers <a href="#" class="wstmorebtn">View All</a></div>
                                                <ul class="wstliststy03">
                                                    <li><a href="#">All-In-One</a> </li>
                                                    <li><a href="#">Copying </a> <span class="wstmenutag orangetag">Exclusive</span></li>
                                                    <li><a href="#">Faxing </a> </li>
                                                    <li><a href="#">Printing Photo Printing</a> </li>
                                                    <li><a href="#">Printing Only</a> </li>
                                                    <li><a href="#">Scanning </a> </li>
                                                </ul>

                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Software <a href="#" class="wstmorebtn">View All</a></div>
                                                <ul class="wstliststy03">
                                                    <li><a href="#">Antivirus & Security</a> </li>
                                                    <li><a href="#">Business & Office</a> <span class="wstmenutag orangetag">Exclusive</span></li>
                                                    <li><a href="#">Web Design</a> </li>
                                                    <li><a href="#">Digital Software</a> </li>
                                                    <li><a href="#">Education & Reference</a> </li>
                                                    <li><a href="#">Lifestyle & Hobbies</a> </li>
                                                    <li><a href="#">Operating Systems</a> </li>
                                                    <li><a href="#">Photography </a> </li>

                                                </ul>
                                                <div class="cl" style="height:8px;"></div>
                                                <div class="wstheading">Accessories <a href="#" class="wstmorebtn">View All</a></div>
                                                <ul class="wstliststy03">
                                                    <li><a href="#">Audio & Video Accessories</a> </li>
                                                    <li><a href="#">Cable Security Devices</a> </li>
                                                    <li><a href="#">Input Devices </a> </li>
                                                    <li><a href="#">Memory Cards</a> </li>
                                                    <li><a href="#">Monitor Accessories</a> </li>
                                                    <li><a href="#">USB Gadgets</a> </li>
                                                </ul>

                                            </div>

                                        </div>

                                        <div id="wstabcontent07" class="clearfix">

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img01.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Interior</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img02.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Styling</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img03.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Utility</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img04.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Spare Parts</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img05.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Protection</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img06.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Cleaning</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img07.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Car Audio</a></li>
                                            </ul>

                                            <ul class="wstliststy04">
                                                <li><img src="images/auto-menu-img08.jpg" alt=" "></li>
                                                <li class="wstheading"><a href="#">Gear & Accessories</a></li>
                                            </ul>

                                        </div>

                                        <div id="wstabcontent08" class="clearfix">

                                            <div class="wstmegamenucolr03 clearfix">
                                                <img src="images/health-menu-img01.jpg" alt="">
                                            </div>

                                            <div class="wstmegamenucoll04 clearfix">
                                                <ul class="wstliststy05 clearfix">
                                                    <li><img src="images/health-menu-img02.jpg" alt=" "></li>
                                                    <li class="wstheading">Health Care</li>
                                                    <li><a href="#">Diabetes </a></li>
                                                    <li><a href="#">Incontinence </a></li>
                                                    <li><a href="#">Cough & Cold</a></li>
                                                    <li><a href="#">Baby & Child Care</a> <span class="wstmenutag bluetag">Popular</span></li>
                                                    <li><a href="#">Women's Health</a></li>
                                                    <li><a href="#">First Aid</a></li>
                                                    <li><a href="#">Smoking Cessation</a></li>
                                                    <li><a href="#">Sleep & Snoring</a></li>
                                                    <li><a href="#">Massage & Relaxation</a></li>
                                                </ul>

                                                <ul class="wstliststy05 clearfix">
                                                    <li><img src="images/health-menu-img03.jpg" alt=" "></li>
                                                    <li class="wstheading">Personal Care</li>
                                                    <li><a href="#">Shaving & Hair Removal</a></li>
                                                    <li><a href="#">Feminine Hygiene</a></li>
                                                    <li><a href="#">Oral Care</a></li>
                                                    <li><a href="#">Foot Care</a> <span class="wstmenutag bluetag">Popular</span></li>
                                                    <li><a href="#">Hand Care</a></li>
                                                    <li><a href="#">Personal Care Appliances</a></li>
                                                    <li><a href="#">Shaving Foams & Creams</a></li>
                                                    <li><a href="#">Hair Removal Creams</a></li>
                                                    <li><a href="#">Hair Care</a></li>
                                                </ul>

                                                <ul class="wstliststy05 clearfix">
                                                    <li><img src="images/health-menu-img04.jpg" alt=" "></li>
                                                    <li class="wstheading">Medical Equipment</li>
                                                    <li><a href="#">Crepe Bandages, Tapes & Supplies </a></li>
                                                    <li><a href="#">Neck Supports</a></li>
                                                    <li><a href="#">Shoulder Supports</a></li>
                                                    <li><a href="#">Arm Supports</a> <span class="wstmenutag bluetag">Popular</span></li>
                                                    <li><a href="#">Elbow Braces</a></li>
                                                    <li><a href="#">Knee & Leg Braces</a></li>
                                                    <li><a href="#">Ankle Braces</a></li>
                                                    <li><a href="#">Foot Supports</a></li>
                                                </ul>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </li>

                        <li class="wssearchbar clearfix">
                            <form class="topmenusearch">
                                <input placeholder="Search Product By Name, Category...">
                                <button class="btnstyle"><i class="searchicon fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </li>

                        <li class="wscarticon clearfix">
                            <a href="#"><i class="fa fa-shopping-basket"></i> <em class="roundpoint">8</em><span class="mobiletext">Shopping Cart</span></a>
                        </li>

                        <li class="wsshopmyaccount clearfix"><a href="#"><i class="fa fa-align-justify"></i>My Account <i class="fa  fa-angle-down"></i></a>
                            <ul class="wsmenu-submenu">
                                <li><a href="#"><i class="fa fa-black-tie"></i>View Profile</a></li>
                                <li><a href="#"><i class="fa fa-heart"></i>My Wishlist</a></li>

                                <li><a href="#"><i class="fa fa-bell"></i>Notnification</a></li>
                                <li><a href="#"><i class="fa fa-question-circle"></i>Help Center</a></li>
                                <li><a href="#"><i class="fa fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>

            </div>
            <!--Menu HTML Code-->
        </div>
    </div>

    <div class="wrapper">
