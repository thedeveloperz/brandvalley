<!-- css - Asif ---->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/fontawesome-stars.min.css">

<section id="signup-form-section">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">person</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Signup</h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <form action="<?php echo base_url();?><?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate autocomplete="off">
                                <input type="hidden" name="form_type" value="save">


                                <div class="row">
                                    <div class="col-sm-12"><h4>Registration Type:</h4></div>
                                </div>
                                 <div class="row">
                                     <div class="col-sm-10 checkbox-radios">
                                         <div class="radio">
                                             <label>
                                                 <input type="radio" name="Type" checked="true" value="customer"><span class="circle"></span><span class="check"></span> Signup as Customer ($50)
                                             </label>
                                         </div>
                                         <div class="radio">
                                             <label>
                                                 <input type="radio" name="Type" value="employee"><span class="circle"></span><span class="check"></span> Signup as Employee ($10)
                                             </label>
                                         </div>
                                     </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="first_name">First Name</label>
                                            <input type="text" name="FirstName" required  class="form-control" id="first_name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="last_name">Last Name</label>
                                            <input type="text" name="LastName" required  class="form-control" id="last_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="email_address">Email Address</label>
                                            <input type="text" name="Email" required  class="form-control" id="email_address" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="telephone">Telephone</label>
                                            <input type="text" name="Phone" required  class="form-control" id="telephone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="password">Password</label>
                                            <input type="password" name="Password" required  class="form-control" id="password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="confirm_password">Confirm Password</label>
                                            <input type="password" name="ConfirmPassword" required  class="form-control" id="Title">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 label-on-left lbl-gender">Gender</label>
                                    <div class="col-sm-10 checkbox-radios">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Gender" checked="true" value="Male"><span class="circle"></span><span class="check"></span> Male
                                            </label>
                                            <label>
                                                <input type="radio" name="Gender" value="Female"><span class="circle"></span><span class="check"></span> Female
                                            </label>
                                        </div>
                                        <div class="radio">
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-4">
                                      <div class="form-group label-floating">
                                          <label class="control-label">&nbsp;</label>
                                          <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="<?php echo lang('choose_country');?>" data-size="7" id="CountryIDSignup" required>
                                              <?php foreach ($countries as $key => $value) { ?>
                                                  <option value="<?php echo $value->CountryID; ?>"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                              <?php }?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group label-floating">
                                          <label class="control-label">&nbsp;</label>
                                          <select name="StateID" class="selectpicker" data-style="select-with-transition" title="<?php echo lang('choose_state');?>" data-size="7" id="StateIDSignup" multiple required>
                                        </select>
                                      </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">&nbsp;</label>
                                        <select name="CityID" class="selectpicker" data-style="select-with-transition" multiple title="<?php echo lang('choose_city');?>" data-size="7" id="CityIDSignup" required>
                                        </select>
                                    </div>
                                </div>
                              </div>


                                <div class="row">
                                    <div class="col-sm-12"><h4>Fee Deposit Confirmation</h4></div>
                                 </div>
                                 <dsiv class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Pay via</label>
                                            <select name="PayVia" class="pay_via selectpicker" data-style="select-with-transition" title="" data-size="7" id="payvia" required>
                                                    <option value="pay_later">Pay Later</option>
                                                    <option value="jazzcash">Jazz Cash</option>
                                                    <option value="easypesa">Easy Pesa</option>
                                                    <option value="upesa">U Pesa</option>
                                                    <option value="ublomni">UBL Omni</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="nic_number">NIC Number</label>
                                            <input type="text" name="Cnic" required  class="form-control" id="nic_number" autocomplete="off">
                                        </div>
                                    </div>
                                </dsiv>

                                <dsiv class="row hide payvia_fields">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="amount_deposit">Amount Deposit</label>
                                            <input type="text" name="Amount"  class="form-control" id="amount_deposit" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="transaction_id">Transaction ID</label>
                                            <input type="text" name="TransactionID"  class="form-control" id="transaction_id" autocomplete="off">
                                        </div>
                                    </div>
                                </dsiv>

                                <dsiv class="row hide payvia_fields">
                                    <!-- <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="amt_deposit">Amount Deposit</label>
                                            <input type="text" name="amount_deposit"  class="form-control" id="amt_deposit" autocomplete="off">
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="deposit_date">Deposit Date</label>
                                            <input type="date" name="DepositDate" class="form-control" value="">
                                        </div>
                                    </div>
                                </dsiv>

                                <div class="form-group text-left m-b-0">
                                    <button class="btn btn-info" type="submit">
                                        <?php echo lang('submit');?>
                                    </button>
                                </div>

                            </form>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>
</section>

<!-- JS Plugins - Asif ---->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script src="<?php echo base_url('assets/plugins/zoomer/jquery.zoom.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>



<script type="text/javascript">

    $(document).ready(function(){

          $(".products-carousel").owlCarousel({
               items : 4, //10 items above 1000px browser width
               itemsDesktop : [1000,5], //5 items between 1000px and 901px
               itemsDesktopSmall : [900,3], // betweem 900px and 601px
               itemsTablet: [600,2], //2 items between 600 and 0
               loop:true,
               nav: true,
               dots: false
          });


    $('#CountryIDSignup').on('change',function(){

            var CountryID = $(this).val();

            if(CountryID == '')
            {
                $('#StateIDSignup').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                        
                        // if(result.array)
                        // {
                        //     alert("hello")
                        //     $(".selectpicker").selectpicker('refresh');
                        // }
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });
       $('#StateIDSignup').on('change',function(){

            var StateID = $(this).val();

            if(StateID == '')
            {
                $('#CityIDSignup').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                        // if(result.array)
                        // {
                        //     //$(".selectpicker").selectpicker('refresh');
                        // }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

       });

       // pay-via select fields hide-show
       $('select.pay_via').on('change', function(){
          var sl_val = $(this).val();
          if(sl_val != 'pay_later'){
            $('.payvia_fields').removeClass('hide').addClass('show');
          }else {
            $('.payvia_fields').removeClass('show').addClass('hide');
          }
       });



    });


</script>
