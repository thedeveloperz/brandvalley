<div class="content">
    <section id="location-section">
        <div class="container-fluid">
            <ul class="location-list clearfix">
                <li>You are here</li>
                <li><?php echo (($CountryData && isset($CountryData[0]->Title) ? $CountryData[0]->Title : ''));?></li>
                <li><?php echo (($StateData && isset($StateData[0]->Title) ? $StateData[0]->Title : ''));?></li>
                <li><?php echo (($CityData && isset($CityData[0]->Title) ? $CityData[0]->Title : ''));?></li>
            </ul>
        </div>
    </section>
<div class="container">
<div class="row">
    <div class="col-md-2">
        <?php if($ProfileTypes){ ?>
        <div class="sub-cats-container">
            <h6>Select</h6>
            <ul class="sub-cats">
                <?php foreach($ProfileTypes as $type){ ?>
                    <li><a class="<?php echo ($type->UserProfileTypeID == $id) ? 'active' : '' ?>" href="<?php echo strtolower(base_url("listings/all/$type->ProfileType")) ?>/<?php echo $type->UserProfileTypeID ?>"><?php echo $type->ProfileType ?></a></li>
                <?php } ?>
            </ul>
        </div><!-- sub-cats-container -->
        <?php } ?>
    </div><!-- col-md-4 -->
    <div class="col-md-10">
        <div class="container-fluid listings">
            <?php if($profiles){ ?>
            <div class="row">
                <?php foreach($profiles as $key => $profile){ ?>
                    <?php if($key % 3 == 0 && $key != 0){ ?>
                    </div><!-- row -->
                    <div class="row">
                    <?php } ?>
                <div class="col-sm-4">
                    <a href="<?php echo base_url('listings/detail') ?>/<?php echo $profile['UserProfileType'] ?>" class="list-box">
                        <div class="list-img">
                            <?php if(!file_exists($profile['Image'])) {
                                $image = base_url("assets/backend/img/no_img.png");
                            }else {
                                $image = base_url($profile['Image']);
                            }
                            ?>
                            <img class="img-responsive" src="<?php echo $image ?>" />
                        </div>
                        <div class="list-content">
                            <h3><?php echo $profile['FullName'] ?></h3>
<!--                            <p class="list-type">--><?php //echo $profile['FullName'] ?><!--</p>-->
                            <p class="list-desc"><?php echo $profile['Email'] ?></p>
                            <p class="list-phone"><i class="fa fa-phone-square" aria-hidden="true"></i>  <?php echo $profile['Phone'] ?></p>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div><!-- row -->
            <?php } else { ?>
                <div class="alert">No <?php echo ucfirst($name)?> found in your selected location.</div>
            <?php } ?>


<!--            <div class="row">-->
<!--                <div class="col-sm-4">-->
<!--                    <a href="#" class="list-box">-->
<!--                        <div class="list-img">-->
<!--                            <img class="img-responsive" src="--><?php //echo base_url('assets/img/burger.jpg'); ?><!--" />-->
<!--                        </div>-->
<!--                        <h3>Username</h3>-->
<!--                        <p class="list-type">Manufacture</p>-->
<!--                        <p class="list-desc">This is very short details, you can view details of this manufacture on its details page.</p>-->
<!--                        <p class="list-phone"><i class="fa fa-phone-square" aria-hidden="true"></i> 0390988333</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="col-sm-4">-->
<!--                    <a href="#" class="list-box">-->
<!--                        <div class="list-img">-->
<!--                            <img class="img-responsive" src="--><?php //echo base_url('assets/img/burger.jpg'); ?><!--" />-->
<!--                        </div>-->
<!--                        <h3>Username</h3>-->
<!--                        <p class="list-type">Manufacture</p>-->
<!--                        <p class="list-desc">This is very short details, you can view details of this manufacture on its details page.</p>-->
<!--                        <p class="list-phone"><i class="fa fa-phone-square" aria-hidden="true"></i> 0390988333</p>-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="col-sm-4">-->
<!--                    <a href="#" class="list-box">-->
<!--                        <div class="list-img">-->
<!--                            <img class="img-responsive" src="--><?php //echo base_url('assets/img/burger.jpg'); ?><!--" />-->
<!--                        </div>-->
<!--                        <h3>Username</h3>-->
<!--                        <p class="list-type">Manufacture</p>-->
<!--                        <p class="list-desc">This is very short details, you can view details of this manufacture on its details page.</p>-->
<!--                        <p class="list-phone"><i class="fa fa-phone-square" aria-hidden="true"></i> 0390988333</p>-->
<!--                    </a>-->
<!--                </div>-->
<!---->
<!--            </div>-->
        </div>
    </div>

    </div><!-- col-md-8 -->
</div><!-- row -->


</div><!-- container -->


</div><!-- main-panel -->

