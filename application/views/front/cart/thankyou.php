<div class="wrapper wrapper-full-page">
    <div class="full-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="card cart-wrapper">
                        <div class="card-header">
                            <h4 class="card-title">Thank You</h4>
                        </div>
                        <div class="card-content">
                            <div class="row">
                                <div class="col-md-12 alert alert-success">Your order has been place successfuly and your order id is <?php echo $OrderTrackID;?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
