<div class="wrapper wrapper-full-page">
    <div class="full-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="card cart-wrapper">
                        <div class="card-header">
                            <h4 class="card-title">Checkout</h4>
                        </div>
                        <div class="card-content">
                            <ul class="nav nav-pills nav-pills-warning">
                                <li class="active">
                                    <a href="#pill1" data-toggle="tab" aria-expanded="true">Order Summary</a>
                                </li>
                                <li class="">
                                    <a href="#pill2" data-toggle="tab" aria-expanded="false">Contact Details</a>
                                </li>
                                <li class="">
                                    <a href="#pill3" data-toggle="tab" aria-expanded="false">Payment</a>
                                </li>
                            </ul>
                            <form method="post" class="form_data" action="<?php echo base_url('checkout/place_order');?>">
                                <input type="hidden" name="PaymentMethod" class="inputPaymentMethod" value="easypesa">
                            <div class="tab-content">
                                <div class="tab-pane active" id="pill1">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="res-table">

                                                <div class="row header">
                                                    <div class="cell">&nbsp;</div>
                                                    <div class="cell">Product</div>
                                                    <div class="cell">Price</div>
                                                    <div class="cell">Qty</div>
                                                    <div class="cell">Total</div>
                                                    <div class="cell">&nbsp;</div>
                                                </div>
                                    <?php 
                                    $total = 0;
                                    if($products){
                                        $count = 1;
                                        
                                           foreach ($products as $key => $value) {
                                            $total = $total + ($value['Quantity'] * $value['Price']);
                                               $image = site_images($value['ProductID'],'ProductImage');
                                               if($image){

                                                if(file_exists($image[0]->ImageName)){
                                                    $image = base_url($image[0]->ImageName);
                                                }else{
                                                    $image = base_url('assets/backend/img.no_image.png');
                                                }

                                               }else{
                                                $image = base_url('assets/backend/img.no_image.png');
                                               }
                                     ?>
                                                <div class="row">
                                                    <div class="cell img-cell" data-title="Name">
                                                        <div class="cart-img-container">
                                                            <img src="<?php echo $image;?>" alt="...">
                                                        </div>
                                                    </div>
                                                    <div class="cell cart-title" data-title="Product">
                                                        <a href="#"><?php echo $value['Title'];  ?></a>
                                                    </div>
                                                    <div class="cell" data-title="Price"><span class="single_price"><?php echo $value['Price']; ?></span></div>
                                                    <div class="cell" data-title="Qty">
                                                        <div class="quantity-box">
                                                            <div class="qty-spinner">
                                                                <input type='button' value='-' data-action="minus" data-single-price="<?php echo $value['Price']; ?>" data-row-num="<?php echo $count; ?>" class='btn_action qtyminus change_quantity' field='quantity-<?php echo $count; ?>' />
                                                                <input type='text' name='quantity' value='<?php echo $value['Quantity']; ?>' class='qty quantity-<?php echo $count; ?>' readonly data-tem-id="<?php echo $value['TempOrderID'];?>" />
                                                                <input type='button' value='+' data-action="plus" data-single-price="<?php echo $value['Price']; ?>" data-row-num="<?php echo $count; ?>" class='btn_action qtyplus change_quantity' field='quantity-<?php echo $count; ?>' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="cell item-total" data-title="Total">
                                                        <span class="ttl-price total_single_price-<?php echo $count; ?>"><?php echo ($value['Quantity'] * $value['Price']  ); ?></span>
                                                    </div>
                                                    <div class="cell delete">
                                                        <button type="button" rel="tooltip" data-placement="left" data-tem-id="<?php echo $value['TempOrderID'];?>" title="" data-action="delete" data-row-num="<?php echo $count; ?>" class="btn_action btn btn-simple" data-original-title="Remove item">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            <?php $count++; } } ?>
                                                <!--<div class="row">
                                                    <div class="cell" data-title="Name">
                                                        <div class="cart-img-container">
                                                            <img src="<?php echo base_url("assets/img/burger.jpg")?>" alt="...">
                                                        </div>
                                                    </div>
                                                    <div class="cell cart-title" data-title="Product">
                                                        <a href="#">Chicken Burder</a>
                                                        <br>
                                                        <small>by McDonals</small>
                                                    </div>
                                                    <div class="cell" data-title="Price"><small>$</small><span class="single_price">10</span></div>
                                                    <div class="cell" data-title="Qty">
                                                        <div class="quantity-box">
                                                            <div class="qty-spinner">
                                                                <input type='button' value='-' data-action="minus" data-single-price="10" data-row-num="2" class='btn_action qtyminus' field='quantity-2' />
                                                                <input type='text' name='quantity' value='1' class='qty quantity-2' readonly />
                                                                <input type='button' value='+' data-action="plus" data-single-price="10" data-row-num="2" class='btn_action qtyplus' field='quantity-2' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="cell" data-title="Total">
                                                        <small>$</small><span class="ttl-price total_single_price-2">10</span>
                                                    </div>
                                                    <div class="cell delete">
                                                        <button type="button" rel="tooltip" data-placement="left" title="" data-action="delete" data-row-num="2" class="btn_action btn btn-simple" data-original-title="Remove item">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </div>
                                                </div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="cart-summary">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Order Summary</h4>
                                                        <p class="category">
                                                            Amount To Pay
                                                        </p>
                                                    </div>
                                                    <div class="card-content text-bold">
                                                        <small>Rs</small><span class="grand_total"><?php echo $total; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
<!--                                            <div class="last-row">-->
<!--                                                <div class="cell">-->
<!--                                                    <ul class="payment-img-list clearfix">-->
<!--                                                        <li><a href=""><img src="https://brandsvalley.net/assets/images/card-img-01.png"></a></li>-->
<!--                                                        <li><a href=""><img src="https://brandsvalley.net/assets/images/card-img-02.png"></a></li>-->
<!--                                                        <li><a href=""><img src="https://brandsvalley.net/assets/images/card-img-03.png"></a></li>-->
<!--                                                        <li><a href=""><img src="https://brandsvalley.net/assets/images/card-img-04.png"></a></li>-->
<!--                                                        <li><a href=""><img src="https://brandsvalley.net/assets/images/card-img-05.png"></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="cell btn-complete-purchase text-right">
                                                <button type="button" data-step="contact_dtls" class="nxt_step btn btn-info btn-round">Contact Detail <i class="material-icons">keyboard_arrow_right</i><div class="ripple-container"></div></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pill2">
                                    <form action="<?php echo base_url();?><?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate autocomplete="off">
                                     <input type="hidden" name="form_type" value="save">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="FullName">Full Name</label>
                                                                    <input type="text" name="Name"  class="form-control" id="FullName">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="email_address">Email Address</label>
                                                                    <input type="text" name="Email"  class="form-control" id="email_address" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="telephone">Telephone</label>
                                                                    <input type="text" name="Telephone"  class="form-control" id="telephone">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="address">Address</label>
                                                                    <input type="text" name="Address"  class="form-control" id="address">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <select name="CountryID" class="selectpicker" data-style="select-with-transition"  title="<?php echo lang('choose_country');?>" data-size="7" id="CountryIDSignup">
                                                                        <?php foreach ($countries as $key => $value) { ?>
                                                                            <option value="<?php echo $value->CountryID; ?>"><?php echo $value->Title.' ('.$value->CountryCode.')'; ?></option>
                                                                        <?php }?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <select name="StateID" class="selectpicker" data-style="select-with-transition" title="<?php echo lang('choose_state');?>" data-size="7" id="StateIDSignup">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <select name="CityID" class="selectpicker" data-style="select-with-transition"  title="<?php echo lang('choose_city');?>" data-size="7" id="CityIDSignup">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="cart-summary">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h4 class="card-title">Order Summary</h4>
                                                                    <p class="category">
                                                                        Amount To Pay
                                                                    </p>
                                                                </div>
                                                                <div class="card-content text-bold">
                                                                    <small>Rs</small><span class="grand_total"><?php echo $total; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="cell btn-complete-purchase text-right">
                                                            <button type="button" data-step="checkout" class="nxt_step btn btn-info btn-round">Pay The Bill <i class="material-icons">keyboard_arrow_right</i><div class="ripple-container"></div></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="pill3">

                                    <div class="card">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <ul class="cartPaymentsList nav nav-pills nav-pills-icons nav-pills-rose nav-stacked" role="tablist">
                                                        <li class="active">
                                                            <a href="#schedule-2" role="tab" data-method="easypesa" data-toggle="tab" aria-expanded="true">
<!--                                                                <img src="--><?php //echo base_url("assets/img/easypay.png") ?><!--">-->
                                                                EasyPesa
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#schedule-2" data-method="jazzcash" role="tab" data-toggle="tab">
<!--                                                                <img src="--><?php //echo base_url("assets/img/jazzcash.png") ?><!--">-->
                                                                JazzCash
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#schedule-2" data-method="upesa" role="tab" data-toggle="tab">
<!--                                                                <img src="--><?php //echo base_url("assets/img/bank-transfer.png") ?><!--">-->
                                                                U Pease
                                                            </a>
                                                        </li>
                                                        <li class="">
                                                            <a href="#schedule-2" data-method="ublomni" role="tab" data-toggle="tab">
<!--                                                                <img src="--><?php //echo base_url("assets/img/bank-transfer.png") ?><!--">-->
                                                                UBL Omni
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="schedule-2">
                                                            <dsiv class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label" for="nic_number">NIC Number</label>
                                                                        <input type="text" name="Cnic"  class="form-control" id="nic_number" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label" for="amount_deposit">Amount Deposit</label>
                                                                        <input type="text" name="AmountDeposit"  class="form-control" id="amount_deposit" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </dsiv>

                                                            <dsiv class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label" for="transaction_id">Transaction ID</label>
                                                                        <input type="text" name="TransactionID"  class="form-control" id="transaction_id" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group label-floating is-focused">
                                                                        <label class="control-label" for="deposit_date">Deposit Date</label>
                                                                        <input type="text" name="DepositDate" class="form-control datepicker" value="01/01/2018">
                                                                    </div>
                                                                </div>
                                                            </dsiv>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="cart-summary">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Order Summary</h4>
                                                                <p class="category">
                                                                    Amount To Pay
                                                                </p>
                                                            </div>
                                                            <div class="card-content text-bold">
                                                                <small>Rs</small><span class="grand_total"><?php echo $total; ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-10 text-right">
                                                    <button type="submit" data-step="checkout" class="nxt_step btn btn-info btn-round">Complete Purchase <i class="material-icons">keyboard_arrow_right</i><div class="ripple-container"></div></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        // This button will increment the value
        $('.qtyplus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('.'+fieldName).val());
            // If is not undefined
            if (!isNaN(currentVal)) {
                // Increment
                $('.'+fieldName).val(currentVal + 1);
            } else {
                // Otherwise put a 0 there
                $('.'+fieldName).val(0);
            }
        });
        // This button will decrement the value till 0
        $(".qtyminus").click(function(e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');

            // Get its current value
            var currentVal = parseInt($('.'+fieldName).val());
            // If it isn't undefined or its greater than 0
            if (!isNaN(currentVal) && currentVal > 1) {
                // Decrement one
                $('.'+fieldName).val(currentVal - 1);
            }
        });


        $('.btn_action').on('click', function () {
            let row_num = $(this).data('row-num');
            let action = $(this).data('action');
            let single_price = parseInt($(this).data('single-price'));
            var quantity = parseInt($('.quantity-'+row_num).val());
            let total_single_price = 0;

        // plus quantity
            if(action == 'plus') {
                total_single_price = single_price * quantity;
            }
        // minus quantity
            if(action == 'minus') {
                total_single_price = single_price * quantity;
            }

        // update single items total price
            $('.total_single_price-'+row_num).html(total_single_price);

        // delete item
            if(action == 'delete') {
                var conf = confirm("Are you sure, you want to delete this item?");
                if(conf === true) {
                    var temp_order_id = $(this).attr('data-tem-id');
                   $.post('<?=base_url()?>cart/delete_cart_item', 
                    {
                        temp_order_id:temp_order_id, 
                        
                    }, function(resp){
                        res = $.parseJSON(resp)
                        if (res.status == 'TRUE') {
                                
                               
                           // $('#openAddToCartConfirmationModal').click();

                            } 

                    });
                    $(this).closest('.row').remove();
                    UpdateGrandTotal();
                }
            }

        // update grand total
            UpdateGrandTotal();

        });

        // update grand total func
        function UpdateGrandTotal() {
            let ttl_prices = $('.ttl-price');
            let p = 0 ;
            ttl_prices.each(function (inx, item) {
                p += parseInt($(this).html());
            });
            $('.grand_total').html(p);
        }

        // next step cart
        $('.nxt_step').on('click', function () {
            let step = $(this).data('step');
            if(step == 'contact_dtls') {
                $('a[href="#pill2"]').trigger('click');
            }
            if(step == 'checkout') {
                $('a[href="#pill3"]').trigger('click');
            }

        });



        $('#CountryIDSignup').on('change',function(){

            var CountryID = $(this).val();

            if(CountryID == '')
            {
                $('#StateIDSignup').html('<option value=""><?php echo lang("choose_state");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({
                    type: "POST",
                    url: base_url + 'index/getCountryStates',
                    data: {
                        'CountryID': CountryID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#StateIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');

                        // if(result.array)
                        // {
                        //     alert("hello")
                        //     $(".selectpicker").selectpicker('refresh');
                        // }
                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

        });
        $('#StateIDSignup').on('change',function(){

            var StateID = $(this).val();

            if(StateID == '')
            {
                $('#CityIDSignup').html('<option value=""><?php echo lang("choose_city");?></option>');
            }
            else
            {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });


                $.ajax({



                    type: "POST",
                    url: base_url + 'index/getStateCities',
                    data: {
                        'StateID': StateID
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                        $('#CityIDSignup').html(result.html);
                        $(".selectpicker").selectpicker('refresh');
                        // if(result.array)
                        // {
                        //     //$(".selectpicker").selectpicker('refresh');
                        // }

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });
            }

        });
        
        // load payment method in hidden input type
        $(".cartPaymentsList li a").on('click', function () {
            let method = $(this).data('method');
            $(".inputPaymentMethod").val(method);
        });


        $('.change_quantity').on('click',function(){
            var row = $(this).attr('data-row-num');
            var value = $('.quantity-'+row).val();
            var id = $('.quantity-'+row).attr('data-tem-id');
            $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

            $.ajax({
                    type: "POST",
                    url: base_url + 'cart/update_cart',
                    data: {
                        'id': id,
                        'value': value
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function(result) {

                    },
                    complete: function() {
                        $.unblockUI();
                    }
                });

        });



    });
</script>