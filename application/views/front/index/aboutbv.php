<div class="wrapper wrapper-full-page">
    <div class="full-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2 class="title">About BrandsValley</h2>
                        <h5 class="description">In today’s digital business world, you need a partner who can help you take advantage of marketing opportunities across a variety of channels in real-time. Brands Valley combines a data-driven approach with knowledge gained from years in digital marketing to deliver outstanding results to our clients. Not only this BV has also setup an e-commerce platform where it is providing services like online shopping cart and employment opportunities for everyone who would like to get benefit from an array of astounding options. </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>