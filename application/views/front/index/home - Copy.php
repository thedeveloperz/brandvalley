<!-- css - Asif ---->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/fontawesome-stars.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">

<section id="top-image-slider">
    <div id="owl-image-slider" class="owl-carousel owl-theme image-slider">
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-01.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-02.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-03.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-04.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-05.jpg') ?>" alt=""></div>
    </div>
</section>

<section id="products-section">
    <div class="products feature-products">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3>Latest Products</h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">
                <?php if(!empty($latest_products)){
                  foreach ($latest_products as $key => $lastest_product) { ?>
                  
                      <div class="item">
                        <div class="product-box">
                            <div class="product-box__img">
                                <img src="<?php echo base_url($lastest_product['ImageName']) ?>" alt="">
                                <div class="product-box__quick-view">
                                    <a href="#" class="product-box__quick-view-btn"><a href="#" class="product-box__quick-view-btn"><i class="fa fa-search" aria-hidden="true"></i></a></a>
                                </div>
                            </div>
                            <div class="product-box__rating">
                                <select class="rating-box">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3" selected>3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <h3 class="product-box__title"><?php echo $lastest_product['Title']; ?></h3>
                            <div class="product-box__btn-box">
<!--                            <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                <div class="ripple-container"></div>-->
<!--                            </button>-->
                            <a href="<?php echo base_url('product/details/'.$lastest_product['ProductID']);?>" class="product-box__dtl-btn">View Full Details</a>
                            </div>
                        </div>
                    </div>
                  
                  <?php 
                  }
                }
                ?>
                  
              </div>
            </div>
        </div>
    </div><!-- products.feature-products -->
    <?php if($top_categories){
              foreach ($top_categories as $key => $cat_products) { 
                if(!empty($cat_products['products'])){
                ?>

    <div class="products mostsold-products">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3><?php echo $cat_products['Title'];?></h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">

                <?php foreach ($cat_products['products'] as $key => $product) { ?>
                 
               
                  <div class="item">
                      <div class="product-box">
                          <div class="product-box__img">
                              <img src="<?php echo base_url($product['ImageName']); ?>" alt="">
                              <div class="product-box__quick-view">
                                  <a href="javascript:void(0);" class="product-box__quick-view-btn"><i class="fa fa-search" aria-hidden="true"></i></a>
                              </div>
                          </div>
                          <div class="product-box__rating">
                              <select class="rating-box">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3" selected>3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                              </select>
                          </div>
                          <h3 class="product-box__title"><?php echo $product['Title']; ?></h3>
                          <div class="product-box__btn-box">
<!--                          <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                              <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                              <div class="ripple-container"></div>-->
<!--                          </button>-->
                          <a href="#" class="product-box__dtl-btn">View Full Details</a>
                          </div>
                      </div>
                  </div>
                <?php } ?>
                  
              </div>
            </div>
        </div>
    </div><!-- products.mostsold-products -->    



                <?php

              }
              }

    }
    ?>
    

    

</section>

<section id="brands-section">
    <div class="container-fluid">
      <div id="brands-slider" class="owl-carousel owl-theme">
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-01.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-02.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-03.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-04.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-05.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-06.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-07.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-08.jpg"></a></div>
        <div class="item"><a href="#"><img src="<?= base_url() . "assets/img/" ?>slide-logo-09.jpg"></a></div>
      </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="quick-product-modal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content quick-modal">
      <div class="loading-animation">
        <img src="<?= base_url() . "assets/img/" ?>loader.svg" width="50" alt="">
      </div>
      <div class="modl">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="row">
            <div class="col-sm-6">
              <div class="modal-img__large">
                <img src="<?= base_url() . "assets/img/" ?>bike.jpg">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="quick-modal__category">
                Appliances
              </div>
              <h3 class="product-box__title">Suzuki Bike 110</h3>
              <div class="product-box__rating">
                  <select class="rating-box">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3" selected>3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                  </select>
              </div>
              <div class="product-box__short-desc">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni dignissimos ducimus, aliquam numquam, excepturi.
              </div>
              <div class="product-box__btn-details">
                <a href="#" class="btn">see details</a>
                <a class="btn btn-info" title="Add To Cart">
                  <span class="btn-label"><i class="material-icons">add_shopping_cart</i></span>
                  Add To Cart<div class="ripple-container"></div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <i _ngcontent-c17="" class="baseline-add_shopping_cart icon-image-preview"></i>
          <div class="row">
            <div class="col-sm-12">
              <div class="product-thumbs clearfix">
                <a href="#" class="product-thumbs__thumbnail"><img src="<?= base_url() . "assets/img/" ?>bike.jpg"></a>
                <a href="#" class="product-thumbs__thumbnail"><img src="<?= base_url() . "assets/img/" ?>vegetable.jpg"></a>
                <a href="#" class="product-thumbs__thumbnail"><img src="<?= base_url() . "assets/img/" ?>fruit.jpg"></a>
                <a href="#" class="product-thumbs__thumbnail"><img src="<?= base_url() . "assets/img/" ?>burger.jpg"></a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>



<!-- JS Plugins - Asif ---->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


<script type="text/javascript">

// rating plugin init
   $(function() {
      $('.rating-box').barrating({
        theme: 'fontawesome-stars'
      });
   });

    $(document).ready(function(){
        // image slider
        $("#owl-image-slider").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            items: 1,
            autoplay: true
        });

				$(".products-carousel").owlCarousel({
					 items : 4, //10 items above 1000px browser width
					 itemsDesktop : [1000,5], //5 items between 1000px and 901px
					 itemsDesktopSmall : [900,3], // betweem 900px and 601px
					 itemsTablet: [600,2], //2 items between 600 and 0
					 loop:true,
					 nav: true,
					 dots: false
        });

				// brands slider
				$('#brands-slider').owlCarousel({
					loop: true,
					autoplay: true,
					items: 6,
					dots: false,
					margin:15
				});

        // trigger quick product view modal
        $(document).on('click', '.product-box__quick-view-btn', function(e){
            e.preventDefault();
            $('#quick-product-modal').modal('show');

            // hide modal main content first
            $('.modl').hide();

            // show loading animation during ajax request
            $('.loading-animation').show();
            setTimeout(function(){
              $('.modl').show();
              $('.loading-animation').hide();
            }, 2000);

        });


        // quick thumb load in main images
        $('.product-thumbs__thumbnail').on('click',function(e){
            e.preventDefault();
            var thumbSrc = $(this).find('img').attr('src');
            // load thumb into main image.
            $(".modal-img__large > img").attr('src', thumbSrc).fadeIn();
        });

    });


</script>
