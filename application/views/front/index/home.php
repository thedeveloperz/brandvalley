<div class="content">

<section id="top-image-slider">
    <div id="owl-image-slider" class="owl-carousel owl-theme image-slider">
        <?php if(!empty($SliderImages)){
           foreach ($SliderImages as $key => $SliderImage) {
            if(file_exists($SliderImage['SliderImageFile'])){
              echo '<div class="item"><img style="width:100%;height:369px;" src="'.base_url($SliderImage['SliderImageFile']).'" alt=""></div>';
            }
            
           }
        }else{ ?>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-01.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-02.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-03.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-04.jpg') ?>" alt=""></div>
        <div class="item"><img src="<?php echo base_url('assets/img/slide-img-05.jpg') ?>" alt=""></div>
      <?php } ?>
    </div>
</section>

<section id="banner">
  <div class="container-fluid align-center">
      <?php if(!empty($GifImages)){ 
                foreach ($GifImages as $key => $GifImage) {
                  if(file_exists($GifImage['SliderImageFile'])){
                  echo '<img class="gif-banner" style="width:100%;height:169px;" src="'.base_url($GifImage['GifImageFile']).'" alt="">';
                }
                }

              }else{ ?>
                <img class="gif-banner" src="<?php echo base_url('assets/img/gif-banner.gif') ?>" alt="">

              <?php } ?>
     
  </div>
</section>

<section id="location-section">
  <div class="container-fluid">
    <ul class="location-list clearfix">
      <li>You are here</li>
      <li><?php echo (($CountryData && isset($CountryData[0]->Title) ? $CountryData[0]->Title : ''));?></li>
      <li><?php echo (($StateData && isset($StateData[0]->Title) ? $StateData[0]->Title : ''));?></li>
      <li><?php echo (($CityData && isset($CityData[0]->Title) ? $CityData[0]->Title : ''));?></li>
    </ul>
  </div>
</section>

<section id="home-links">
<!--  <div class="container-fluid">-->
<!--    <div class="row">-->
<!--      --><?php //if($manufactures){
//                foreach ($manufactures as $key => $manu) {
//                           if($key != 0 && $key % 6 == 0){
//                            echo '</div>';
//                            echo '<div class="row">';
//                           }
//                            ?>
<!--                  <div class="col-sm-2"><a href="--><?php //echo base_url('Manufacturer/detail/'.$manu->UserID);?><!--" class="h-link">--><?php //echo $manu->FullName; ?><!--</a></div>-->
<!--                -->
<!--                --><?php //}
//              }
//       ?><!--         -->
<!--      -->
<!--      -->
<!--    </div>-->
<!--    -->
<!--  </div>-->
    <div class="container-fluid brands-links">
<!--        <div class="row">-->
        <?php foreach($ProfileTypes as $key => $value) { ?>
            <?php if($key % 5 == 0 && $key != 0){ ?>
<!--                </div>-->
<!--                <div class="row">-->
            <?php } ?>
                 <div class="col-5"><a class="brand-link" href="<?php echo strtolower(base_url("listings/all/$value->ProfileType")) ?>/<?php echo $value->UserProfileTypeID ?>"><?php echo $value->ProfileType ?> </a></div>
        <?php } ?>
<!--        </div>  -->
    </div>
</section>

<section id="products-section">
    <div class="products feature-products">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3>Latest Products</h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">
                <?php if(!empty($latest_products)){
                  foreach ($latest_products as $key => $lastest_product) { ?>
                  
                      <div class="item">
                        <div class="product-box">
                            <div class="product-box__img">

                                <?php if(!file_exists($lastest_product['ImageName'])) {
                                    $image = base_url("assets/backend/img/no_img.png");
                                }else {
                                    $image = base_url($lastest_product['ImageName']);
                                }
                                ?>
                                <img src="<?php echo $image ?>" alt="">

                                <div class="product-box__quick-view">
                                    <a href="#" class="product-box__quick-view-btn" data-id="<?php echo  $lastest_product['ProductID'];?>"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <h3 class="product-box__title"><?php echo $lastest_product['Title']; ?></h3>
                            <div class="product_price">Rs. <?php echo $lastest_product['Price']; ?></div>
                            <div class="views">Views(<?php echo get_product_view_count($lastest_product['ProductID']) ?>)</div>
                            <div class="product-box__rating">
                                <?php $avg_rating = getProductAvgRating($lastest_product['ProductID']) ?>
                                <select class="rating-box">
                                    <?php for($i=1; $i<=5; $i++) { ?>
                                        <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="product-box__btn-box">
<!--                            <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                                <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                                <div class="ripple-container"></div>-->
<!--                            </button>-->
                            <a href="<?php echo base_url('product/details/'.$lastest_product['ProductID']);?>" class="product-box__dtl-btn">View Full Details</a>
                            </div>
                        </div>
                    </div>
                  
                  <?php 
                  }
                }
                ?>
                  
              </div>
            </div>
        </div>
    </div><!-- products.feature-products -->
    <?php if($top_categories){
              foreach ($top_categories as $key => $cat_products) { 
                if(!empty($cat_products['products'])){
                ?>

    <div class="products mostsold-products category" data-category_id="<?php echo $cat_products['CategoryID'] ?>">
      <div class="container-fluid">
            <div class="carousel-container">
              <div class="products-carousel__title">
                <h3><?php echo $cat_products['Title'];?></h3>
              </div>
              <div class="products-carousel owl-carousel owl-theme">

                <?php foreach ($cat_products['products'] as $key => $product) { ?>
                 
               
                  <div class="item">
                      <div class="product-box">
                          <div class="product-box__img">
                              <?php if(!file_exists($product['ImageName'])) {
                                  $image = base_url("assets/backend/img/no_img.png");
                              }else {
                                  $image = base_url($product['ImageName']);
                              }
                              ?>
                              <img src="<?php echo $image ?>" alt="">
                              <div class="product-box__quick-view">
                                  <a href="javascript:void(0);" class="product-box__quick-view-btn" data-id = "<?php echo  $product['ProductID'];?>"><i class="fa fa-search" aria-hidden="true"></i></a>
                              </div>
                          </div>
                          <h3 class="product-box__title"><?php echo $product['Title']; ?></h3>
                          <div class="product_price">Rs. <?php echo $product['Price']; ?></div>
                          <div class="views">Views(<?php echo get_product_view_count($product['ProductID']) ?>)</div>
                          <div class="product-box__rating">
                              <?php $avg_rating = getProductAvgRating($product['ProductID']) ?>
                              <select class="rating-box">
                                  <?php for($i=1; $i<=5; $i++) { ?>
                                      <option value="<?php echo $i; ?>" <?php echo ($i == $avg_rating) ? "selected": "" ?> > <?php echo $i?> </option>
                                  <?php } ?>
                              </select>
                          </div>
                          <div class="product-box__btn-box">
<!--                          <button class="btn btn-just-icon btn-simple btn-reddit">-->
<!--                              <i class="fa fa-heart-o" aria-hidden="true"></i>-->
<!--                              <div class="ripple-container"></div>-->
<!--                          </button>-->
                          <a href="<?php echo base_url('product/details/'.$product['ProductID']);?>" class="product-box__dtl-btn">View Full Details</a>
                          </div>
                      </div>
                  </div>
                <?php } ?>
                  
              </div>
            </div>
        </div>
    </div><!-- products.mostsold-products -->    



                <?php

              }
            }

    }
    ?>
    

    

</section>
<div class="LoadMore" id="LoadMore"><img src="<?php echo base_url('assets/images/loader.svg') ?>" /> </div>
<a href="javascript:void(0)" class="btn load_more" style="width: 150px;display: block; margin: 10px auto; background-color:#0066c0">Load More</a>
<?php if($business_pages) { ?>
<section id="brands-section">
    <div class="container-fluid">
        <div class="products-carousel__title">
            <h3>Branded Vendors</h3>
        </div>
      <div id="brands-slider" class="owl-carousel owl-theme">
          <?php foreach($business_pages as $page) { ?>
              <div class="item"><a href="<?php echo base_url('business/brand/'); ?>/<?php echo $page->BusinessPageID ?>"> <img src="<?php echo base_url($page->BusinessLogo); ?>"></a></div>
          <?php } ?>
      </div>
    </div>
</section>
<?php } ?>

</div><!-- main-panel -->

<!-- Modal -->
<div class="modal fade" id="quick-product-modal" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content quick-modal">
      <div class="loading-animation">
        <img src="<?= base_url() . "assets/img/" ?>loader.svg" width="50" alt="">
      </div>
      <div class="modl">
        
      </div>

    </div>

  </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script>
    $(document).ready(function(){

        // var waypoint = new Waypoint({
        //     element: document.getElementById('LoadMore'),
        //     handler: getNextProducts,
        //         offset: '100%'
        // });

        //function getNextProducts(direction) {
        //    if(direction == 'down') {
        //        // send displayed categories in comma separated.
        //        let cat_ids = '';
        //        $.each($('div.category'), function (index, item) {
        //            cat_ids += $(item).data('category_id') + ',';
        //        });
        //        if(cat_ids != ''){
        //          $("#LoadMore").css("visibility", "visible");
        //            $.ajax({
        //                type: 'POST',
        //                data: {cat_ids:cat_ids},
        //                url: "<?php //echo base_url('product/loadmoreproducts')?>//",
        //                success: function (response) {
        //                    $("#LoadMore").css("visibility", "hidden");
        //                    $('#products-section').append(response);
        //                    Waypoint.refreshAll();
        //                }
        //            });
        //        }
        //    }
        //}

        $('.load_more').on('click', function () {
            let cat_ids = '';
            $.each($('div.category'), function (index, item) {
                cat_ids += $(item).data('category_id') + ',';
            });
            if(cat_ids != ''){
                $("#LoadMore").css("visibility", "visible");
                $.ajax({
                    type: 'POST',
                    data: {cat_ids:cat_ids},
                    url: "<?php echo base_url('product/loadmoreproducts')?>",
                    success: function (response) {
                        $("#LoadMore").css("visibility", "hidden");
                        $('#products-section').append(response);
                    }
                });
            }
        });

});
</script>

<script type="text/javascript">

// rating plugin init
   $(function() {
      $('.rating-box').barrating({
        theme: 'fontawesome-stars',
          readonly: true
      });
   });

    $(document).ready(function(){
        // image slider
        $("#owl-image-slider").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            items: 1,
            autoplay: true,
            smartSpeed: 1000
        });

        $(".products-carousel").owlCarousel({
           items : 5, //10 items above 1000px browser width
           itemsDesktop : [1000,5], //5 items between 1000px and 901px
           itemsDesktopSmall : [900,3], // betweem 900px and 601px
           itemsTablet: [600,2], //2 items between 600 and 0
           loop:false,
           nav: true,
           dots: false,
           responsive : {
                320 : {
                    items: 1
                },
                480 : {
                    items : 2,
                },
                768 : {
                    items : 2,
                },
                1200 : {
                    items : 5,
                }
            }
        });

        // brands slider
        $('#brands-slider').owlCarousel({
          loop: false,
          autoplay: false,
          items: 6,
          dots: false,
          margin: 10,
          autoplayHoverPause: true,
          autoplayTimeout: 3000,
          smartSpeed: 1000,
          responsive : {
                320 : {
                    items: 2
                },
                480 : {
                    items : 2,
                },
                768 : {
                    items : 3,
                },
                1200 : {
                    items : 6,
                }
            }
        });

        // trigger quick product view modal
        $(document).on('click', '.product-box__quick-view-btn', function(e){

            e.preventDefault();
            $('.loading-animation').show();
            $('#quick-product-modal').modal('show');
            $.ajax({
            type: "POST",
            url: base_url+'product/details/'+$(this).attr('data-id')+'/true',
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                $('.modl').hide();
                $('.modl').html(result.html);

                setTimeout(function(){
                  $('.modl').show();
                  $('.loading-animation').hide();
                }, 2000);
            }
        }); 



            

            // hide modal main content first
            //$('.modl').hide();

            // show loading animation during ajax request
            //$('.loading-animation').show();
            

        });

    });


</script>
