<!DOCTYPE html>
<html>
   <head>
      <title></title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body>
      <table width="540" style="font-family: arial; border: solid 1px #ccc;">
         <thead>
            <tr>
               <td colspan="4" style="background:#8c8c8c; height: 10px;"></td>
            </tr>
            <tr>
               <td style="text-align: left;">
                  <img  src="<?php echo base_url('assets/images');?>/logo.png" width="130">
               </td>
               <td style="text-align: right;">
                  <i class="fa fa-phone" style="color: #e2197c;"></i> <a style="color:#000; text-decoration: none;" href="tel:+ 92 301 427 8992">+ 92 301 427 8992</a>
                  <br>
                  <i class="fa fa-envelope" style="color: #e2197c;"></i> <a  style="color:#000; text-decoration: none;" href="mailto:info@brandsvalley.net">info@brandsvalley.net</a>
               </td>
            </tr>
            <tr>
               <td colspan="4" style="text-align: center;     text-align: center;
                  background: #e02881;
                  color: #fff;
                  font-weight: bold;
                  font-size: 18px">Order Confirmation</td>
            </tr>
         </thead>
         <tbody>

            <!--order confirmed-->            

            <tr><td height="15" colspan="4"></td></tr>
            <tr><td height="60" colspan="4" style="text-align: center;"> <img src="<?php echo base_url('assets/images');?>/check1.png" width="50"/> </td></tr>
            <tr style="text-align: center;"> <td colspan="4"><b>Your Order has been Sucessfully Placed.</b></td></tr>
            <tr style="text-align: center; height: 50;"> <td colspan="4">Your Order No: <b style="color: #39b54a;"><?php echo $order_items[0]['OrderTrackID']; ?></b></td></tr>

            <!-- End order confirmed-->  


            <!--Shipping-->    

            <tr><td height="25" colspan="4"></td></tr>
            <tr><td height="25" colspan="4" style="background: #a264ad; color: #fff;">&nbsp; <b>Shipping</b></td></tr>
            <tr><td height="15" colspan="4"></td></tr>
            <tr style="background: #fadbff;"><td>&nbsp; Name:</td> <td colspan="2"><?php echo ucfirst($order_items[0]['Name']); ?></td></tr>
            <tr><td>&nbsp; Phone:</td> <td colspan="2"><?php echo $order_items[0]['Telephone']; ?></td></tr>
            <tr style="background: #fadbff;"><td>&nbsp; Email:</td> <td colspan="2"><?php echo $order_items[0]['Email']; ?></td></tr>
            <tr><td>&nbsp; Order No:</td> <td colspan="2"><?php echo $order_items[0]['OrderTrackID']; ?></td></tr>
            
            <tr style="background: #fadbff;"><td>&nbsp; Address:</td> <td colspan="2"><?php echo $order_items[0]['Address']; ?></td></tr>
            <tr><td>&nbsp; Country:</td> <td colspan="2"><?php echo $order_items[0]['Country'];?></td></tr>

            <tr style="background: #fadbff;"><td>&nbsp; State:</td> <td colspan="2"><?php echo $order_items[0]['State']; ?></td></tr>
            <tr><td>&nbsp; City:</td> <td colspan="2"><?php echo $order_items[0]['City']; ?></td></tr>
            

            <!--End Shipping-->


            <!--Order Summary-->      

            <tr><td height="15" colspan="4"></td></tr>
            <tr><td height="25" colspan="4" style="background: #a264ad; color: #fff;">&nbsp; <b>Order Summary</b></td></tr>
            <tr><td height="15" colspan="4"></td></tr>

            <tr style="font-size: 14px; font-weight: bold;"><td>&nbsp;Qty</td><td>Product Name</td> <td>Price</td><td>Total&nbsp;</td></tr>
             <?php 
                $total = 0;
                
                foreach($order_items as $key => $value){
                   $total= $total + ($value['Price']  * $value['Quantity']);?>
            <tr style="font-size: 14px;"><td>&nbsp;<?php echo $value['Quantity']; ?></td> <td><?php echo $value['Title'];?></td><td><?php echo $value['Price']; ?></td> <td><?php echo ($value['Price']  * $value['Quantity']); ?></td></tr>

        <?php } ?>



           

            <tr><td height="15" colspan="4"></td></tr>

            <tr style="font-size: 14px;"><td></td><td></td><td style="text-align: right;">Total:</td> <td><?php echo $total; ?></td></tr>            
            
            
            <tr style="font-size: 14px;"><td height="15" colspan="4"></td></tr>

            <tr><td colspan="4" style="height:0.3px; background: #a264ad;"></td></tr>

            <tr style="font-size: 14px;"><td height="15" colspan="4"></td></tr>

            <tr><td height="20" colspan="4" style="text-align: center;"> <img src="<?php echo base_url('assets/images');?>/thank.jpg" width="150"/> </td></tr>

            <tr><td height="25" colspan="4" style="text-align: center;"><b style="color: #a264ad;">Thank you for your Order!</b></td></tr>

            <tr style="font-size: 14px;"><td height="15" colspan="4"></td></tr>







            <!--End Order Summary-->  



         </tbody>
         <tfoot>
            <tr style="background:#ddd;">
               <td colspan="4" style="text-align:center;">
                  <img src="<?php echo base_url('assets/images');?>/logo.png" width="120">
               </td>
            </tr>
            <tr>
               <td colspan="4" height="30" style="text-align: center; font-size: 12px;">
                  info@brandsvalley.net | + 92 301 427 8992 | 65 Main Road, PO BOX 28, PAK Kaiapoi 7442, Chirstal NZ
               </td>
            </tr>
            <tr bgcolor="#585858">
               <td colspan="4" height="30" style="text-align: center; color: #fff; font-size: 14px;">© <?php echo date('Y');?> All Copy Rights</td>
            </tr>
         </tfoot>
      </table>
   </body>
</html>
