<?php

/*
 * English language
 */

//General 

$lang['some_thing_went_wrong'] = 'There is something went wrong';


$lang['save_successfully']     = 'Save Successfully';
$lang['update_successfully']   = 'Updated Successfully';
$lang['deleted_successfully']  = 'Deleted Successfully';
$lang['you_should_update_data_seperately_for_each_language']  = 'You sould need to update data separately for each language';
$lang['yes']  = 'Yes';
$lang['no']  = 'No';
$lang['actions']  = 'Action';
$lang['submit']  = 'Submit';
$lang['back']  = 'Back';
$lang['add']  = 'Add';
$lang['edit']  = 'Edit';
$lang['are_you_sure']  = 'Are you sure you want to delete this?';
$lang['view']     = 'View';
$lang['add']     = 'Add';
$lang['edit']     = 'Edit';
$lang['delete']     = 'Delete';
$lang['is_active']                 = 'Is Active';
$lang['create_table']                 = 'Create Table';
$lang['create_model']                 = 'Create Model';
$lang['create_view']                 = 'Create View';
$lang['create_controller']                 = 'Create Controller';
$lang['logout']                 = 'Logout';

$lang['please_add_role_first'] = 'Please add role first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';
$lang['AreYouSureDeleteFile'] = 'Are you sure you want to delete this file?';

$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module']        = 'Parent Module';
$lang['slug']                 = 'Slug';
$lang['icon_class']            = 'Icon Class';

$lang['title']                 = 'Title';
$lang['add_module']            = 'Add Module';
$lang['modules']                = 'Modules';
$lang['module']                = 'Module';
$lang['module_rights']                = 'Module Rights';

$lang['please_add_module_first']                = 'Please add module first';
$lang['rights']            = 'Rights';


// Roles Section

$lang['select_role']            = 'Select Role';
$lang['add_role']               = 'Add Role';
$lang['roles']                  = 'Roles';
$lang['role']                   = 'Role';
$lang['please_add_role_first']                = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';


// activity section 

$lang['add_activity']            = 'Add Activity';
$lang['activities']              = 'Activities';
$lang['activity']                = 'Activity';
$lang['choose_activity']                = 'Choose Activity';

// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name']                    = 'Name';
$lang['user']                    = 'User';
$lang['users']                    = 'Users';
$lang['add_user']                    = 'Add User';
$lang['select_user']                    = 'Select User';

$lang['email']                    = 'Email';
$lang['password']                 = 'Password';
$lang['min_length']               = '(Min char 8)';
$lang['confirm_password']         = 'Confirm Password';
$lang['current_password']                 = 'Current Password';
$lang['new_password']                 = 'New Password';
$lang['change_password']                 = 'Change Password';
$lang['invalid_current_password']				=  'Invalid current password.';
$lang['password_confirm_password_not_match']		=	'Password and confirm password doesn\'t match.';





$lang['add_city']            = 'Add City';
$lang['citys']            = 'Citys';
$lang['city']            = 'City';
$lang['edit_city']		 = 'Edit City';
$lang['add_district']            = 'Add District';
$lang['districts']            = 'Districts';
$lang['district']            = 'District';
$lang['please_add_distric_first'] = 'Please add distric first';
$lang['choose_district'] = 'Choose District';
$lang['add_patient']            = 'Add Patient';
$lang['patients']            = 'Patients';
$lang['patient']            = 'Patient';
$lang['add_center']            = 'Add center';
$lang['centers']            = 'Centers';
$lang['center']            = 'Center';
$lang['add_screening_camp']            = 'Add Screening camp';
$lang['screening_camps']            = 'Screening camps';
$lang['screening_camp']            = 'Screening camp';
$lang['add_center_type']            = 'Add Center type';
$lang['center_types']            = 'Center Types';
$lang['center_type']            = 'Center Type';
$lang['add_email_template'] = 'Add Email Template';
$lang['email_templates'] = 'Email Templates';
$lang['email_template'] = 'Email Template';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'Description';
$lang['Image'] = 'Image';
$lang['add_language']            = 'Add Language';
$lang['languages']            = 'Languages';
$lang['language']            = 'Language';
$lang['short_code']            = 'Short Code';

$lang['is_default']            = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_test']            = 'Add Test';
$lang['tests']            = 'Tests';
$lang['test']            = 'Test';
$lang['add_country']            = 'Add Country';
$lang['countrys']            = 'Countrys';
$lang['country']            = 'Country';
$lang['country_code']            = 'Country Code';
$lang['choose_country']			= 'Choose Country';
$lang['edit_country']		= 'Edit Country';
$lang['please_add_country_first'] = 'Please add country first';
$lang['add_state']            = 'Add State';
$lang['states']            = 'States';
$lang['state']            = 'State';
$lang['choose_state'] 	  ='Choose State';
$lang['edit_state']		  = 'Edit State';
$lang['please_add_state_first'] = 'Please add state first';
$lang['add_category']            = 'Add Category';
$lang['categorys']            = 'Categories';
$lang['category']            = 'Category';
$lang['add_child_category']            = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Catagories Of';


$lang['add_dollerSystem']            = 'Add Doller System User Type';
$lang['dollerSystems']            = 'Doller Systems  User Types';
$lang['dollerSystem']            = 'Doller System';
$lang['price']            = 'Price';
$lang['commission']            = 'Commission ( In percentage )';

$lang['add_product']            = 'Add Product';
$lang['products']            = 'Products';
$lang['product']            = 'Product';

$lang['add_package']            = 'Add Package';
$lang['packages']            = 'Packages';
$lang['package']            = 'Package';
$lang['duration']           = 'Duration (Days)';
$lang['charges']           = 'Charges';
$lang['discount']          = 'Discount';
$lang['company_name']		= 'Company Name';
$lang['company_url']		= 'Company URL';
$lang['email']				= 'Email';
$lang['sub_category']		= 'Sub Category';
$lang['company_name']		= 'Company Name';
$lang['contact']			= 'Contact';
$lang['whats_app']			= 'What\'s App';
$lang['position_number']			= 'Position Number';
$lang['short_description']			= 'Short Description';
$lang['description']			= 'Description';
$lang['slider_image']		= 'Slider Image';
$lang['global']		= 'Global';
$lang['choose_category'] 		= 'Select Category';
$lang['choose_sub_category']		= 'Select Sub Category';
$lang['choose_city']		= 'Select City';
$lang['video_url']		= 'Video URL';
$lang['youtube_video']		= 'Youtube Video';
$lang['i_want_to_publish'] 	= 'I want to publish ad as';
$lang['duration_package'] 	= 'Duration Package';
$lang['amount_calculations'] 	= 'Amount Calculations';
$lang['grand_total'] 	= 'Grand Total';
$lang['discount'] 	= 'Discount';
$lang['net_payables'] 	= 'Net Payables';
$lang['pay_via'] 	= 'Pay Via';
$lang['id_card_number'] 	= 'ID Card Number';
$lang['select'] 	= 'Select';
$lang['choose_payment_method'] = 'Choose Payment Method';
$lang['add_payment']            = 'Add Payment';
$lang['payments']            = 'Payments';
$lang['payment']            = 'Payment';
$lang['approved_successfully']            = 'Approved Successfully';
$lang['add_order']            = 'Add Order';
$lang['orders']            = 'Orders';
$lang['order']            = 'Order';
$lang['add_shop']            = 'Add Shop';
$lang['shops']            = 'Shops';
$lang['shop']            = 'Shop';
$lang['add_manufacture']            = 'Add Manufacture';
$lang['manufactures']            = 'Manufactures';
$lang['manufacture']            = 'Manufacture';
$lang['add_businessproduct']            = 'Add Businessproduct';
$lang['businessproducts']            = 'Businessproducts';
$lang['businessproduct']            = 'Businessproduct';