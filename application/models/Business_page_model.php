<?php

class Business_page_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("business_pages");
    }

    public function getAllBusinessProducts($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'DESC',$sort_field = 'SortOrder',$like = false,$like_keyword = false)
    {

        $this->db->select('businessproducts.*,  businessproducts_text.* ');
        $this->db->join('businessproducts_text','businessproducts.BusinessproductID = businessproducts_text.BusinessproductID' );
        $this->db->join('system_languages','system_languages.SystemLanguageID = businessproducts_text.SystemLanguageID' );

        if($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
            $this->db->where('system_languages.IsDefault','1');
        }
        if($where)
        {
            $this->db->where($where);
        }
        $this->db->order_by('businessproducts.'.$sort_field,$sort);

        if($like){
            $this->db->like($like,$like_keyword,'both');
        }
        $result = $this->db->get('businessproducts');
        //echo $this->db->last_query();exit();
        if($as_array)
        {
            $data =  $result->result_array();
        }else{
            $data = $result->result();
        }

        return $data;

    }

    public function getBusinessPages($as_array=false, $where = false, $sort = 'ASC', $limit = false, $start = 0)
    {

        $this->db->select('business_pages.*');

        if($where)
        {
            $this->db->where($where);
        }

        if($limit){
            $this->db->limit($limit,$start);
        }

        $result = $this->db->get($this->table);

        //echo $this->db->last_query();exit();

        if($as_array)
        {

            $data =  $result->result_array();
        }else{
            $data = $result->result();
        }



        return $data;

    }
}