<?php
	Class Payment_model extends Base_Model
	{
	    public function __construct()
	    {
	        parent::__construct("payments");


	    }



	    public function getRegistrationPayments($where = false,$system_language_code = 'EN')
	    {
	    	$this->db->select('users.Type,users_text.FullName,payments.*');
	    	$this->db->from('payments');
	    	$this->db->join('users','users.UserID = payments.CommonID');
	    	$this->db->join('users_text','users.UserID = users_text.UserID');
	        
	        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );


	        if($where){
	        	$this->db->where($where);
	        }

	    	if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	        }else
	        {
	                $this->db->where('system_languages.IsDefault','1');
	        }
	        return $this->db->get()->result();
	            
		}

		public function getPublishProductPayments($where = false,$system_language_code = 'EN')
	    {
	    	$this->db->select('users_text.FullName,products.DollerSystemID,products_text.Title,packages_text.Title as PackageTitle,payments.*');
	    	$this->db->from('payments');

	    	$this->db->join('products','products.ProductID = payments.CommonID');
	    	$this->db->join('products_text','products.ProductID = products_text.ProductID');


	    	$this->db->join('packages','products.PackageID = packages.PackageID','left');
	    	$this->db->join('packages_text','packages.PackageID = packages_text.PackageID','left');

	    	$this->db->join('users','users.UserID = products.CreatedBy');
	    	$this->db->join('users_text','users.UserID = users_text.UserID');
	        
	        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
	        if($where){
	        	$this->db->where($where);
	        }

	    	if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	        }else
	        {
	                $this->db->where('system_languages.IsDefault','1');
	        }
	        return $this->db->get()->result();
	            
		}


        public function totalEarning($where = false,$system_language_code = 'EN')
        {
            $this->db->select('SUM(payments.Amount) as Total');
            $this->db->from('payments');

            $this->db->join('products','products.ProductID = payments.CommonID');
            $this->db->join('products_text','products.ProductID = products_text.ProductID');


            $this->db->join('packages','products.PackageID = packages.PackageID','left');
            $this->db->join('packages_text','packages.PackageID = packages_text.PackageID','left');

            $this->db->join('users','users.UserID = products.CreatedBy');
            $this->db->join('users_text','users.UserID = users_text.UserID');

            $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
            if($where){
                $this->db->where($where);
            }

            if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                $this->db->where('system_languages.IsDefault','1');
            }
            return $this->db->get()->result();

        }




    }

