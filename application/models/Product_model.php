<?php
	Class Product_model extends Base_Model
	{
	    public function __construct()
	    {
	        parent::__construct("products");

	    }

	    public function getProductData($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$like = false,$like_keyword = false)
	    {

	            $this->db->select('products.*,  products_text.*, categories_text.Title as Category, sub_cat_text.Title as SubCategory,site_images.ImageName');
	            $this->db->join('products_text','products.ProductID = products_text.ProductID' );
	            $this->db->join('categories_text','products.CategoryID = categories_text.CategoryID', 'LEFT' );
	            $this->db->join('categories_text as sub_cat_text','products.SubCategoryID = sub_cat_text.CategoryID', 'LEFT' );
	            $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
	            
	            
	             $this->db->join('site_images','(site_images.FileID = products.ProductID AND site_images.ImageType = "ProductImage" )', 'LEFT' );
	            
	            
	            if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }
	            if($where)
	            {
	                    $this->db->where($where);
	            }
	            $this->db->where('products.Hide','0');
                $this->db->group_by('products.ProductID');
                $this->db->order_by('products.'.$sort_field,$sort);

	            if($like){
	            	$this->db->like($like,$like_keyword,'both');
	            }
	            $result = $this->db->get('products');
	            //echo $this->db->last_query();exit();
	            if($as_array)
	            {
	                   
	                $data =  $result->result_array();
	            }else{
	                $data = $result->result();
	            }

	           

	            return $data;
	            
	    }
            
            
            public function getSliderImages()
	    {

	            $this->db->select('site_images.ImageName');
	            $this->db->from('products');
                    $this->db->join('site_images', 'site_images.FileID = products.ProductID');
	            $this->db->where('products.Hide','0');
                    $this->db->where('site_images.ImageType','ProductImage');
                    $this->db->group_by('site_images.FileID');
	            $this->db->limit(5);
	            $result = $this->db->get();
	            return $result->result_array();
	            
	    }


	    public function getProducts($system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'products.SortOrder',$limit = false,$start = 0)
	    {

	            $this->db->select('products.*,  products_text.*, categories_text.Title as Category, sub_cat_text.Title as SubCategory,site_images.ImageName,packages.Duration');
	            $this->db->join('products_text','products.ProductID = products_text.ProductID' );
	            $this->db->join('categories_text','products.CategoryID = categories_text.CategoryID', 'LEFT' );
	            $this->db->join('categories_text as sub_cat_text','products.SubCategoryID = sub_cat_text.CategoryID', 'LEFT' );

	            $this->db->join('packages','products.PackageID = packages.PackageID','left');
	    		$this->db->join('packages_text','packages.PackageID = packages_text.PackageID','left');


	            $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
	            
	            $this->db->join('product_locations','product_locations.ProductID = products.ProductID', 'LEFT' );

	            $this->db->join('site_images','(site_images.FileID = products.ProductID AND site_images.ImageType = "ProductImage" )', 'LEFT' );
	            
	            
	            
	            if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }
	            if($where)
	            {
	                    $this->db->where($where);
	            }


	            $this->db->where('products.Hide','0');
	            $this->db->group_by('products.ProductID');
	            $this->db->order_by($sort_field,$sort);
	            if($limit){
	            	$this->db->limit($limit,$start);
	            }
	            return $this->db->get('products')->result_array();
	            
	            
	            
	    }


	    public function getSingleProducts($product_id ,$system_language_code = 'EN')
	    {

	            $this->db->select('products.*,  products_text.Title, categories_text.Title as Category, sub_cat_text.Title as SubCategory,users.AvailableBalance,packages.Duration');
	            $this->db->join('products_text','products.ProductID = products_text.ProductID' );
	            $this->db->join('users','users.UserID = products.CreatedBy');
	            $this->db->join('categories_text','products.CategoryID = categories_text.CategoryID', 'LEFT' );


	            $this->db->join('packages','products.PackageID = packages.PackageID','left');
	    		$this->db->join('packages_text','packages.PackageID = packages_text.PackageID','left');
	            
	            $this->db->join('categories_text as sub_cat_text','products.SubCategoryID = sub_cat_text.CategoryID', 'LEFT' );
	            $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
	            
	           
	            
	            
	            
	            if($system_language_code) {
	                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }
	            


	            $this->db->where('products.ProductID',$product_id);
	            
	            return $this->db->get('products')->row_array();
	            
	            
	            
	    }



	    public function getSliderAndGif($where= false,$order_by = false,$limit = false,$start = 0){

            	$this->db->select('products.ProductID,products.SliderImageFile,,products.GifImageFile');
            	$this->db->from('products');

            	$this->db->join('product_locations','product_locations.ProductID = products.ProductID');
            	



	            if($where){
	            	$this->db->where($where);
	            }
	            if($order_by){
	            	$this->db->order_by($order_by);
	            }

                $this->db->group_by('products.ProductID');
	           
	            if($limit){
	            	$this->db->limit($limit,$start);
	            }

	            return $this->db->get()->result_array();
            }
            
	}