<?php

class Order_model extends Base_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct('orders');
    }

   
    
    
   
    
    

    public function getSoldProductQuantity($ProductID){
        $this->db->select('*');
        $this->db->from('order_items');
        $this->db->where('product_id',$ProductID);
        $soldPieces = 0;
        foreach($this->db->get()->result() as $items)
        {
            if($items->TotalPieces > 0)
            {
                $soldPieces += $items->TotalPieces*$items->quantity;
            }
            else
            {
                $soldPieces += $items->quantity;
            }
        }

        return $soldPieces;
    }
    
    
    public function getUserCartDetail($user_id=''){
            
            $query = "SELECT temp_orders.*,products.*,products_text.* from temp_orders LEFT JOIN products ON temp_orders.ProductID = products.ProductID LEFT JOIN products_text ON products_text.ProductID = products.ProductID WHERE temp_orders.UserID=$user_id";
            $result = $this->db->query($query);
            return $result->result_array();
    }
    
   
    
    public function getAllUserOrder($user_id){
        
        $this->db->select('*');
        $this->db->from('orders');
        if($user_id != 0){
            $this->db->where('UserID',$user_id);
        }
        
        
        return $this->db->get()->result_array();
        
        
    }


    public function getOrders($where = false,$system_language_code = false){
        
        $this->db->select('orders.*');
        $this->db->from('orders');
        $this->db->join('order_items','order_items.OrderID = orders.OrderID');
        $this->db->join('products','products.ProductID = order_items.ProductID');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }

        if($where){
            $this->db->where($where);
        }

         $this->db->group_by('orders.OrderID');
        
        
        return $this->db->get()->result_array();
        
        
    }
    
    public function getOrdersDetail($order_id,$system_language_code = 'EN',$where = false){
        
        $this->db->select('orders.*,orders.CreatedAt as OrderCreateDate,order_items.*,products.*,products_text.*,upt.FullName as ProductPublisherFullName,up.Email as ProductPulisherEmail,uc.Email as CustomerEmail,uct.FullName as CustomerFullName,uc.Phone as CustomerPhone,countries_text.Title as Country,states_text.Title as State,cities_text.Title as City');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.OrderID = orders.OrderID');
        $this->db->join('products','products.ProductID = order_items.ProductID');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );

        $this->db->join('users up','up.UserID = products.CreatedBy','LEFT');
        $this->db->join('users_text upt','upt.UserID = up.UserID','LEFT');

        $this->db->join('users uc','uc.UserID = orders.UserID','LEFT');
        $this->db->join('users_text uct','uct.UserID = uc.UserID','LEFT');


        $this->db->join('countries_text','countries_text.CountryID = orders.CountryID');
        $this->db->join('states_text','states_text.StateID = orders.StateID');
        $this->db->join('cities_text','cities_text.CityID = orders.CityID');
        
        $this->db->where('orders.OrderID',$order_id);
        if($where){
            $this->db->where($where);
        }
        if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        
        
        $this->db->group_by('order_items.ProductID');
        
        return $this->db->get()->result_array();
        
        
    }

    public function getOrdersDetailWithCustomerDetail($order_id,$system_language_code = false){
        
        $this->db->select('orders.*,order_items.*,products.*,products_text.*,users_text.FullName,users.Email as UserEmail,countries_text.Title as Country,states_text.Title as State,cities_text.Title as City');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.OrderID = orders.OrderID');
        
        $this->db->join('users','users.UserID = orders.UserID');
        $this->db->join('users_text','users_text.UserID = users.UserID');

        $this->db->join('countries_text','countries_text.CountryID = orders.CountryID');
        $this->db->join('states_text','states_text.StateID = orders.StateID');
        $this->db->join('cities_text','cities_text.CityID = orders.CityID');

        $this->db->join('products','products.ProductID = order_items.ProductID');
        $this->db->join('products_text','products_text.ProductID = products.ProductID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        
        $this->db->where('orders.OrderID',$order_id);
        if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        
        
        $this->db->group_by('order_items.ProductID');
        
        return $this->db->get()->result_array();
        
        
    }
    
    
    
}
