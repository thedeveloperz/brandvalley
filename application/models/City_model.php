<?php
    Class City_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("cities");

        }




        public function getDataByCity($city_name,$system_language_code = 'EN'){


        	$this->db->select('cities.CityID,cities_text.Title as CityTitle,states.StateID,states_text.Title as StateTitle,countries.CountryID,countries_text.Title as CountryTitle');

        	$this->db->from('cities');

        	$this->db->join('cities_text','cities_text.CityID = cities.CityID');
        	$this->db->join('system_languages','system_languages.SystemLanguageID = cities_text.SystemLanguageID');

        	$this->db->join('states','states.StateID = cities.StateID');
        	$this->db->join('states_text','states_text.StateID = states.StateID');

        	$this->db->join('countries','countries.CountryID = states.CountryID');
        	$this->db->join('countries_text','countries_text.CountryID = countries.CountryID');

        	if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }

            $this->db->like('LOWER(cities_text.Title)',strtolower($city_name),'both');


            return  $this->db->get()->row_array();


        }
        
        
    }
    
    
    