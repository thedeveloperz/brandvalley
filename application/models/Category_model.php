<?php
        Class Category_model extends Base_Model
        {
            public function __construct()
            {
                parent::__construct("categories");

            }




            public function getRandomCategoryWithProducts($system_language_code = false,$where= false,$order_by = false,$limit = false,$start = 0,$group_by = false) {

            	$this->db->select('categories.CategoryID,categories_text.Title');
            	$this->db->from('categories');

            	$this->db->join('categories_text','categories_text.CategoryID = categories.CategoryID');
            	$this->db->join('system_languages','system_languages.SystemLanguageID = categories_text.SystemLanguageID');

            	$this->db->join('products','products.CategoryID = categories.CategoryID');
            	$this->db->join('products_text','products_text.ProductID = products.ProductID');



            	if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
	            }else
	            {
	                    $this->db->where('system_languages.IsDefault','1');
	            }


	            if($where){
	            	$this->db->where($where);
	            }
	            if($order_by){
	            	$this->db->order_by($order_by);
	            }

                if($group_by){
                    $this->db->group_by($group_by);
                }
	           
	            if($limit){
	            	$this->db->limit($limit,$start);
	            }

	            return $this->db->get()->result_array();
            }


            public function getLoadMoreCategory($system_language_code = false,$where= false,$order_by = false,$limit = false,$start = 0,$group_by = false){

                $this->db->select('categories.CategoryID,categories_text.Title');
                $this->db->from('categories');

                $this->db->join('categories_text','categories_text.CategoryID = categories.CategoryID');
                $this->db->join('system_languages','system_languages.SystemLanguageID = categories_text.SystemLanguageID');

                $this->db->join('products','products.CategoryID = categories.CategoryID');
                $this->db->join('products_text','products_text.ProductID = products.ProductID');



                if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
                }else
                {
                    $this->db->where('system_languages.IsDefault','1');
                }


                if($where){
                    $this->db->where($where);
                }
                if($order_by){
                    $this->db->order_by($order_by);
                }

                if($group_by){
                    $this->db->group_by($group_by);
                }

                if($limit){
                    $this->db->limit($limit,$start);
                }

                return $this->db->get()->result_array();







            }


        }