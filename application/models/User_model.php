<?php
Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }

    
    /*
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        $this->db->where('users.Email',$data['Email']);
        $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    */
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$data['Email']);
       //$this->db->or_where('users.UName',$data['Email']);
       $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    
    
    public function getUserDataByEmail($email,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$email);
      
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
    }

    public function getUserDataBySocialID($socialID,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        
        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
       // $this->db->where($where);
       $this->db->where('users.Email',$socialID);
      
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
    }

    public function getUserDataById($user_id,$system_language_code = false){

        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.UserID',$user_id);

        if($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
            $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
    }
}
?>