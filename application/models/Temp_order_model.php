<?php

class Temp_order_model extends Base_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct('temp_orders');
    }

   
   public function alreadyAddToCart($user_id='',$product_id = ''){
            
            $query = "SELECT * from temp_orders WHERE UserID=$user_id AND ProductID = $product_id";
            $result = $this->db->query($query);
            return $result->row_array();
    }

    public function getTotalProduct($user_id){
        $this->db->select('COUNT(ProductID) as products_count, SUM(quantity) as total');
        $this->db->from('temp_orders');
        $this->db->where('UserID',$user_id);
        return $this->db->get()->result();
    }
    
    
    
}
