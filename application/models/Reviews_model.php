<?php
Class Reviews_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("reviews");
    }



    public function getReviews($product_id ,$system_language_code = 'EN')
    {

        $this->db->select('reviews.*,  users_text.FullName, users.Image');
        $this->db->join('users','users.UserID = reviews.UserID');
        $this->db->join('users_text','users_text.UserID = users.UserID' );

        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );





        if($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
            $this->db->where('system_languages.IsDefault','1');
        }

        $this->db->where('reviews.ProductID',$product_id);

        return $this->db->get('reviews')->result_array();

    }


}