<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');


    function currentDate()
	{
		return date('Y-m-d H:i:s');
	}

	
   function print_rm($data)
	{
            echo '<pre>';
            print_r($data);exit;
	}

	function dump($data)
    {
        echo '<pre>';
        print_r($data);exit;
    }

    function checkUserFeePaid(){
        $CI = & get_Instance();
        $CI->load->model('User_model');
        $user_info = $CI->User_model->get($CI->session->userdata['admin']['UserID'],true,'UserID');

        if($user_info['RoleID'] != 1 && $user_info['RoleID'] != 2 && $user_info['RegistrationFeePaid'] == 'No'){
            redirect('cms/dashboard/payFee');
        }else{
            return true;
        }
    }
	

    function checkRightAccess($module_id,$role_id,$can){
        $CI = & get_Instance();
        $CI->load->model('Module_rights_model');
        
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['RoleID']   = $role_id;
        $fetch_by[$can]       = 1;
        
        $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
    
    function NullToEmpty($data)
    {
        $returnArr = array();
        if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
        {
            $i = 0;
            foreach ($data as $row)
            {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    }else{
                        $returnArr[$i][$key] = $value;
                    }
                }
                $i++;
            }
        }else{
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                }else{
                    $returnArr[$key] = $value;
                }
            }
        }
        return $returnArr;
    }

    function checkUserRightAccess($module_id,$user_id,$can){
        $CI = & get_Instance();
        $CI->load->model('Modules_users_rights_model');
        
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['UserID']   = $user_id;
        $fetch_by[$can]       = 1;
        
        $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
	

    function getSubCategories($category_id,$language = 'EN'){
        $CI = & get_Instance();
        $CI->load->model('Category_model');
        
       
        $result = $CI->Category_model->getAllJoinedData(false,'CategoryID',$language,'categories.IsActive = 1 AND categories.ParentID = '.$category_id.'','ASC','categories_text.Title',true);
        if($result){
            return $result;
        }else{
            return false;
        }
    }
    
   function getSystemLanguages(){
       $CI = & get_Instance();
       $CI->load->model('System_language_model');
       $languages = $CI->System_language_model->getAllLanguages();
       return $languages;
   }
   
    function getDefaultLanguage()
   {
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
        $fetch_by['IsDefault'] = 1;
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        return $result;
   }
   
   
   function getAllActiveModules($role_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Module_rights_model');
        $result = $CI->Module_rights_model->getModulesWithRights($role_id,$system_language_id,$where);
        return $result;
   }
   
   
   function getActiveUserModule($user_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Modules_users_rights_model');
        $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id,$system_language_id,$where,true);
        return $result;
   }
	
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('admin'))
	   {
		   return true;
		   
	   }else
	   {
		   redirect($CI->config->item('base_url'));
	   }
   }

   function getProductAvgRating($product_id){
        $CI = & get_Instance();
       $query = $CI->db->select_avg('rating', 'Rating');
       $query = $CI->db->where("ProductID", $product_id);
       $query = $CI->db->get('reviews');
       $result = $query->row();
       return round($result->Rating);
    }

    function canWriteReview($user_id, $product_id){
        $CI = & get_Instance();
        $query = $CI->db->select('rating', 'Rating');
        $query = $CI->db->where("UserID", $user_id);
        $query = $CI->db->where("ProductID", $product_id);
        $query = $CI->db->get('reviews');
        $result = $query->result_array();
        return (count($result) > 0 ) ? false : true;
    }

   function emailTemplate($data)
    {
      return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta content="en-us" http-equiv="Content-Language">  
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>BrandsValley</title>
    
<style type="text/css">
body {  
    margin:0;  
    padding:0;  
    background-color:transparent;  
    color:#999999;  
    font-family:Arial, Helvetica, sans-serif;  
    font-size:14px;  
    -webkit-text-size-adjust:none;  
    -ms-text-size-adjust:none;  
}  
h1 {  
    color:#5b5b5b;  
    margin-bottom:10px !important;  
}  
h2 {  
    color:#5b5b5b !important;  
    margin-bottom:5px !important;  
}  
h3 {  
    color:#999999 !important;  
    margin-bottom:0px !important;  
}  
a, a:link, a:visited {  
    color:#ad77bb;  
    text-decoration:none;  
}  
a:hover, a:active {  
    text-decoration:none;  
    color:#894b98 !important;  
}
.main-table td {
    border: 1px solid #eaeaea;
}
/*Hotmail and Yahoo specific code*/  
.ReadMsgBody {width: 100%;}  
.ExternalClass {width: 100%;}  
.yshortcuts {color: #894b98;}  
.yshortcuts a span {color: #894b98; border-bottom: none !important; background: none !important;}  
/*Hotmail and Yahoo specific code*/  
</style>
</head>
<!-- Start of page container -->  
<table width="100%" bgcolor="#FFFFFF" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:0;"> 
    <tr>  
        <td id="pageContainer" bgcolor="#fff" background="'.base_url().'assets/images/email_template_images/bg.gif" style="padding:0 20px 30px 20px; background-image:url("'.base_url().'assets/images/email_template_images/bg.gif); background-position:0; background-repeat:repeat;">  
  

            <table width="620" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:14px; line-height:15pt; color:#999999; margin:0 auto;">  
                                <tbody>
                                    <tr>  
                                                <td style="padding: 10px 5px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:15pt;color: #fff;background:#d8d8d8">  
                                        <a href="#"><img alt="image" width="140" height="" src="'.base_url().'assets/img/logo.png" border="0" hspace="0" vspace="0"></a></td>
                                        
                                        
                                    </tr> 
                                </tbody>
                        </table>
            <!-- Start of content container -->  
            <table width="620" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:14px; line-height:15pt; color:#999999; margin:0 auto; border:1px #e2e2e2 solid;">  
                <tr>  
                    <td bgcolor="#FFFFFF" style="padding:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:15pt; color:#999999;">  
                        <!-- Start of content block -->  
                        <table width="620" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:14px; line-height:15pt; color:#999999;min-height:610px;">  
                              
                             
                            <tr>  
                                <td colspan="2" style="padding-top:10px; padding-right:20px; padding-bottom:5px; padding-left:20px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:15pt; color:#999999;text-align:justify;">  
                                    '.$data.'
                                </td>  
                            </tr>
                                                        
                                                        
                                                        
                            <tr>  
                                <td width="580" colspan="2" style="padding-right:20px; padding-bottom:5px; padding-left:20px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:15pt; color:#999999;">
                                                                
                                                             
                                            
                    </td>  
                </tr>  
            </table>  
            <!-- End of content container -->  
                                    <table width="620" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:14px; line-height:15pt; color:#999999; margin:0 auto;">  
                                <tbody>
                                    <tr>
                                        <td style="width: 400px; background: #212124;padding-left: 10px;"><p>© copyright '.date('Y').'</p></td>
                                        <td style="width: 100px; padding: 10px 5px;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:15pt;color: #fff;background: #212124;">  
                                            <a href="#"><img src="'.base_url().'assets/images/email_template_images/twitter.png"></a>
                                            <a href="#" style="margin-left: 5px"><img src="'.base_url().'assets/images/email_template_images/facebook.png"></a>
                                            <a href="#" style="margin-left: 5px"><img src="'.base_url().'assets/images/email_template_images/google-plus.png"></a>
                                            <a href="#" style="margin-left: 5px"><img src="'.base_url().'assets/images/email_template_images/youtube.png"/></a>
                                        </td>  
                                    </tr> 
                                </tbody>
                        </table>
              
        </td>  
    </tr>  
</table>
<!-- End of page container -->  
</body>  
</html>';
    }
   
   
   function sendEmail($data= array())
	{
	
	
		$CI = & get_Instance();
		$CI->load->library('email');
		$CI->email->from($data['from']);
		$CI->email->to($data['to']);
		$CI->email->subject($data['subject']);
		$CI->email->message($data['body']);
		$CI->email->set_mailtype('html');
		
       if($CI->email->send()){
			return true;

		}else
		{
			return false;
		}
		
		

	}
 
 
	