<?php

class Base_Controller extends CI_Controller {

    protected $language;
    protected $country;
    protected $state;
    protected $city;

    public function __construct() {
 
        parent::__construct();
        
        $this->load->helper('cookie');
       //  delete_cookie('location');exit;

        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
        $this->data['site_setting'] = $this->getSiteSetting();
        if(!get_cookie('location')){

            $location_data = $this->getLocationByIp();
            if(!empty($location_data)){
                $this->country = $location_data['CountryID'];
                $this->state = $location_data['StateID'];
                $this->city = $location_data['CityID'];

                $cookie = array(
                    'name' => 'location',
                    'value' => $this->country.'@'.$this->state.'@'.$this->city,
                    'expire' => time() + 86500,

                 );

                set_cookie($cookie);
            }
        }else{

            $location_data = explode('@',get_cookie('location'));

            $this->country = $location_data[0];
            $this->state = $location_data[1];
            $this->city = $location_data[2]; 
        }

       $this->load->Model([
            'Country_model',
            'Category_model'
            
            
        ]);


      

        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language,'countries.IsActive = 1','ASC','countries_text.Title',true);
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.IsActive = 1 AND categories.ParentID = 0','ASC','categories_text.Title',true,15);
        $this->data['search_categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.IsActive = 1 AND categories.ParentID = 0','ASC','categories_text.Title',true);


    }


    public function setCookie($location_data = array()){
            
            if(get_cookie('location')){
                delete_cookie('location');
            }   

            //print_r($location_data);exit;
            if($location_data['CountryID'] == ''){
                $location_data['CountryID'] = 166;
            }
            if($location_data['StateID'] == ''){
                $location_data['StateID'] = 2728;
            }
            if($location_data['CityID'] == ''){
                $location_data['CityID'] = 31439;
                
            }
            $this->country = $location_data['CountryID'];
            $this->state = $location_data['StateID'];
            $this->city = $location_data['CityID'];

            $cookie = array(
                'name' => 'location',
                'value' => $this->country.'@'.$this->state.'@'.$this->city,
                'expire' => time() + 86500,

             );

            set_cookie($cookie);
    }

    public function changeLanguage($language) {
        $this->load->Model('Model_system_language');
        $fetch_by['GlobalCode'] = $language;
        $result = $this->Model_system_language->getWithMultipleFields($fetch_by);
        $languageID = $result->SysLID;
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
            $languageID = $default_lang->SystemLanguageID;
        }
        $this->session->set_userdata('lang', $language);
        //$this->session->set_userdata('languageID',$languageID);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting() {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false, $size = false) {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);


                // resize images if required.
                if($size) {
                    imageResize($size, $path . $file_name);
                }

                if (!$multiple) {
                    $image = $path . $file_name;
                    return $image;
                } else {
                    $this->load->model('Site_images_model');
                    $data['FileID'] = $id;
                    $data['ImageType'] = $type;
                    $uploaded_image = $path . $file_name;
                    $data['ImageName'] = $path . $file_name;
                    $this->Site_images_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }
    
    public function DeleteImage() {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


    public function getLocationByIp(){
        
        $this->load->model('City_model');
        if (in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local'])) {
           
             $IPaddress = '103.217.177.166';
        }else{
            $IPaddress =  $_SERVER['REMOTE_ADDR'];//'103.217.177.166';
        }
        //$IPaddress =  $_SERVER['REMOTE_ADDR'];//'103.217.177.166';
        $json = file_get_contents("http://ipinfo.io/{$IPaddress}");
        $details = json_decode($json);
        $names = json_decode(file_get_contents("http://country.io/names.json"), true);
        $location = array();

        $location['country_name']   = (isset($details->country) ? $names[$details->country] : 'Pakistan');
        $location['state_name']     = (isset($details->region) ? $details->region : 'Punjab');
        $location['city_name'] = (isset($details->city) ? $details->city : 'Punjab');

        $get_details = $this->City_model->getDataByCity($details->city);

        return $get_details;

       



    }
    
}
