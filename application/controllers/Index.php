<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller {
    public  $data = array();


    
    public function __construct() 
    {   
        
        parent::__construct();  
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
         $this->load->Model([
            'Country_model',
            'State_model',
            'State_text_model',
            'City_model',
            'City_text_model',
            'Product_model',
            'User_model',
            'UserProfileType_model',
            'Business_page_model'
        ]);
        
 
    }
     
    
    public function index()
    {   

        
       


        if(isset($_POST) && !empty($_POST)){

            $loc_data = array();
            $loc_data['CountryID'] = ($this->input->post('CountryID') != '' ? $this->input->post('CountryID') : 166 );
            $loc_data['StateID'] = ($this->input->post('StateID') != '' ? $this->input->post('StateID') : 2728 );//$this->input->post('StateID');
            $loc_data['CityID'] = ($this->input->post('CityID') != '' ? $this->input->post('CityID') : 31439 );//$this->input->post('CityID');

            $this->setCookie($loc_data);
          
        }

        $where  = '';
        $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";
        $this->data['latest_products'] = $this->Product_model->getProducts($this->language,$where . ' AND products.isActive = 1 ','DESC','products.ProductID',10,0);

        
        

        $top_categories = $this->Category_model->getRandomCategoryWithProducts($this->language,'categories.IsActive = 1 AND products.isActive = 1 ','rand()',1,0,'categories.CategoryID');

        $cat_products = array();
        if($top_categories){

            foreach ($top_categories as $key => $value) {

                    $products = $this->Product_model->getProducts($this->language,$where.' AND products.isActive = 1 AND products.CategoryID = '.$value['CategoryID'].' ','DESC','products.ProductID',8,0);


                    if($products){
                        $value['products'] = $products;
                    }else{
                        $value['products'] = array();
                    }
                    $cat_products[] = $value; 
 
            }


        }

        $this->data['top_categories'] = $cat_products;

        //print_rm($cat_products);
        
         /*$where_manufactures = "manufactures.CountryID = $this->country AND manufactures.StateID = $this->state AND manufactures.CityID = $this->city AND manufactures.IsActive = 1";
        $this->data['manufactures'] = $this->Manufacture_model->getAllJoinedData(false,'ManufactureID',$this->language,$where_manufactures);*/
        $where_manufactures = "users.CountryID = $this->country AND users.StateID = $this->state AND users.CityID = $this->city AND users.IsActive = 1 AND users.UserProfileType = 'Manufacturer'";
        $this->data['manufactures'] = $this->User_model->getAllJoinedData(false,'UserID',$this->language,$where_manufactures);
       


        $this->data['SliderImages'] = $this->Product_model->getSliderAndGif('products.IsActive = 1 AND product_locations.CityID
             = '.$this->city.' AND products.SliderImage = 1','rand()',4,0);
       

        $this->data['GifImages']    = $this->Product_model->getSliderAndGif('products.IsActive = 1 AND product_locations.CityID
             = '.$this->city.' AND products.Gif = 1','rand()',3,0);


        $this->data['CountryData'] =  $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID = '.$this->country,'DESC','');

        // Get all listings OR Profile Types OR manufacturers
        $this->data['ProfileTypes'] = $this->UserProfileType_model->getAll();


        $this->data['StateData'] =  $this->State_model->getJoinedData(false,'StateID','states.StateID = '.$this->state,'DESC','');
        $this->data['CityData'] =  $this->City_model->getJoinedData(false,'CityID','cities.CityID = '.$this->city,'DESC','');

        // get business pages by location
        $where_pages = "business_pages.CountryID = $this->country AND business_pages.StateID = $this->state AND business_pages.CityID = $this->city";
        $this->data['business_pages'] = $this->Business_page_model->getBusinessPages(false, $where_pages);

        //print_rm($this->data['business_pages']);

        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/home';
        $this->load->view('front/layouts/default',$this->data);
    }




    public function getCountryStates()
    {
        $CountryID = $this->input->post('CountryID');

        // To make selected state if user has selected state used in edit forms
        $selectedStateID = $this->input->post('SelectedStateID');

        $parent                             = 'State_model';
        
        $response = array();
        $options = '<option value="">'.lang("choose_state").'</option>';

        if(is_array($CountryID))
        {
            
            foreach ($CountryID as $key => $country) {
                $getCountryDetail = $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID ='.$country,'DESC','');
                $options .= '<optgroup label="'.$getCountryDetail[0]->Title.'">';
                $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$country,'ASC','states_text.Title',true);

                foreach ($States as $key => $value) {
                    if(!empty($selectedStateID)) {
                        $selected = ($selectedStateID == $value->StateID) ? "selected": "";
                        $options .= '<option value="'.$value->StateID.'" '.$selected.'>'.$value->Title.'</option>';
                    } else {
                        $options .= '<option value="'.$country.'-'.$value->StateID.'">'.$value->Title.'</option>';
                    }
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {

            $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$CountryID,'ASC','states_text.Title',true);

            foreach ($States as $key => $value) {
                if(!empty($selectedStateID)) {
                    $selected = ($selectedStateID == $value->StateID) ? "selected": "";
                    $options .= '<option value="'.$value->StateID.'" '.$selected.'>'.$value->Title.'</option>';
                }else {
                    $options .= '<option value="'.$value->StateID.'">'.$value->Title.'</option>';
                }

            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }
    

     public function getStateCities()
    {
        $StateID = $this->input->post('StateID');

        // To make selected city if user has selected city used in edit forms
        $SelectedCityID = $this->input->post('SelectedCityID');

        $parent                             = 'City_model';

        $response = array();
        $options = '<option value="">'.lang("choose_city").'</option>';
        
        if(is_array($StateID))
        {
            
            foreach ($StateID as $key => $state) {
                $statearr = explode('-', $state);

                $getStateDetail = $this->State_model->getJoinedData(false,'StateID','states.StateID ='.$statearr[1],'DESC','');

                $options .= '<optgroup label="'.$getStateDetail[0]->Title.'">';
                $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$statearr[1],'ASC','cities_text.Title',true);

                foreach ($Cities as $key => $value) {
                    if(!empty($SelectedCityID)) {
                        $selected = ($SelectedCityID == $value->CityID) ? "selected" : "";
                        $options .= '<option value="'.$value->CityID.'" '.$selected.' >'.$value->Title.'</option>';
                    }else {
                        $options .= '<option value="' . $state . '-' . $value->CityID . '">' . $value->Title . '</option>';
                    }
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {
            $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$StateID,'ASC','cities_text.Title',true);

            foreach ($Cities as $key => $value) {
                if(!empty($SelectedCityID)) {
                    $selected = ($SelectedCityID == $value->CityID) ? "selected" : "";
                    $options .= '<option value="'.$value->CityID.'" '.$selected.' >'.$value->Title.'</option>';
                }else {
                    $options .= '<option value="'.$value->CityID.'">'.$value->Title.'</option>';
                }

            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }


    public function setLocation(){
        
// i am leaving this because google api is paid
        $latitude = $this->input->post('Latitude');
        $lognitude = $this->input->post('Longitude');
        if(get_cookie('location')){
            delete_cookie('location');
        }
        
        
        $geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$lognitude&sensor=false");
        $output_deals = json_decode($geocode_stats);
        
        echo $country = $output_deals->results[2]->address_components[4]->long_name;
        $cookie = array(
                'name' => 'location',
                'value' => $latitude.'@'.$lognitude,
                'expire' => time() + 86500,

            );

        set_cookie($cookie);


       echo get_cookie('location');exit;

    }


    public function getCateroyProduct($CategoryID = ''){

        $keyword = strval($this->input->post('query'));

        $where = '';
        if($CategoryID != '') {
            $where = 'products.isActive = 1 AND products.CategoryID = '.$CategoryID;
        }else {
            $where .= ' products.isActive = 1 ';
        }


        $products = $this->Product_model->getProductData(true,'','EN',$where,'ASC','SortOrder','products_text.Title',$keyword);

        //echo $this->db->last_query(); exit;

        $result = array();
        if(!empty($products)){
            foreach ($products as $key => $value) {
                $result[$key]['label'] = $value["Title"];
                $result[$key]['key'] = $value["ProductID"];
            }
        }

        echo json_encode($result);

    }


    public function aboutbv() {


        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/aboutbv';
        $this->load->view('front/layouts/default',$this->data);
    }

    public function policy() {


        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/policy';
        $this->load->view('front/layouts/default',$this->data);
    }

    public function terms() {


        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/terms';
        $this->load->view('front/layouts/default',$this->data);
    }


}