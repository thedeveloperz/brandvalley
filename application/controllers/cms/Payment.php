<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
        $this->load->model('Payment_model');
        $this->load->model('User_model');
        $this->load->model('Product_model');
        $this->load->model('Package_model'); 
                
                
                
                
            $this->data['language']      = $this->language;
            $this->data['ControllerName'] = $this->router->fetch_class();
            $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
            $this->data['TableKey'] = 'PaymentID';
            $this->data['Table'] = 'payments';
       
        
    }
     
    
    public function index()
    {
          


          $parent  = $this->data['Parent_model'];
        $where = '';
        $total_earning_date_where = '';
        if($this->input->post()) {
            if($this->input->post('start_date')  == '' && $this->input->post('end_date')  == ''){

                $this->session->set_flashdata('message', "Please select date rang.");
                redirect(base_url('/cms/payment'));
            }
            $start_date = date('Y-m-d',strtotime($this->input->post('start_date')));
            $end_date   = date('Y-m-d',strtotime($this->input->post('end_date')));

            if(strtotime($this->input->post('start_date')) > strtotime($this->input->post('end_date'))){
                $temp = $start_date;
                $start_date = $end_date;
                $end_date = $temp;

            }


            $where .= ' AND DATE(payments.DepositDate) BETWEEN "'.$start_date.'" AND "'.$end_date.'"';

            //echo $start_date; exit;

            $this->data['post_data'] = $this->input->post();
        }else{
            $total_earning_date_where = ' AND DATE(payments.DepositDate) BETWEEN "'.date('Y-m-t').'" AND "'.date('Y-m-t',strtotime('today')).'"';
        }
          
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          if($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 2){
              $this->data['pending_registrations'] = $this->$parent->getRegistrationPayments('payments.Status = "pending" AND payments.PaymentType = "Registration" '.$where.'');
              $this->data['approved_registrations'] = $this->$parent->getRegistrationPayments('payments.Status = "approved" AND payments.PaymentType = "Registration" '.$total_earning_date_where.' '.$where.'');

          }




        if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
            $where .= " AND products.CreatedBy = ".$this->session->userdata['admin']['UserID'];


            $this->data['total_earning'] = $this->$parent->totalEarning('payments.Status = "approved" AND payments.PaymentType = "Product" AND (products.DollerSystemID = 2 OR products.DollerSystemID = 3) '.$where.'');

            $this->data['total_earning'] = ($this->data['total_earning'][0]->Total/2);

        }
          $this->data['pending_products'] = $this->$parent->getPublishProductPayments('payments.Status = "pending" AND payments.PaymentType = "Product" '.$where.'');

        $this->data['approved_products'] = $this->$parent->getPublishProductPayments('payments.Status = "approved" AND payments.PaymentType = "Product" '.$where.'');


        //echo $this->db->last_query();
        //print_rm($this->data['total_earning']);
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
           
            
            case 'approved':
                $this->approved();
            break;
                
                 
        }
    }


    private function approved(){


        $payment_id = $this->input->post('id');

        $payment_data = $this->Payment_model->get($payment_id,true,'PaymentID');
        if($payment_data){

            if($payment_data['PaymentType'] == 'Registration'){
                $this->User_model->update(array('RegistrationFeePaid' => 'Yes'),array('UserID' => $payment_data['CommonID'] ));
                $this->User_model->update(array('IsActive' => 1),array('UserID' => $payment_data['CommonID'] ));
            }

            if($payment_data['PaymentType'] == 'Product'){
                $this->Product_model->update(array('IsActive' => 1),array('ProductID' => $payment_data['CommonID']));

                $product_data = $this->Product_model->getSingleProducts($payment_data['CommonID']);
                //print_rm($product_data);
                if($product_data['Duration'] != ''){
                    $this->Product_model->update(array('ExpiryDate' => date('Y-m-d',strtotime("+ ".$product_data['Duration']." days"))),array('ProductID' => $payment_data['CommonID']));
                }else{
                    $this->Product_model->update(array('ExpiryDate' => date('Y-m-d',strtotime("+ 30 days"))),array('ProductID' => $payment_data['CommonID']));
                }
                
                $new_balance = $product_data['AvailableBalance'] +  ($payment_data['Amount']/2);
                $this->User_model->update(array('AvailableBalance' => $new_balance),array('UserID' => $product_data['CreatedBy'] ));
            }



            $affected_row = $this->Payment_model->update(array('Status' => 'approved'),array('PaymentID' => $payment_id));

            if($affected_row > 0){
                
                $success['error']   = false;
                $success['success'] = lang('approved_successfully');
                echo json_encode($success);
                exit;
            }

        }

    }
    
    
    
    
    

}