<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
       
        require_once APPPATH . 'third_party/googleApi/Google_Client.php';
        require_once APPPATH . 'third_party/googleApi/contrib/Google_Oauth2Service.php';
        require_once APPPATH . 'third_party/facebooksdk/vendor/autoload.php';
        
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Role_model');
        $this->load->model('Modules_users_rights_model');
        $this->load->model('Module_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Site_images_model');
        $this->load->model('UserProfileType_model');

        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['language'] = $this->language;
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
        $this->data['ControllerName'] = $this->router->fetch_class();
    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }

        redirect(base_url('cms/account/login'));
    }

    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }
        $this->data['view'] = 'backend/login';
        $this->load->view('backend/login', $this->data);

    }

    public function profile()
    {
        if (!$this->session->userdata('admin')) {
            redirect(base_url('cms/account/login'));
        }

        $user_id = $this->session->userdata('admin')['UserID'];

        $this->data['result'] = $this->User_model->getJoinedData(false, 'UserID', 'users.UserID ='.$user_id,'DESC','');

        $this->data['ProfileTypes'] = $this->UserProfileType_model->getAll();

        $this->data['view'] = 'backend/account/profile';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'updateProfile':
                $this->updateProfile();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }

    public function updateProfile()
    {

        $post_data = $this->input->post();
        unset($post_data['form_type']);
        $update_user_data['Phone'] = $post_data['Phone'];
        $update_user_data['UserProfileType'] = $post_data['UserProfileType'];
        $update_user_data['AboutUs'] = $post_data['AboutUs'];
        $update_user_data['Phone'] = $post_data['Phone'];
        $update_user_data['CountryID'] = $post_data['CountryID'];
        $update_user_data['StateID'] = $post_data['StateID'];
        $update_user_data['CityID'] = $post_data['CityID'];

        // Image uploading
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $uploaded_image = $this->uploadImage('Image', 'uploads/images/', false, false, false, 'medium');
            $update_user_data['Image'] = $uploaded_image;
            $this->session->userdata['admin']['Image'] = $update_user_data['Image'];
        }

        if (isset($_FILES['CoverImage']["name"][0]) && $_FILES['CoverImage']["name"][0] != '') {
            $update_user_data['CoverImage'] = $this->uploadImage('CoverImage', 'uploads/images/');
            
        }

        $update_by['UserID'] = $this->session->userdata('admin')['UserID'];;
        $this->User_model->update($update_user_data, $update_by);

        $save_child_data['FullName']         = $post_data['FullName'];
        $update_by['SystemLanguageID']       =  1; //default lang

        $this->User_text_model->update($save_child_data, $update_by);


        $success['error'] = false;
        $success['success'] = lang('save_successfully');
        $success['reload'] = true;
        echo json_encode($success);
        exit;

        //print_rm($post_data);
    }

    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('FullName', 'Full Name', 'required');
        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Country', 'Country', 'required');
        $this->form_validation->set_rules('State', 'State', 'required');
        $this->form_validation->set_rules('City', 'City', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }

    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);


        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['redirect'] = true;

           

            $data['url'] = 'cms/dashboard';
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['Password'] = md5($post_data['Password']);

        $user = $this->User_model->getUserData($post_data, $this->language);
       

        if (!empty($user)) {

            if($user['RoleID'] != '1' && $user['RoleID'] != '2')
            {
                if($user['IsActive'] == 0){
                    $data = array();
                    $data['success'] = 'false';
                    $data['error'] = 'Your account is not activated please contact admin';
                    echo json_encode($data);
                    exit();  
                }
                

            }


           
            
            $this->session->set_userdata('admin', $user);

            if (get_cookie('temp_order_key')) {
               // echo 'here';exit;
                $temp_user_id = get_cookie('temp_order_key');
                $update = array();
                $update['UserID'] = $this->session->userdata['admin']['UserID'];
                $update_by['UserID'] = $temp_user_id;
                $this->Temp_order_model->update($update, $update_by);
                delete_cookie('temp_order_key');

            }
            //$this->updateUserLoginStatus();
            return true;
        } else {
            return false;
        }

    }


     public function facebook_login() {

        /*            if($this->session->userdata('user_loggedin')){

          redirect('/home/dashboard');

         */
        @session_start();
        $fb = new Facebook\Facebook([
            'app_id' => '412317422558097', // Replace {app-id} with your app id
            'app_secret' => '9c4864bc3fa617af7e9bfd02c6eebc1b',
            'default_graph_version' => 'v2.2',
        ]);

        if (isset($_GET['code'])){ 

            // Get user facebook profile details


            try {

                $helper = $fb->getRedirectLoginHelper();
                $accessToken = $helper->getAccessToken();


                $object = $fb->get('/me?fields=id,first_name,last_name,email,gender,locale,picture', $accessToken);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            // Preparing data for database insertion
            $body = (string) $object->getBody();
            $userProfile = json_decode($body);


            
            // my working 
            $user = $this->User_model->getUserData($userProfile->email, $this->language);
           if (!empty($user)) {

                if($user['RoleID'] != '1' && $user['RoleID'] != '2')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                    

                }
                
                $this->session->set_userdata('admin', $user);
                //$this->updateUserLoginStatus();
                redirect('cms/dashboard');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'facebook';
                $save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 3;
                
                $save_parent_data['Email']          = $userProfile->email;
                $save_parent_data['Gender']          = $userProfile->gender;
                $save_parent_data['RegistrationFeePaid'] = 'No';
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
               
                
                 
                

                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    
                
                
                
                    $default_lang = getDefaultLanguage();
                    
                    
                    
                    $save_child_data['FullName']                        = $userProfile->first_name.' '.$userProfile->last_name;
                    $save_child_data['UserID']                          = $insert_id;
                    $save_child_data['SystemLanguageID']                = 1;
                   $this->$child->save($save_child_data);

                    $modules = $this->Module_model->getAll();
                    foreach($modules as $key => $value)
                    {
                        
                            $other_data[] = [
                                    'ModuleID'  => $value->ModuleID,
                                    'UserID'    => $insert_id,
                                    'CanView'   => (checkRightAccess($value->ModuleID, 3, 'CanView') ? 1 : 0),
                                    'CanAdd'    => (checkRightAccess($value->ModuleID, 3, 'CanAdd') ? 1 : 0),
                                    'CanEdit'   => (checkRightAccess($value->ModuleID, 3, 'CanEdit') ? 1 : 0),
                                    'CanDelete' => (checkRightAccess($value->ModuleID, 3, 'CanDelete') ? 1 : 0),
                                    'CreatedAt' => date('Y-m-d H:i:s'),
                                    'CreatedBy' => 1,
                                    'UpdatedAt' => date('Y-m-d H:i:s'),
                                    'UpdatedBy' => 1
                            ];



                    }
                    
                    
                   
                    $this->Modules_users_rights_model->insert_batch($other_data);
                    
                    
                    $user = $this->User_model->getUserData($userProfile->email, $this->language);
                    $this->session->set_userdata('admin', $user);

                  
                  
                    redirect('cms/dashboard');


                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect('account/login');
                }

            } 
            
            // end

           





            // Get logout URL
            //  $data['logoutUrl'] = $this->facebook->logout_url();
        } else {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(base_url().'cms/account/facebook_login', $permissions);
            $loginUrl = htmlspecialchars($loginUrl);
            $loginUrl = str_replace("amp;", "", $loginUrl);
            header("Location: $loginUrl");
            exit;
        }
    }

    public function google_login() {
        if ($this->session->userdata('user_loggedin')) {
            redirect('/');
        } else {
            $clientId = '412994844618-fvjajuvp6bfq8fi0dg7b573c8ajqimm9.apps.googleusercontent.com'; //Google client ID
            $clientSecret = 'f05-xVrxSR86MR0PdZcaji_V'; //Google client secret
            $redirectURL = base_url() . 'cms/account/google_login';

            //Call Google API
            $gClient = new Google_Client();
            $gClient->setApplicationName('Login');
            $gClient->setClientId($clientId);
            $gClient->setClientSecret($clientSecret);
            $gClient->setRedirectUri($redirectURL);
            $google_oauthV2 = new Google_Oauth2Service($gClient);

            if (isset($_GET['code'])) {
                $gClient->authenticate($_GET['code']);
                $_SESSION['token'] = $gClient->getAccessToken();
                header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));


                if (isset($_SESSION['token'])) {
                    $gClient->setAccessToken($_SESSION['token']);
                }

                if ($gClient->getAccessToken()) {
                    $userProfile = $google_oauthV2->userinfo->get();

                    

                   

                    // my working 
            $user = $this->User_model->getUserData($userProfile['email'], $this->language);
           if (!empty($user)) {

                if($user['RoleID'] != '1' && $user['RoleID'] != '2')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                    

                }
                
                $this->session->set_userdata('admin', $user);
                //$this->updateUserLoginStatus();
                redirect('cms/dashboard');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'gmail';
                //$save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 3;
                
                $save_parent_data['Email']          = $userProfile['email'];
                $save_parent_data['Gender']         = $userProfile['gender'];
                $save_parent_data['RegistrationFeePaid'] = 'No';
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
               
                
                 
                

                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    
                
                
                
                    $default_lang = getDefaultLanguage();
                    
                    
                    
                    $save_child_data['FullName']                        = $userProfile['given_name'].' '.$userProfile['family_name'];
                    $save_child_data['UserID']                          = $insert_id;
                    $save_child_data['SystemLanguageID']                = 1;
                   $this->$child->save($save_child_data);

                    $modules = $this->Module_model->getAll();
                    foreach($modules as $key => $value)
                    {
                        
                            $other_data[] = [
                                    'ModuleID'  => $value->ModuleID,
                                    'UserID'    => $insert_id,
                                    'CanView'   => (checkRightAccess($value->ModuleID, 3, 'CanView') ? 1 : 0),
                                    'CanAdd'    => (checkRightAccess($value->ModuleID, 3, 'CanAdd') ? 1 : 0),
                                    'CanEdit'   => (checkRightAccess($value->ModuleID, 3, 'CanEdit') ? 1 : 0),
                                    'CanDelete' => (checkRightAccess($value->ModuleID, 3, 'CanDelete') ? 1 : 0),
                                    'CreatedAt' => date('Y-m-d H:i:s'),
                                    'CreatedBy' => 1,
                                    'UpdatedAt' => date('Y-m-d H:i:s'),
                                    'UpdatedBy' => 1
                            ];



                    }
                    
                    
                   
                    $this->Modules_users_rights_model->insert_batch($other_data);
                    
                    
                    $user = $this->User_model->getUserData($userProfile->email, $this->language);
                    $this->session->set_userdata('admin', $user);

                  
                  
                    redirect('cms/dashboard');


                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect('account/login');
                }

            } 
            
            // end
                }
            } else {
                $url = $gClient->createAuthUrl();
                header("Location: $url");
                exit;
            }
        }
    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url') . 'account/login');

    }
}