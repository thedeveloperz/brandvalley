<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
        checkUserFeePaid();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model',
            ucfirst($this->router->fetch_class()).'_location_model',
            'Country_model',
            'Country_text_model',
            'Category_model',
            'Category_text_model',
            'State_model',
            'State_text_model',
            'City_model',
            'City_text_model',
            'Site_images_model',
            'Site_videos_model',
            'DollerSystem_model',
             'Payment_model',
             'Package_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['Location_model'] = ucfirst($this->router->fetch_class()).'_location_model';
                $this->data['TableKey'] = 'ProductID';
                $this->data['Table'] = 'products';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';

          $where = false;
          if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2)
          {
            $where = $this->data['Table'].'.CreatedBy = '.$this->session->userdata['admin']['UserID'];
          }

         
        
          $this->data['results'] = $this->$parent->getProductData(false,$this->data['TableKey'],$this->language, $where);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {

         if(!checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];        
        
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.ParentID = 0');
        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language);

        $this->data['doller_systems']    = $this->DollerSystem_model->getAllJoinedData(false,'DollerSystemID',$this->language,'dollersystems.IsActive = 1');
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $location                           = $this->data['Location_model'];
        $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');

        
    
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.ParentID = 0');
        
        $this->data['subcategories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.ParentID = '.$this->data['result'][0]->CategoryID);
        $this->data['countries'] = $this->Country_model->getAllJoinedData(false,'CountryID',$this->language);

        $locations = $this->$location->getMultipleRows(array('ProductID'=>$id), true);

        $productStates = array();
        $productCities = array();
        $this->data['selectedCountries'] = array();

        $statesDrp = '';
        $citiesDrp = '';

        if($locations)
        {
            $productStates = array_column($locations, 'StateID');
            $productCities = array_column($locations, 'CityID');
            $this->data['selectedCountries'] = array_column($locations, 'CountryID');
        

            foreach ($locations as $key => $loc) {
                $getCountryDetail = $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID ='.$loc['CountryID'],'DESC','');
                $statesDrp .= '<optgroup label="'.$getCountryDetail[0]->Title.'">';
                $States = $this->State_model->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$loc['CountryID']);

                foreach ($States as $key => $value) {
                    $statesDrp .= '<option '.(in_array($value->StateID, $productStates) ? 'selected="selected"' : '').' value="'.$loc['CountryID'].'-'.$value->StateID.'">'.$value->Title.'</option>';
                }
                $statesDrp .= '</optgroup>'; 

                $getStateDetail = $this->State_model->getJoinedData(false,'StateID','states.StateID ='.$loc['StateID'],'DESC','');
                $citiesDrp .= '<optgroup label="'.$getStateDetail[0]->Title.'">';
                $Cities = $this->City_model->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$loc['StateID']);

                foreach ($Cities as $key => $value) {
                    $citiesDrp .= '<option '.(in_array($value->CityID, $productCities) ? 'selected="selected"' : '').' value="'.$loc['CountryID'].'-'.$loc['StateID'].'-'.$value->CityID.'">'.$value->Title.'</option>';
                }
                $citiesDrp .= '</optgroup>';
            }
        }

        
        $this->data['states'] = $statesDrp;

        $this->data['cities'] = $citiesDrp;


        $this->data['site_images'] = $this->Site_images_model->getMultipleRows(array('FileID'=>$id, 'ImageType'=>'ProductImage'), true);

        $this->data['site_videos'] = $this->Site_videos_model->getMultipleRows(array('FileID'=>$id, 'VideoType'=>'Productvideos'), true);
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required');

        // $this->form_validation->set_rules('CompanyName', 'Company Name', 'required');
        // $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('CategoryID', 'Category', 'required');
        $this->form_validation->set_rules('SubCategoryID', 'Sub Category', 'required');
        $this->form_validation->set_rules('Country[]', 'Country', 'required');
        $this->form_validation->set_rules('State[]', 'State', 'required');
        $this->form_validation->set_rules('City[]', 'City', 'required');
        $this->form_validation->set_rules('Price', 'Price', 'required');


        if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 ) {
            $this->form_validation->set_rules('DollersystemID', 'Publish Ad', 'required');
            $this->form_validation->set_rules('DurationPackage', 'Package Type', 'required');
            $this->form_validation->set_rules('PaymentVia', 'Payment Type', 'required');
            $this->form_validation->set_rules('Cnic', 'CNIC', 'required');
            $this->form_validation->set_rules('Amount', 'Amount', 'required');
            $this->form_validation->set_rules('TransactionID', 'Transaction Id', 'required');
        }




        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        if(!checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }



        $post_data                          = $this->input->post();

       //print_rm($post_data);

        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $location                           = $this->data['Location_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
        $save_payment_data                  = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue)) {
            $sort = $getSortValue['SortOrder'] + 1;
        }

        // expiry date

        if($this->session->userdata['admin']['RoleID'] == 1
            || $this->session->userdata['admin']['RoleID'] == 2 ) {

            $save_parent_data['ExpiryDate']  = date('Y-m-d', strtotime("+30 days"));
            $post_data['IsActive'] = 1;

        }

        $save_parent_data['CompanyUrl']       =  (isset($post_data['CompanyUrl']) ? $post_data['CompanyUrl'] : '');
        $save_parent_data['Price']       =  (isset($post_data['Price']) ? $post_data['Price'] : 0);
        $save_parent_data['PackageID']       = (isset($post_data['DurationPackage']) ? 1 : 0 ); //$post_data['DurationPackage'];
        $save_parent_data['DollersystemID']       = (isset($post_data['DollersystemID']) ? 1 : 0 );
        $save_parent_data['Email']          = (isset($post_data['Email']) ? $post_data['CompanyUrl'] : '');
        $save_parent_data['CategoryID']       = $post_data['CategoryID'];
        $save_parent_data['SubCategoryID']       = $post_data['SubCategoryID'];
        $save_parent_data['Contact']       = $post_data['Contact'];
        $save_parent_data['WhatsApp']       = (isset($post_data['WhatsApp']) ? $post_data['WhatsApp'] : '');
        $save_parent_data['PositionNumber']   = (isset($post_data['PositionNumber']) ? $post_data['PositionNumber'] : '');
        $save_parent_data['SliderImage']       = (isset($post_data['SliderImage']) ? 1 : 0 );
        $save_parent_data['Gif']       = (isset($post_data['Gif']) ? 1 : 0 );



        $save_parent_data['Globe']       = (isset($post_data['Globe']) ? 1 : 0 );
        
        
        $save_parent_data['SortOrder']      = $sort;

        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $locations = $post_data['City'];
      

        if (isset($_FILES['SliderImageFile']["name"][0]) && $_FILES['SliderImageFile']["name"][0] != '') {

            //print_rm($_FILES['SliderImageFile']);
            $save_parent_data['SliderImageFile'] = $this->uploadImage('SliderImageFile', 'uploads/images/');
        }
        if (isset($_FILES['GifImageFile']["name"][0]) && $_FILES['GifImageFile']["name"][0] != '') {
            $save_parent_data['GifImageFile'] = $this->uploadImage('GifImageFile', 'uploads/images/');
        }

        $insert_id                          = $this->$parent->save($save_parent_data);

        $save_location_data = array();
        foreach($locations as $loc)
        {
            $locarr = explode('-', $loc);
            $save_location_data['ProductID'] = $insert_id;
            $save_location_data['CountryID'] = $locarr[0];
            $save_location_data['StateID'] = $locarr[1];
            $save_location_data['CityID'] = $locarr[2];
            $this->$location->save($save_location_data);
            
        }
        if($insert_id > 0)
            {
                
                //Email Multiple Files
                if (isset($_FILES['Image']) && !empty($_FILES['Image'])) {
                    $file = 'Image';
                    $path = 'uploads/product_image/';
                    $key = $insert_id;
                    $type = 'ProductImage';
                    $this->uploadImage($file, $path, $key, $type, TRUE, 'large');
                } else {
                    $data['FileID'] = $insert_id;
                    $data['ImageType'] = 'ProductImage';
                    $data['ImageName'] = 'assets/backend/images/no_image.jpg';
                    $this->Site_images_model->save($data);
                }
                //End

                if(isset($post_data['VideoURL']))
                {
                    $videos = $post_data['VideoURL'];
                    foreach($videos as $vid)
                    {
                        if($vid != '')
                        {
                            $vidData['FileID'] = $insert_id;
                            $vidData['VideoType'] = 'Productvideos';
                            $vidData['VideoURL'] = $vid;
                            $this->Site_videos_model->save($vidData);
                        }
                    }
                }   
            
            
                $default_lang = getDefaultLanguage();
                
                
                $save_child_data['CompanyName']                  = (isset($post_data['CompanyName']) ? $post_data['CompanyName'] : '');
                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data['ShortDescription']             = $post_data['ShortDescription'];
                $save_child_data['Description']                  = $post_data['Description'];
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);


                if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2 )
                {

                    $package = $this->Package_model->get($post_data['DurationPackage'], true, 'PackageID');
                    $priceToPaid = 0;
                    if($package) {
                        $priceToPaid = $package['Charges'] - $package['Discount'];

                    }

                    if($save_parent_data['Gif'] == 1){
                        $priceToPaid = $priceToPaid + 10;
                    }
                    if($save_parent_data['SliderImage'] == 1){
                        $priceToPaid = $priceToPaid + 20;
                    }

                    // Save Payment Information.
                    $save_payment_data['CommonID']                = $insert_id;
                    $save_payment_data['PaymentType']             = 'Product';
                    $save_payment_data['PaymentVia']              = $post_data['PaymentVia'];
                    $save_payment_data['Cnic']                    = $post_data['Cnic'];
                    $save_payment_data['TransactionID']           = $post_data['TransactionID'];
                    $save_payment_data['DepositDate']             = date('Y-m-d', strtotime($post_data['DepositDate']));
                    $save_payment_data['Amount']                  = $post_data['Amount'];
                    $save_payment_data['AmountToPaid']            = $priceToPaid;

                    $this->Payment_model->save($save_payment_data);
                }
              
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
    private function update()
    {
        

        if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }

            if(!checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];
                $location                           = $this->data['Location_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
            unset($post_data['form_type']);
            $save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                    
                   
                    $save_parent_data['CompanyUrl']       = (isset($post_data['CompanyUrl']) ? $post_data['CompanyUrl'] : '');
                    $save_parent_data['Email']       = (isset($post_data['Email']) ? $post_data['Email'] : '');
                    $save_parent_data['CategoryID']       = $post_data['CategoryID'];
                    $save_parent_data['SubCategoryID']       = $post_data['SubCategoryID'];
                    $save_parent_data['Contact']       = (isset($post_data['Contact']) ? $post_data['Contact'] : '');
                    $save_parent_data['WhatsApp']       = (isset($post_data['WhatsApp']) ? $post_data['WhatsApp'] : '');
                    $save_parent_data['PositionNumber']   = (isset($post_data['PositionNumber']) ? $post_data['PositionNumber'] : '');
                    $save_parent_data['SliderImage']       = (isset($post_data['SliderImage']) ? 1 : 0 );
                    $save_parent_data['Globe']       = (isset($post_data['Globe']) ? 1 : 0 );
                    $save_parent_data['Gif']       = (isset($post_data['Gif']) ? 1 : 0 );

                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    if (isset($_FILES['SliderImageFile']["name"][0]) && $_FILES['SliderImageFile']["name"][0] != '') {
                        $save_parent_data['SliderImageFile'] = $this->uploadImage('SliderImageFile', 'uploads/images/');
                    }
                    if (isset($_FILES['GifImageFile']["name"][0]) && $_FILES['GifImageFile']["name"][0] != '') {
                        $save_parent_data['GifImageFile'] = $this->uploadImage('GifImageFile', 'uploads/images/');
                    }
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    
                    
                    
                    $this->$parent->update($save_parent_data,$update_by);

                    $locations = $post_data['City'];

                    $delete_location_by = array();

                    $delete_location_by['ProductID'] = base64_decode($post_data[$this->data['TableKey']]);

                    $this->$location->delete($delete_location_by);

                    $save_location_data = array();
                    foreach($locations as $loc)
                    {
                        $locarr = explode('-', $loc);
                        $save_location_data['ProductID'] = base64_decode($post_data[$this->data['TableKey']]);
                        $save_location_data['CountryID'] = $locarr[0];
                        $save_location_data['StateID'] = $locarr[1];
                        $save_location_data['CityID'] = $locarr[2];
                        $this->$location->save($save_location_data);
                        
                    }

                    //Email Multiple Files
                    if (isset($_FILES['Image']) && !empty($_FILES['Image'])) {
                        $file = 'Image';
                        $path = 'uploads/product_image/';
                        $key = base64_decode($post_data[$this->data['TableKey']]);
                        $type = 'ProductImage';
                        $this->uploadImage($file, $path, $key, $type, TRUE, 'large');
                    }

                    $delete_video_by = array();
                    $delete_video_by['FileID'] = base64_decode($post_data[$this->data['TableKey']]);
                    $delete_video_by['VideoType'] = 'Productvideos';
                    $this->Site_videos_model->delete($delete_video_by);


                    if(isset($post_data['VideoURL']))
                    {
                        $videos = $post_data['VideoURL'];
                        foreach($videos as $vid)
                        {
                            if($vid != '')
                            {
                                $vidData['FileID'] = base64_decode($post_data[$this->data['TableKey']]);
                                $vidData['VideoType'] = 'Productvideos';
                                $vidData['VideoURL'] = $vid;
                                $this->Site_videos_model->save($vidData);
                            }
                        }
                    }  



                    $save_child_data['CompanyName']                  = (isset($post_data['CompanyName']) ? $post_data['CompanyName'] : '');
                    $save_child_data['Title']                        = $post_data['Title'];
                    $save_child_data['ShortDescription']             = $post_data['ShortDescription'];
                    $save_child_data['Description']                  = $post_data['Description'];
                   
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['CompanyName']                  = $post_data['CompanyName'];
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data['ShortDescription']             = $post_data['ShortDescription'];
                        $save_child_data['Description']                  = $post_data['Description'];
                       
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];



                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['CompanyName']                  = $post_data['CompanyName'];
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data['ShortDescription']             = $post_data['ShortDescription'];
                        $save_child_data['Description']                  = $post_data['Description'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
        
        


        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkUserRightAccess(45,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $location                           = $this->data['Location_model'];
        
        $delete_video_by = array();
        $delete_video_by['FileID'] = $this->input->post('id');
        $delete_video_by['VideoType'] = 'Productvideos';
        $this->Site_videos_model->delete($delete_video_by);

        $get_site_images = $this->Site_images_model->getMultipleRows(array('FileID'=>$this->input->post('id'), 'ImageType'=>'ProductImage'), true);

        if ($get_site_images) {
            foreach ($get_site_images as $file) {
                if (file_exists($file['ImageName'])) {
                    unlink($file['ImageName']);
                }
                $image_deleted_by['SiteImageID'] = $file['SiteImageID'];
                $this->Site_images_model->delete($image_deleted_by);
            }
        }

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $this->$location->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    

}