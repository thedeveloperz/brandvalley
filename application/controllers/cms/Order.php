<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            'Order_status_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                
                $this->data['TableKey'] = 'OrderID';
                $this->data['Table'] = 'orders';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          $where = false;
          if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
            $where ='orders.Status = "Received" AND products.CreatedBy = '.$this->session->userdata['admin']['UserID'];// we are ading Received because admin did approved his payment 
          }

          $this->data['results'] = $this->$parent->getOrders($where,$this->language);


          
          $this->load->view('backend/layouts/default',$this->data);
    }
    
    
    public function detail($id = '')
    {
        if(!checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];

        $where = false;
          if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
            $where ='products.CreatedBy = '.$this->session->userdata['admin']['UserID'];// we are ading Received because admin did approved his payment 
          }
        $this->data['order_items']          = $this->$parent->getOrdersDetail($id,$this->language,$where);
    
        
        if(!$this->data['order_items']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
      
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/invoice';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'change_status':
                $this->changeStatus();
                
          break; 
                
                 
        }
    }


    private function changeStatus(){
        if(!checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanView')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        
        $update_by = array();
        $update_by['OrderID'] = $this->input->post('id');
        if($this->input->post('value') == 'Received'){
            $this->Order_model->update(array('Status' => $this->input->post('value')),$update_by);
         }elseif($this->input->post('value') == 'Dispatched' || $this->input->post('value') == 'Delivered'){

            
            $update_by['UserID'] = $this->session->userdata['admin']['UserID'];
            $user_status_check   = $this->Order_status_model->getWithMultipleFields($update_by);

            if($user_status_check){
                $this->Order_status_model->update(array('Status' => $this->input->post('value')),$update_by);
            }else{
                $save_data = array();
                $save_data['UserID'] = $this->session->userdata['admin']['UserID'];
                $save_data['Status'] = $this->input->post('value');
                $save_data['OrderID'] = $this->input->post('id');
                $this->Order_status_model->save($save_data);
            }


         }

       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('update_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    
    
    

}