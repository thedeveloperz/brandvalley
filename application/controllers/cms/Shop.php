<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
              $this->load->Model([
               'Order_model',
                'Order_status_model'
            ]); 
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = 'Order_model';
                
                
                $this->data['TableKey'] = 'OrderID';
                $this->data['Table'] = 'orders';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          $where = false;
          if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
            $where ='orders.UserID = '.$this->session->userdata['admin']['UserID'];// we are ading Received because admin did approved his payment 
          }

          $this->data['results'] = $this->$parent->getOrders($where,$this->language);


          
          $this->load->view('backend/layouts/default',$this->data);
    }
    

    public function detail($id = '')
    {
        if(!checkUserRightAccess(49,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];

        $where = false;
          if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2){
            $where ='orders.UserID = '.$this->session->userdata['admin']['UserID'];// we are ading Received because admin did approved his payment 
          }
        $this->data['order_items']          = $this->$parent->getOrdersDetail($id,$this->language,$where);
    
        
        if(!$this->data['order_items']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
      
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/invoice';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    

}