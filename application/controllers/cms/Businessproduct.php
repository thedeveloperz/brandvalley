<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Businessproduct extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model'
        ]);

        //$this->load->library('image_lib');


        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Role_model');
        $this->load->model('UserProfileType_model');
        $this->load->model('Business_page_model');

                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'BusinessproductID';
                $this->data['Table'] = 'businessproducts';
       
        
    }


    public function businessPage()
    {
        if (!$this->session->userdata('admin')) {
            redirect(base_url('cms/account/login'));
        }

        $user_id = $this->session->userdata('admin')['UserID'];

        $business_page = $this->Business_page_model->get($user_id, false, 'UserID');
        $this->data['businessPage'] = $business_page;
        //print_rm($business_page);


        $this->data['result'] = $this->User_model->getJoinedData(false, 'UserID', 'users.UserID ='.$user_id,'DESC','');

        //$this->data['ProfileTypes'] = $this->UserProfileType_model->getAll();


        //print_rm($this->data['ProfileTypes']);


        $this->data['view'] = 'backend/businessproduct/business_page';
        $this->load->view('backend/layouts/default', $this->data);
    }

    /**
     *  BusinessPage creation and update is happening in this function.
     */
    public function businessPageAction()
    {
        $post_data = $this->input->post();
        $user_id = $this->session->userdata('admin')['UserID'];
        $business_page = $this->Business_page_model->get($user_id, false, 'UserID');

        if($post_data) {
            $input_data['UserID'] = $user_id;
            $input_data['BusinessName'] = $post_data['BusinessName'];
            $input_data['CountryID'] = $post_data['CountryID'];
            $input_data['StateID'] = $post_data['StateID'];
            $input_data['CityID'] = $post_data['CityID'];
            $input_data['CreatedAt'] = date('Y-m-d H:i:s');
            $input_data['UpdatedAt'] = date('Y-m-d H:i:s');

// Upload Business Logo
            if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                  $imageUploaded = $this->uploadImage('Image', 'uploads/images/', false, false, false, 'thumbnail');
                  $input_data['BusinessLogo'] = $imageUploaded;
            }

         // check is already created then update the page otherwise add new page
            $insert_id = ($business_page) ? $business_page->BusinessPageID : 0;
            $notification = 'Business Page Created Successfully';
            if (!$business_page) {
             // add new business page
                $insert_id = $this->Business_page_model->save($input_data);
            } else {
             // update business page
                $update_by = array();
                $update_by['BusinessPageID'] = $insert_id;
                $this->Business_page_model->update($input_data, $update_by);
                $notification = 'Business Page Updated Successfully';
            }

            $success['error'] = false;
            $success['success'] = $notification;
            $success['reload'] = true;

            echo json_encode($success);
            exit;


        }else {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
    }


    
    public function index()
    {
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        $user_id = $this->session->userdata['admin']['UserID'];
        $business_page = $this->Business_page_model->get($user_id, false, 'UserID');


        $where = false;
        if($this->session->userdata['admin']['RoleID'] != 1 && $this->session->userdata['admin']['RoleID'] != 2) {
            $where = $this->data['Table'].'.CreatedBy = '.$user_id;
            if($business_page) {
                $where = $this->data['Table'].'.CreatedBy = '.$user_id . " AND " . $this->data['Table'] .".BusinessPageID = $business_page->BusinessPageID";
            }
        }

        $this->data['results'] = $this->Business_page_model->getAllBusinessProducts(false,$this->data['TableKey'],$this->language, $where);

        $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
//         if(!checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanAdd')){
//             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
//             redirect(base_url('cms/'.$this->router->fetch_class()));
//        }
        $user_id = $this->session->userdata('admin')['UserID'];
        $business_page = $this->Business_page_model->get($user_id, false, 'UserID');
        if(!$business_page) {
            $errors['error'] =  "Create business page first, before creating business product.";
            $errors['success'] = false;
            redirect($_SERVER['HTTP_REFERER']);
        }
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
//        if(!checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit')){
//             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
//             redirect(base_url('cms/'.$this->router->fetch_class()));
//        }
        $parent = $this->data['Parent_model'];
        $product = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
        if(!$product) redirect(base_url('cms/'.$this->router->fetch_class()));


        $this->data['product'] = $product[0];
        //print_rm($this->data['product']);

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
    $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;
            case 'businessPageAction':
                $this->validateBusinessPageValues();
                $this->businessPageAction();
          break;      
                 
        }
    }


    private function validateBusinessPageValues(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('BusinessName', 'Business Page Name', 'required');
        $this->form_validation->set_rules('CountryID[]', 'Country', 'required');
        $this->form_validation->set_rules('StateID[]', 'State', 'required');
        $this->form_validation->set_rules('CityID[]', 'City', 'required');





        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required');
        $this->form_validation->set_rules('Description', lang('description'), 'required');
        $this->form_validation->set_rules('CountryID[]', 'Country', 'required');
        $this->form_validation->set_rules('StateID[]', 'State', 'required');
        $this->form_validation->set_rules('CityID[]', 'City', 'required');


        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }


    
    private function save()
    {

//        if(!checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanAdd')){
//            $errors['error'] =  lang('you_dont_have_its_access');
//            $errors['success'] = false;
//            $errors['redirect'] = true;
//            $errors['url'] = 'cms/'.$this->router->fetch_class();
//            echo json_encode($errors);
//            exit;
//        }

        // First check this user has business page or not?
        $user_id = $this->session->userdata('admin')['UserID'];
        $business_page = $this->Business_page_model->get($user_id, false, 'UserID');
        if(!$business_page) {
            $errors['error'] =  "Create business page first, before creating business product.";
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }


        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }

        $save_parent_data['BusinessPageID'] = $business_page->BusinessPageID;
        $save_parent_data['CountryID'] = $post_data['CountryID'];
        $save_parent_data['StateID'] = $post_data['StateID'];
        $save_parent_data['CityID'] = $post_data['CityID'];

        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];

        // upload image
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $uploaded_image = $this->uploadImage('Image', 'uploads/images/', false, false, false, 'medium');
            $save_parent_data['Image'] = $uploaded_image;
        }
        $insert_id   = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
            
                $default_lang = getDefaultLanguage();

                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data['Description']                  = $post_data['Description'];
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);

                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;

            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
//                if(!checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit')){
//                $errors['error'] =  lang('you_dont_have_its_access');
//                $errors['success'] = false;
//                $errors['redirect'] = true;
//                $errors['url'] = 'cms/'.$this->router->fetch_class();
//                echo json_encode($errors);
//                exit;
//            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];

                if(isset($post_data[$this->data['TableKey']])){
                    $id = $post_data['BusinessproductID'];
                    $this->data['result']  = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');

                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
            unset($post_data['form_type']);
                 $save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){

                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = $post_data['BusinessproductID'];

                    $save_parent_data['CountryID'] = $post_data['CountryID'];
                    $save_parent_data['StateID'] = $post_data['StateID'];
                    $save_parent_data['CityID'] = $post_data['CityID'];

                    // upload image
                    if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                        $uploaded_image = $this->uploadImage('Image', 'uploads/images/', false, false, false, 'medium');
                        $save_parent_data['Image'] = $uploaded_image;
                    }


                    $this->$parent->update($save_parent_data,$update_by);


                    $save_child_data['Title']                        = $post_data['Title'];
                    $save_child_data['Description']                        = $post_data['Description'];



                   
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  1;


                    //print_rm($save_child_data);

                    $this->$child->update($save_child_data,$update_by);




                }else{
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                       
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];



                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['Title']                        =  $post_data['Title'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;
        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   
    
    private function delete(){
        
         if(!checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    

}