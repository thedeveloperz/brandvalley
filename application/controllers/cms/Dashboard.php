<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
        
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->load->model('Payment_model');
        $this->load->model('User_model');        
       
		
	}
	 
    
    public function index()
	{
          
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        $this->load->view('backend/layouts/default',$this->data);
	}

	public function payFee(){
		$this->data['view'] = 'backend/pay_fee_form';
        $this->load->view('backend/layouts/default',$this->data);

	}

	public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validation();
                $this->savePaidFee();
          break; 
           
                 
        }
    }

    private function validation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
        $this->form_validation->set_rules('Cnic', 'Cnic', 'required');
        $this->form_validation->set_rules('Amount', 'Deposit Amount', 'required');
        $this->form_validation->set_rules('TransactionID', 'Transaction ID', 'required');
           

           
            


        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
                return true;
        }

    }


    private function savePaidFee(){
    	
    	$post_data                            = $this->input->post();
        $fetch_already = array();
        $fetch_already['Status'] = 'pending';
        $fetch_already['CommonID'] = $this->session->userdata['admin']['UserID'];
        $fetch_already['PaymentType'] = 'Registration';

        $check_already = $this->Payment_model->getWithMultipleFields($fetch_already);
        
        if($check_already){
        	$success['error']   = 'Your have already paid fee.please wait admin to approved your payment and your old transaction id is "'.$check_already->TransactionID.'"';
            $success['success'] = false;
            echo json_encode($success);
            exit;
        }





        $save_payment_array = array();
        $save_payment_array['CommonID']       = $this->session->userdata['admin']['UserID'];
        $save_payment_array['PaymentType']    = 'Registration';
        $save_payment_array['PaymentVia']     = $post_data['PayVia'];
        $save_payment_array['Cnic']    		  = $post_data['Cnic']; 
        $save_payment_array['Amount']         = $post_data['Amount']; 
       
        $save_payment_array['TransactionID']   = $post_data['TransactionID'];
        $save_payment_array['DepositDate']     = $post_data['DepositDate'];
        $save_payment_array['Status']          = 'pending';
        $save_payment_array['AmountToPaid'] = ($post_data['Type'] == 'customer' ? 50 : 10);

        $insert_id = $this->Payment_model->save($save_payment_array);     
       
        
         
        

        
        if($insert_id > 0)
            {

            	$update_user = array();
		        $update_user_by  = array();

		        $update_user['Type'] = $post_data['Type'];
		        $update_user_by['UserID'] = $this->session->userdata['admin']['UserID'];

		        $this->User_model->update($update_user,$update_user_by);
                
                $success['error']   = false;
	            $success['reset']   = True;
	            $success['success'] = 'Your request sent successfully.please wait admin to approved your payment';
	            echo json_encode($success);
	            exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
    
    
    
	

}