<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Base_Controller {
	public 	$data = array();



	public function __construct()
	{
		parent::__construct();
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
         $this->load->Model([
            'Product_model',
            'Site_images_model',
            'Category_model',
             'Site_videos_model',
             'Reviews_model'

        ]);


	}


	public function details($product_id = '',$ajax_para = false)
	{


        

         if(isset($_POST) && !empty($_POST) && !$ajax_para){

           $SearchCategoryID = $this->input->post('SearchCategoryID');
           $keyword      = $this->input->post('txtCategory');
            $this->data['product'] =$this->Product_model->getProductData(true,'','EN','products.CategoryID = '.$SearchCategoryID,'ASC','SortOrder','products_text.Title',$keyword);

            $product_id = $this->data['product'][0]['ProductID'];

            $this->data['post_data'] = $this->input->post();
          
            }else{
             $this->data['product'] = $this->Product_model->getProducts($this->language,'products.ProductID = '.$product_id.'','DESC','products.ProductID');
            }


       

        $user_id = $this->session->userdata['admin']['UserID'];

     // youtube video
        $fetch_by = array();
        $fetch_by['VideoType'] = 'Productvideos';
        $fetch_by['FileID']    = $product_id;

        $this->data['ProductVideo'] = $this->Site_videos_model->getWithMultipleFields($fetch_by);

        //print_rm($this->data['ProductVideo']);


        $fetch_by = array();
        $fetch_by['ImageType'] = 'ProductImage';
        $fetch_by['FileID']    = $product_id;
        
        $this->data['product_images'] = $this->Site_images_model->getMultipleRows($fetch_by);

    // product reviews

        $fetch_by = array();
        $fetch_by['ProductID']    = $product_id;

        $this->data['reviews'] = $this->Reviews_model->getReviews($product_id);

        //print_rm($this->data['reviews']);
        //print_rm(canWriteReview($user_id));

		
        if($ajax_para){
            
            $html  = $this->load->view('front/'.$this->data['ControllerName'].'/quick_view',$this->data,true);
            
            $success['html'] = $html;
            
            echo json_encode($success);
            exit;


        } else {
            if(empty($this->data['product'])){
                redirect(base_url());
            }


            $where  = '';
            $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";

             $this->data['related_products'] = $this->Product_model->getProducts($this->language,$where.' AND products.CategoryID = '.$this->data['product'][0]['CategoryID'].' AND products.ProductID != '.$this->data['product'][0]['ProductID'].'','DESC','products.ProductID',10,0);
             $this->data['sub_categories'] = $this->Category_model->getAllJoinedData(true,'CategoryID', $this->language,'categories.ParentID = '.$this->data['product'][0]['CategoryID'].'');
            
            $this->data['view'] = 'front/'.$this->data['ControllerName'].'/details';
            $this->load->view('front/layouts/default',$this->data);
        }
        
	}



	public function addReview()
    {

        $this->reviewValidation();
        $post_data = $this->input->post();
        $save_review_array = array();
        $save_review_array['UserID']     = $this->session->userdata['admin']['UserID'];
        $save_review_array['ProductID']     = $post_data['ProductID'];
        $save_review_array['Review']     = $post_data['review'];
        $save_review_array['Rating']     = $post_data['rating'];

        $this->Reviews_model->save($save_review_array);
        $success['error']   = false;
        $success['reload']   = True;
        $success['success'] = 'Your review is posted successfully.';
        echo json_encode($success);
        exit;

    }


    private function reviewValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('review', 'Review description is required', 'required');






        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }

    }



    public function categories()
    {
        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/categories';
        $this->load->view('front/layouts/default',$this->data);
    }






}
