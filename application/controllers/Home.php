<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller {
    public  $data = array();


    
    public function __construct() 
    {   

        parent::__construct();
        require_once APPPATH . 'third_party/googleApi/Google_Client.php';
        require_once APPPATH . 'third_party/googleApi/contrib/Google_Oauth2Service.php';
        require_once APPPATH . 'third_party/facebooksdk/vendor/autoload.php';
        
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Role_model');
        $this->load->model('Modules_users_rights_model');
        $this->load->model('Module_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Site_images_model');
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['language'] = $this->language;
        $this->data['TableKey'] = 'UserID';
        $this->data['Table'] = 'users';
        $this->data['ControllerName'] = $this->router->fetch_class();
 
    }


    public function facebook_login() {
  

        /*            if($this->session->userdata('user_loggedin')){

          redirect('/home/dashboard');

         */
        if (!session_id()) {
            session_start();
        }

        $fb = new Facebook\Facebook([
            'app_id' => '412317422558097', // Replace {app-id} with your app id
            'app_secret' => '9c4864bc3fa617af7e9bfd02c6eebc1b',
            'default_graph_version' => 'v2.2',
        ]);

        if (isset($_GET['code'])){ 

            // Get user facebook profile details


            try {

                $helper = $fb->getRedirectLoginHelper();
               if (isset($_GET['state'])) {
                    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
                }
                $accessToken = $helper->getAccessToken();


                $object = $fb->get('/me?fields=id,first_name,last_name,email,gender,locale,picture', $accessToken);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            // Preparing data for database insertion
            $body = (string) $object->getBody();
            $userProfile = json_decode($body);

            // if no email then check socialID
            if(isset($userProfile->email)) {
                $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
            } else {
                $user = $this->User_model->getUserDataBySocialID($userProfile->id, $this->language);
                $userProfile->email = '';
            }
            
            
           if (!empty($user)) {

                if($user['RoleID'] != '1' && $user['RoleID'] != '2')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                    

                }
                
                $this->session->set_userdata('admin', $user);
                //$this->updateUserLoginStatus();
                redirect('cms/dashboard');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'facebook';
                $save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 3;
                
                $save_parent_data['Email']          = $userProfile->email;
                $save_parent_data['Gender']          = (isset($userProfile->gender) ? $userProfile->gender : 'Male');
                $save_parent_data['RegistrationFeePaid'] = 'No';
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
               
                
                 
                

                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    
                
                
                
                    $default_lang = getDefaultLanguage();


                    $fb_first_name = (isset($userProfile->first_name) ? $userProfile->first_name : 'User');
                    $fb_last_name = (isset($userProfile->last_name) ? $userProfile->last_name : 'Name');
                    $save_child_data['FullName']                        = $fb_first_name.' '.$fb_last_name;
                    $save_child_data['UserID']                          = $insert_id;
                    $save_child_data['SystemLanguageID']                = 1;
                   $this->$child->save($save_child_data);

                    $modules = $this->Module_model->getAll();
                    foreach($modules as $key => $value)
                    {
                        
                            $other_data[] = [
                                    'ModuleID'  => $value->ModuleID,
                                    'UserID'    => $insert_id,
                                    'CanView'   => (checkRightAccess($value->ModuleID, 3, 'CanView') ? 1 : 0),
                                    'CanAdd'    => (checkRightAccess($value->ModuleID, 3, 'CanAdd') ? 1 : 0),
                                    'CanEdit'   => (checkRightAccess($value->ModuleID, 3, 'CanEdit') ? 1 : 0),
                                    'CanDelete' => (checkRightAccess($value->ModuleID, 3, 'CanDelete') ? 1 : 0),
                                    'CreatedAt' => date('Y-m-d H:i:s'),
                                    'CreatedBy' => 1,
                                    'UpdatedAt' => date('Y-m-d H:i:s'),
                                    'UpdatedBy' => 1
                            ];



                    }
                    
                    
                   
                    $this->Modules_users_rights_model->insert_batch($other_data);
                    
                    
                    $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
                    $this->session->set_userdata('admin', $user);

                  
                  
                    redirect('cms/dashboard');


                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect('account/login');
                }

            } 
            
            // end

           





            // Get logout URL
            //  $data['logoutUrl'] = $this->facebook->logout_url();
        } else {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(base_url().'home/facebook_login', $permissions);
            $loginUrl = htmlspecialchars($loginUrl);
            $loginUrl = str_replace("amp;", "", $loginUrl);
            header("Location: $loginUrl");
            exit;
        }
    }

    public function google_login() {
        

        // if ($this->session->userdata('user_loggedin')) {
        //     redirect('/');
        //     print_r($$this->session->userdata('user_loggedin')); exit;
        if (!session_id()) {
            session_start();
        } else {
            $clientId = '412994844618-fvjajuvp6bfq8fi0dg7b573c8ajqimm9.apps.googleusercontent.com'; //Google client ID
            $clientSecret = 'f05-xVrxSR86MR0PdZcaji_V'; //Google client secret
            $redirectURL = base_url() . 'home/google_login';

            //Call Google API
            $gClient = new Google_Client();
            $gClient->setApplicationName('Login');
            $gClient->setClientId($clientId);
            $gClient->setClientSecret($clientSecret);
            $gClient->setRedirectUri($redirectURL);
            $google_oauthV2 = new Google_Oauth2Service($gClient);

            if (isset($_GET['code'])) {
                $gClient->authenticate($_GET['code']);
                $_SESSION['token'] = $gClient->getAccessToken();
                header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));


                if (isset($_SESSION['token'])) {
                    $gClient->setAccessToken($_SESSION['token']);
                }

                if ($gClient->getAccessToken()) {
                    $userProfile = $google_oauthV2->userinfo->get();

                    

                   

                    // my working 
            $user = $this->User_model->getUserDataByEmail($userProfile['email'], $this->language);
           if (!empty($user)) {

                if($user['RoleID'] != '1' && $user['RoleID'] != '2')
                {
                    if($user['IsActive'] == 0){
                        $this->session->set_flashdata('message', 'Your Account has been blocked by Administrator.');
                        redirect('account/login');
                    }
                    

                }
                
                $this->session->set_userdata('admin', $user);
                //$this->updateUserLoginStatus();
                redirect('cms/dashboard');
            }else{
                $parent                             = 'User_model';
                $child                              = 'User_text_model';
                $save_parent_data                   = array();
                $save_child_data                    = array();
               
                $getSortValue = $this->$parent->getLastRow('UserID');
                   
                $sort = 0;
                if(!empty($getSortValue))
                {
                   $sort = $getSortValue['SortOrder'] + 1;
                }

                
                $save_parent_data['FromSocial']     = 'gmail';
                //$save_parent_data['SocialID']       = $userProfile->id;
                $save_parent_data['SortOrder']      = $sort;
                $save_parent_data['IsActive']       = 1;
                $save_parent_data['RoleID']         = 3;
                
                $save_parent_data['Email']          = $userProfile['email'];
                $save_parent_data['Gender']         = (isset($userProfile['gender']) ? $userProfile['gender'] : '') ;
                $save_parent_data['RegistrationFeePaid'] = 'No';
                $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
               
                
                 
                

                $insert_id                          = $this->$parent->save($save_parent_data);

                if($insert_id > 0)
                {
                    
                
                
                
                    $default_lang = getDefaultLanguage();
                    
                    
                    
                    $save_child_data['FullName']                        = $userProfile['given_name'].' '.$userProfile['family_name'];
                    $save_child_data['UserID']                          = $insert_id;
                    $save_child_data['SystemLanguageID']                = 1;
                   $this->$child->save($save_child_data);

                    $modules = $this->Module_model->getAll();
                    foreach($modules as $key => $value)
                    {
                        
                            $other_data[] = [
                                    'ModuleID'  => $value->ModuleID,
                                    'UserID'    => $insert_id,
                                    'CanView'   => (checkRightAccess($value->ModuleID, 3, 'CanView') ? 1 : 0),
                                    'CanAdd'    => (checkRightAccess($value->ModuleID, 3, 'CanAdd') ? 1 : 0),
                                    'CanEdit'   => (checkRightAccess($value->ModuleID, 3, 'CanEdit') ? 1 : 0),
                                    'CanDelete' => (checkRightAccess($value->ModuleID, 3, 'CanDelete') ? 1 : 0),
                                    'CreatedAt' => date('Y-m-d H:i:s'),
                                    'CreatedBy' => 1,
                                    'UpdatedAt' => date('Y-m-d H:i:s'),
                                    'UpdatedBy' => 1
                            ];



                    }
                    
                    
                   
                    $this->Modules_users_rights_model->insert_batch($other_data);
                    
                    
                    $user = $this->User_model->getUserDataByEmail($userProfile->email, $this->language);
                    $this->session->set_userdata('admin', $user);

                  
                  
                    redirect('cms/dashboard');


                }else
                {
                   $this->session->set_flashdata('message', 'Something went wrong.');
                   redirect('account/login');
                }

            } 
            
            // end
                }
            } else {
                $url = $gClient->createAuthUrl();
                header("Location: $url");
                exit;
            }
        }
    }
     
    
    
    
    
    

}