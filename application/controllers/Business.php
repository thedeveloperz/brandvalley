<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends Base_Controller {
	public 	$data = array();



	public function __construct()
	{
		parent::__construct();
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
         $this->load->Model([
             'Business_page_model',
             'State_model',
             'State_text_model',
             'City_model',
             'City_text_model'
        ]);


	}


	public function brand($business_page_id = '')
    {
        if($business_page_id == '') redirect("/");

        if(isset($_POST) && !empty($_POST)){
            $loc_data = array();
            $loc_data['CountryID'] = ($this->input->post('CountryID') != '' ? $this->input->post('CountryID') : 166 );
            $loc_data['StateID'] = ($this->input->post('StateID') != '' ? $this->input->post('StateID') : 2728 );//$this->input->post('StateID');
            $loc_data['CityID'] = ($this->input->post('CityID') != '' ? $this->input->post('CityID') : 31439 );//$this->input->post('CityID');
            $this->setCookie($loc_data);
        }

        $user_id = $this->session->userdata('admin')['UserID'];

        $business_page = $this->Business_page_model->get($business_page_id, false, 'BusinessPageID');
        if(!$business_page) redirect("/");

        $this->data['BusinessPage'] = $business_page;

//      print_rm($this->data['BusinessPage']);

        $where = "businessproducts.BusinessPageID = $business_page->BusinessPageID";

        $this->data['products'] = $this->Business_page_model->getAllBusinessProducts(false,"BusinessproductID",$this->language, $where);

//       print_rm($this->data['products']);

        $this->data['CountryData'] =  $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID = '.$this->country,'DESC','');
        $this->data['StateData'] =  $this->State_model->getJoinedData(false,'StateID','states.StateID = '.$this->state,'DESC','');
        $this->data['CityData'] =  $this->City_model->getJoinedData(false,'CityID','cities.CityID = '.$this->city,'DESC','');

        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/brand';
        $this->load->view('front/layouts/default',$this->data);


    }


	public function details($product_id = '',$ajax_para = false)
	{
         if(isset($_POST) && !empty($_POST) && !$ajax_para){

           $SearchCategoryID = $this->input->post('SearchCategoryID');
           $where = false;
           if($SearchCategoryID != ''){
               $where = 'products.CategoryID = '.$SearchCategoryID;
           }
           $keyword      = $this->input->post('txtCategory');
            $this->data['product'] =$this->Product_model->getProductData(true,'','EN',$where,'ASC','SortOrder','products_text.Title',$keyword);

            $product_id = $this->data['product'][0]['ProductID'];

            $this->data['post_data'] = $this->input->post();
          
            }else{
             $this->data['product'] = $this->Product_model->getProducts($this->language,'products.ProductID = '.$product_id.'','DESC','products.ProductID');
            }


       

        //$user_id = $this->session->userdata['admin']['UserID'];

     // youtube video
        $fetch_by = array();
        $fetch_by['VideoType'] = 'Productvideos';
        $fetch_by['FileID']    = $product_id;

        $this->data['ProductVideo'] = $this->Site_videos_model->getWithMultipleFields($fetch_by);

        //print_rm($this->data['ProductVideo']);


        $fetch_by = array();
        $fetch_by['ImageType'] = 'ProductImage';
        $fetch_by['FileID']    = $product_id;
        
        $this->data['product_images'] = $this->Site_images_model->getMultipleRows($fetch_by);

    // product reviews

        $fetch_by = array();
        $fetch_by['ProductID']    = $product_id;

        $this->data['reviews'] = $this->Reviews_model->getReviews($product_id);

        //print_rm($this->data['reviews']);
        //print_rm(canWriteReview($user_id));

		
        if($ajax_para){
            
            $html  = $this->load->view('front/'.$this->data['ControllerName'].'/quick_view',$this->data,true);
            
            $success['html'] = $html;
            
            echo json_encode($success);
            exit;


        } else {
            if(empty($this->data['product'])){
                redirect(base_url());
            }


            $where  = '';
            $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";

            $this->data['related_products'] = $this->Product_model->getProducts($this->language,$where.' AND products.CategoryID = '.$this->data['product'][0]['CategoryID'].' AND products.ProductID != '.$this->data['product'][0]['ProductID'].'','DESC','products.ProductID',10,0);
            $this->data['sub_categories'] = $this->Category_model->getAllJoinedData(true,'CategoryID', $this->language,'categories.ParentID = '.$this->data['product'][0]['CategoryID'].'');
            
            $this->data['view'] = 'front/'.$this->data['ControllerName'].'/details';
            $this->load->view('front/layouts/default',$this->data);
        }
        
	}




    public function categories($category_id='', $sub_category_id = '')
    {
        if($category_id == '') {
            redirect("/");
        }


        $this->data['sub_cats'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,"categories.ParentID=$category_id");

        //print_rm($this->data['sub_cats']);

        $where  = '';
        $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";
        $this->data['sub_category'] = '';
        if($sub_category_id != '') {
            $where .= " AND products.SubCategoryID = $sub_category_id";
            $this->data['sub_category'] = $this->Category_model->getJoinedData(true, 'CategoryID', 'categories.CategoryID ='.$sub_category_id);
        }
        $this->data['category_products'] = $this->Product_model->getProducts($this->language, $where.' AND products.CategoryID = '.$category_id,'DESC','products.ProductID');


        //print_rm($this->data['category_products']);



        $this->data['category_id'] = $category_id;
        $this->data['sub_category_id'] = $sub_category_id;

        $this->data['main_category'] = $this->Category_model->getJoinedData(true, 'CategoryID', 'categories.CategoryID ='.$category_id);



        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/categories';
        $this->load->view('front/layouts/default', $this->data);
    }

    public function loadmoreproducts()
    {
        //exit("helll");
        $post_data = $this->input->post();
        $cat_ids = '';
        if($post_data['cat_ids']) {
            $cat_ids = rtrim($post_data['cat_ids'], ",");
        }

        if($post_data['cat_ids'] == ''){
            echo '<div class="alert alert-danger">No product available now.</div>';
            exit;
        }

        $where  = '';
        $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";
        $more_categories = $this->Category_model->getLoadMoreCategory($this->language,"categories.IsActive = 1 AND categories.CategoryID NOT IN ($cat_ids)",'rand()', 1, 0, 'categories.CategoryID');

        $cat_products = array();
        if($more_categories){

            foreach ($more_categories as $key => $value) {
                $products = $this->Product_model->getProducts($this->language,$where.' AND products.CategoryID = '.$value['CategoryID'].' ','DESC','products.ProductID',8,0);
                if($products){
                    $value['products'] = $products;
                }else{
                    $value['products'] = array();
                }
                $cat_products[] = $value;
            }
        }

        $this->data['top_categories'] = $cat_products;

        //print_rm($this->data['top_categories']);


        $data = $this->load->view('front/product/loadmore', $this->data, TRUE);
        echo $data;
        exit;
    }


}

