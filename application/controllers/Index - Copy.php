<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller {
	public 	$data = array();


	
	public function __construct() 
	{   
		parent::__construct();	
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
         $this->load->Model([
            'State_model',
            'State_text_model',
            'City_model',
            'City_text_model',
            'Product_model'
            
        ]);
        
 
	}
	 
    
    public function index()
	{	

		
		//delete_cookie('location');

        $where  = '';
        $where .= "product_locations.CountryID = $this->country AND product_locations.StateID = $this->state AND product_locations.CityID = $this->city";
        $this->data['latest_products'] = $this->Product_model->getProducts($this->language,$where,'DESC','products.ProductID',10,0);

		
        

        $top_categories = $this->Category_model->getRandomCategoryWithProducts($this->language,'categories.IsActive = 1 AND categories.ParentID = 0','rand()',3);

        $cat_products = array();
        if($top_categories){

            foreach ($top_categories as $key => $value) {

                    $products = $this->Product_model->getProducts($this->language,$where.' AND products.CategoryID = '.$value['CategoryID'].' ','DESC','products.ProductID',8,0);


                    if($products){
                        $value['products'] = $products;
                    }else{
                        $value['products'] = array();
                    }
                    $cat_products[] = $value; 
 
            }


        }

        $this->data['top_categories'] = $cat_products;
        
        
           
		//echo $this->db->last_query();exit;
        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/home';
        $this->load->view('front/layouts/default',$this->data);
	}




	public function getCountryStates()
    {
        $CountryID = $this->input->post('CountryID');

        $parent                             = 'State_model';
        
        $response = array();
        $options = '<option value="">'.lang("choose_state").'</option>';

        if(is_array($CountryID))
        {
            
            foreach ($CountryID as $key => $country) {
                $getCountryDetail = $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID ='.$country,'DESC','');
                $options .= '<optgroup label="'.$getCountryDetail[0]->Title.'">';
                $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$country);

                foreach ($States as $key => $value) {
                    $options .= '<option value="'.$country.'-'.$value->StateID.'">'.$value->Title.'</option>';
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {

            $States = $this->$parent->getAllJoinedData(false,'StateID',$this->language,'states.IsActive = 1 AND states.CountryID = '.$CountryID);

            foreach ($States as $key => $value) {
                $options .= '<option value="'.$value->StateID.'">'.$value->Title.'</option>';
            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }
    

     public function getStateCities()
    {
        $StateID = $this->input->post('StateID');

        $parent                             = 'City_model';

        $response = array();
        $options = '<option value="">'.lang("choose_city").'</option>';
        
        if(is_array($StateID))
        {
            
            foreach ($StateID as $key => $state) {
                $statearr = explode('-', $state);
                $getStateDetail = $this->State_model->getJoinedData(false,'StateID','states.StateID ='.$statearr[0],'DESC','');
                $options .= '<optgroup label="'.$getStateDetail[0]->Title.'">';
                $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$statearr[0]);

                foreach ($Cities as $key => $value) {
                    $options .= '<option value="'.$state.'-'.$value->CityID.'">'.$value->Title.'</option>';
                }
                $options .= '</optgroup>';
            }
            $response['array'] = true;
        }
        else
        {
            $Cities = $this->$parent->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1 AND cities.StateID = '.$StateID);

            foreach ($Cities as $key => $value) {
                $options .= '<option value="'.$value->CityID.'">'.$value->Title.'</option>';
            }
            $response['array'] = false;
        }

        $response['success'] = true;
        $response['html'] = $options;

        echo json_encode($response);
        exit();
    }


    public function setLocation(){
		
// i am leaving this because google api is paid
    	$latitude = $this->input->post('Latitude');
    	$lognitude = $this->input->post('Longitude');
    	if(get_cookie('location')){
    		delete_cookie('location');
    	}
    	
    	
		$geocode_stats = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$lognitude&sensor=false");
		$output_deals = json_decode($geocode_stats);
		
		echo $country = $output_deals->results[2]->address_components[4]->long_name;
    	$cookie = array(
                'name' => 'location',
                'value' => $latitude.'@'.$lognitude,
                'expire' => time() + 86500,

            );

        set_cookie($cookie);


       echo get_cookie('location');exit;

	}
   
    
    
    
	

}