<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
         $this->load->model('User_text_model');
        $this->load->model('Payment_model');
        $this->load->model('Module_model');
        $this->load->model('Modules_users_rights_model');
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['site_setting'] = $this->getSiteSetting();
    }

    public function signup()
    {
        $location_data = $this->getLocationByIp();
        $this->data['country'] = strtolower(( $location_data['CountryTitle'] ? $location_data['CountryTitle'] : '' ));

        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/signup';
		$this->load->view('front/layouts/default',$this->data);
    }

    public function login()
    {
        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/login';
        $this->load->view('front/layouts/default',$this->data);
    }
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->registrationValidation();
                $this->save();
          break; 
           
                 
        }
    }
    
    
    private function registrationValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
           // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
            $this->form_validation->set_rules('Type', 'Registration Type', 'required');
            $this->form_validation->set_rules('FullName', 'First Name', 'required');
            //$this->form_validation->set_rules('LastName', 'Last Name', 'required');
            $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
            //$this->form_validation->set_rules('ConfirmEmail', lang('confirm_email'), 'required');
             $this->form_validation->set_rules('Phone', 'Telephone', 'required');
            $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
            $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
            
            $this->form_validation->set_rules('CountryID', 'Country', 'required');
            $this->form_validation->set_rules('StateID', 'State', 'required');
            $this->form_validation->set_rules('CityID', 'City', 'required');
            
            if($this->input->post('PayVia') != 'pay_later'){
                $this->form_validation->set_rules('Cnic', 'Cnic', 'required');
                $this->form_validation->set_rules('Amount', 'Deposit Amount', 'required');
                $this->form_validation->set_rules('TransactionID', 'Transaction ID', 'required');
                
            }

           
            


        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
                return true;
        }

    }
    
     public function save(){

        $post_data                          = $this->input->post();
        $post_data['RoleID']                = 3;//front users
        $parent                             = 'User_model';
        $child                              = 'User_text_model';
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow('UserID');
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           $sort = $getSortValue['SortOrder'] + 1;
        }
       
       
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = 0;
        $save_parent_data['RoleID']         = $post_data['RoleID'];
        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];

        //$save_parent_data['Website']        = $post_data['Website'];
        $save_parent_data['Type']           = $post_data['Type'];
        $save_parent_data['Phone']          = $post_data['Phone'];
        $save_parent_data['Gender']          = $post_data['Gender'];
        
        $save_parent_data['CountryID']          = $post_data['CountryID'];
        $save_parent_data['StateID']          = $post_data['StateID'];  
        $save_parent_data['CityID']          = $post_data['CityID']; 
        
        $save_parent_data['RegistrationFeePaid'] = ($post_data['PayVia'] == 'pay_later' ? 'No':'Yes');


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
       
        
         
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
            
                $default_lang = getDefaultLanguage();
                
                
                
                $save_child_data['FullName']                        = $post_data['FullName'];
                $save_child_data['UserID']                          = $insert_id;
                $save_child_data['SystemLanguageID']                = 1;
               $this->$child->save($save_child_data);

                $modules = $this->Module_model->getAll();
                foreach($modules as $key => $value)
                {
                    /*if($post_data['RoleID'] == 1 || $post_data['RoleID'] == 2){
                        $rights_all = 1;
                    }else{
                        $rights_all = 0;
                    }*/

                        $other_data[] = [
                                'ModuleID'  => $value->ModuleID,
                                'UserID'    => $insert_id,
                                'CanView'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanView') ? 1 : 0),
                                'CanAdd'    => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanAdd') ? 1 : 0),
                                'CanEdit'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanEdit') ? 1 : 0),
                                'CanDelete' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanDelete') ? 1 : 0),
                                'CreatedAt' => date('Y-m-d H:i:s'),
                                'CreatedBy' => 1,
                                'UpdatedAt' => date('Y-m-d H:i:s'),
                                'UpdatedBy' => 1
                        ];



                }
                
                
               
                $this->Modules_users_rights_model->insert_batch($other_data);
                
                // add payment
               
               if($post_data['PayVia'] != 'pay_later'){
                    $save_payment_array = array();
                    $save_payment_array['CommonID']       = $insert_id;
                    $save_payment_array['PaymentType']     = 'Registration';
                    $save_payment_array['PaymentVia']     = $post_data['PayVia'];
                    $save_payment_array['Cnic']    = $post_data['Cnic']; 
                    $save_payment_array['Amount']      = $post_data['Amount']; 
                   
                    $save_payment_array['TransactionID']         = $post_data['TransactionID'];
                    $save_payment_array['DepositDate']         = date('Y-m-d', strtotime($post_data['DepositDate']));
                    $save_payment_array['Status']         = 'pending';
                    $save_payment_array['AmountToPaid'] = ($post_data['Type'] == 'customer' ? 50 : 10);
                    $this->Payment_model->save($save_payment_array);
               }

                

              
                $success['error']   = false;
                $success['reload']   = True;
                $success['success'] = 'Your account is created successfully please wait admin to approved your account';
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }

    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url'));

    }
    


}
