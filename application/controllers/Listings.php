<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listings extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
       // checkAdminSession();
                
                $this->load->Model([
            'User_model',
             'UserProfileType_model',
            'State_model',
            'State_text_model',
            'City_model',
            'City_text_model'
        ]);

        $this->load->helper('text');





        $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = 'User_model';
                
                $this->data['TableKey'] = 'UserID';
                $this->data['Table'] = 'users';
       
        
    }

    public function all($name = '', $id = '')
    {
        if($id == '' || $name == '') {
            redirect("/");
        }

        if(isset($_POST) && !empty($_POST)){

            $loc_data = array();
            $loc_data['CountryID'] = ($this->input->post('CountryID') != '' ? $this->input->post('CountryID') : 166 );
            $loc_data['StateID'] = ($this->input->post('StateID') != '' ? $this->input->post('StateID') : 2728 );//$this->input->post('StateID');
            $loc_data['CityID'] = ($this->input->post('CityID') != '' ? $this->input->post('CityID') : 31439 );//$this->input->post('CityID');
            $this->setCookie($loc_data);
        }

        $parent  = $this->data['Parent_model'];

        $where_manufactures = "users.CountryID = $this->country AND users.StateID = $this->state AND users.CityID = $this->city AND users.IsActive = 1 AND users.UserProfileType = $id";
        $this->data['profiles'] = $this->User_model->getAllJoinedData(true,'UserID',$this->language, $where_manufactures);

        //$this->data['profiles']  = $this->$parent->getJoinedData(true, $this->data['TableKey'], $this->data['Table'].'.UserProfileType='.$id, 'DESC', '');

    //print_rm($this->data['profiles']);


        // Get all listings OR Profile Types OR manufacturers
        $this->data['ProfileTypes'] = $this->UserProfileType_model->getAll();

        $this->data['id'] = $id;
        $this->data['name'] = $name;


        $this->data['CountryData'] =  $this->Country_model->getJoinedData(false,'CountryID','countries.CountryID = '.$this->country,'DESC','');
        $this->data['StateData'] =  $this->State_model->getJoinedData(false,'StateID','states.StateID = '.$this->state,'DESC','');
        $this->data['CityData'] =  $this->City_model->getJoinedData(false,'CityID','cities.CityID = '.$this->city,'DESC','');


        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/listing';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('front/layouts/default',$this->data);

    }
     
    
    
    
    public function detail($id = '')
    {
        
        
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->getJoinedData(true,$this->data['TableKey'],$this->data['Table'].'.UserProfileType='.$id,'DESC','');
        
        
        if(!$this->data['result']){
           redirect(base_url()); 
        }

        $this->data['view'] = 'front/'.$this->data['ControllerName'].'/details';
        $this->data[$this->data['TableKey']]   = $id;
        $this->load->view('front/layouts/default',$this->data);
        
    }
    
    
    
    
    
    
    

}