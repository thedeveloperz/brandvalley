<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Order_model');
        $this->load->model('Order_item_model');
        $this->load->model('Temp_order_model');
        $this->load->model('User_model');
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        
    }
    
    
    public function index(){
         $this->order_detail();
     }
    public function order_detail($step = 1){
        if (!$this->session->userdata('admin')) 
        {
            $this->session->set_userdata('url','checkout');
            redirect('account/login');
        }else{
            
            
            $user_id = $this->session->userdata['admin']['UserID'];
            $this->data['products'] = $this->Order_model->getUserCartDetail($user_id);
            //echo '<pre>';print_r($this->data['products']);exit;
            
            if(empty($this->data['products'])){
                $this->session->set_flashdata('message', "There is no item in the cart.");
            redirect(base_url());
            }
         
            $this->data['total_product'] = getTotalProduct($user_id);

            $this->data['userinfo']      = $this->User_model->getJoinedData(true,'UserID','users.UserID ='.$this->session->userdata['admin']['UserID']);
           
            $this->data['step']  = $step;
            
            $this->data['view'] = 'front/cart/cart';
            $this->load->view('front/layouts/default',$this->data);
            
            
            
            
        }
    }
    public function place_order(){
        
        if ($this->input->post() != NULL) {
            $this->form_validation->set_rules('Name', 'FullName', 'trim|required');
            $this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('Address', 'Address', 'trim|required');
            $this->form_validation->set_rules('Telephone', 'Telephone', 'trim|required');
            $this->form_validation->set_rules('CountryID', 'Country', 'trim|required');
            $this->form_validation->set_rules('StateID', 'State', 'trim|required');
            $this->form_validation->set_rules('CityID', 'City', 'trim|required');
            $this->form_validation->set_rules('Cnic', 'CNIC', 'trim|required');
            $this->form_validation->set_rules('AmountDeposit', 'Amount', 'trim|required');

            $this->form_validation->set_rules('TransactionID', 'TransactionID', 'trim|required');
            
            $this->form_validation->set_rules('DepositDate', 'Deposit Date', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }
            $post_data = $this->input->post();
            
            $user_id = $this->session->userdata['admin']['UserID'];
            $products = $this->Order_model->getUserCartDetail($user_id);
            
            
            $save_data = array();
            $save_data['OrderTrackID'] = RandomString();
            $save_data['UserID'] = $user_id;
            $save_data['Name'] = $post_data['Name'];
            $save_data['Telephone'] = $post_data['Telephone'];
            $save_data['Email'] = $post_data['Email'];
            $save_data['Address'] = $post_data['Address'];
            $save_data['CountryID'] = $post_data['CountryID'];
            $save_data['StateID'] = $post_data['StateID'];
            $save_data['CityID'] = $post_data['CityID'];
            $save_data['Status'] = 'Pending';
            $save_data['PaymentMethod'] = $post_data['PaymentMethod'];
            $save_data['Cnic'] = $post_data['Cnic'];
            $save_data['TransactionID'] = $post_data['TransactionID'];
            $save_data['AmountDeposit'] = $post_data['AmountDeposit'];
            $save_data['DepositDate'] = date('Y-m-d H:i:s',strtotime($post_data['DepositDate']));
            
            $save_data['CreatedAt'] = $save_data['UpdatedAt'] = date('Y-m-d H:i:s');
            
            
            $insert_id = $this->Order_model->save($save_data);
            
            if($insert_id > 0){
                foreach($products as $product){
                    $child_data[] = [
                                        'OrderID'   => $insert_id,
                                        'ProductID'	=> $product['ProductID'],
                                        'Quantity'	=> $product['Quantity'],
                                        'Price'     => $product['Price']
                                        
                                ];
                }
                
                
                $this->Order_item_model->insert_batch($child_data);
                
                $deleted_by['UserID'] = $user_id;
                $this->Temp_order_model->delete($deleted_by);
                $this->sendThankyouEmailToCustomer($insert_id);
                $success['error']   = false;
                $success['success'] = "Your order has been placed successfully and your order id is ".$save_data['OrderTrackID'].".";
                $success['redirect'] = true;
                $success['url'] = 'checkout/thankyou';
                $this->session->set_flashdata('order_id',$save_data['OrderTrackID']);
                echo json_encode($success);
                exit;
                
                
            }else{
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
            
            
            
            
            
        }
        
    }


    public function thankyou(){
        if($this->session->flashdata('order_id')){
            $this->data['OrderTrackID'] = $this->session->flashdata('order_id');
            $this->data['view'] = 'front/cart/thankyou';
            $this->load->view('front/layouts/default', $this->data);
        }else{
            redirect(base_url());
        }
    }


    public function sendThankyouEmailToCustomer($order_id)
    {
        $this->data['order_items'] = $order_details  =  $this->Order_model->getOrdersDetailWithCustomerDetail($order_id);
      
        $message = $this->load->view('front/emails/order_confirmation',$this->data, true);
        $email_data['to'] = $order_details[0]['UserEmail'].','.$order_details[0]['Email'];
        $email_data['subject'] = 'Order received at brandsvalley : Order # ' . $order_details[0]['OrderTrackID'];
        $email_data['from'] = 'noreply@brandsvalley.net';
        $email_data['body'] = $message;
        //print_rm($email_data);
        sendEmail($email_data);
        
        
        return true;
    }    
    

    


}
